import 'package:flutter/material.dart';
import 'package:restorant_app/resources/app_colors.dart';
// importm

const TextStyle boldSizeMedium =
    TextStyle(fontWeight: FontWeight.w600, fontSize: 14);
const TextStyle boldSizeMedium14 =
    TextStyle(fontWeight: FontWeight.w600, fontSize: 12);
const TextStyle appBarTextColor =
    TextStyle(fontWeight: FontWeight.w500, fontSize: 12, color: darkAccent);
const TextStyle bottomBarTextColor =
    TextStyle(fontWeight: FontWeight.w700, fontSize: 12, color: lightPrimary);
const TextStyle bottomBarTextBold =
    TextStyle(fontWeight: FontWeight.w700, fontSize: 16, color: lightPrimary);
const TextStyle titleRestaurant =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 18);
const TextStyle titlePriceRestaurant =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 22);
const TextStyle defaltText =
    TextStyle(fontWeight: FontWeight.normal, fontSize: 12);
const TextStyle defaltTextBold15 =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 15);
const TextStyle defaltTextSemiBold15 =
    TextStyle(fontWeight: FontWeight.w600, fontSize: 15);
const TextStyle defaltTextBold10 =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 10);
const TextStyle defaltText15 =
    TextStyle(fontWeight: FontWeight.normal, fontSize: 15, color: Colors.black);
const TextStyle titleText =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 18);
const TextStyle titleTextWhite =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white);
const TextStyle titleTextWhiteBTN =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white);
const TextStyle subTitleText =
    TextStyle(fontWeight: FontWeight.normal, fontSize: 14);
const TextStyle pedidoItemTitle = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 16,
);
const TextStyle pedidoItemSubTitle =
    TextStyle(fontWeight: FontWeight.w300, fontSize: 10);
TextStyle pedidoItemPrice =
    TextStyle(fontWeight: FontWeight.w600, fontSize: 16, color: Colors.black87);
TextStyle pedidoRestaurantTitle = TextStyle(
    fontWeight: FontWeight.w800, fontSize: 16, color: Colors.green[900]);

const TextStyle dateText =
    TextStyle(fontWeight: FontWeight.w300, fontSize: 10, color: Colors.grey);
