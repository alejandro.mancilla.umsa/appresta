import 'package:flutter/material.dart';

const Color lightPrimary      = Colors.white;
const Color darkPrimary       = Colors.black;
const Color lightAccent       = Color(0xff6EB471);
const Color darkAccent        = Color(0xffF99F00);
const Color lightBG           = Colors.white;
const Color darkBG            = Colors.black54;
const Color badgeColor        = Colors.red;
const Color buttonColor       = Color(0xff6EB471);
const Color transparentColor  = Colors.transparent;
const Color opacityTitleColor = Color(0x80ffffff);
const Color greyBG            = Color(0xFFFAFAFA);
const Color hintColor         = Color(0xFFE8E8EA);
const Color shadowColor       = Colors.grey;
const Color lightGreyColor    = Color(0xFFBEBEBE);
const Color placeholderColor  = Color(0xFFAAAAAA);
const Color chipsColor        = Color(0xFFE6F8E6);
