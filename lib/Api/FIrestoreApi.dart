import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:restorant_app/Core/Models/BusinessModel.dart';
import 'package:restorant_app/Core/Models/OrderFinalModel.dart';
import 'package:restorant_app/Core/Models/ProductModelUnit.dart';
import 'package:restorant_app/Core/Utils.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';

class ApiFirebase {
  CollectionReference users = FirebaseFirestore.instance.collection('Parners');
  CollectionReference business =
      FirebaseFirestore.instance.collection('Business');
  CollectionReference products =
      FirebaseFirestore.instance.collection('Products');
  UserPreferences prefs = UserPreferences();

  void addUserParner(
      {String name,
      String number,
      String fechaNac,
      String idFirebase,
      String tokenNotificationsParner}) {
    users.doc(idFirebase).get().then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        users
            .doc(idFirebase)
            .update({
              'name': name,
              'number': number,
              'fechaNac': fechaNac,
              'idFirebase': idFirebase,
              'tokenNotificationsParner': tokenNotificationsParner,
            })
            .then((value) => print("UserParner Added"))
            .catchError((error) => print("Failed to add userParner: $error"));
      } else {
        users
            .doc(idFirebase)
            .set({
              'name': name,
              'number': number,
              'fechaNac': fechaNac,
              'idFirebase': idFirebase,
              'tokenNotificationsParner': tokenNotificationsParner,
              'status': false
            })
            .then((value) => print("UserParner Added"))
            .catchError((error) => print("Failed to add userParner: $error"));
      }
    });
  }

  void readRestaurants(ProviderRestaurant providerRestaurant) {
    providerRestaurant.listaBusiness = [];
    business
        .where('googleId', arrayContains: prefs.userGoogleId)
        .get()
        .then((QuerySnapshot querySnapshot) {
      List<BusinessModel> listaBusinessModel = [];
      querySnapshot.docs.forEach((doc) {
        BusinessModel businessModel = BusinessModel.fromJson(doc.data());
        listaBusinessModel.add(businessModel);
      });

      providerRestaurant.listaBusiness = listaBusinessModel;
      providerRestaurant.obtenerHoraSegunDia();
      print('####### readRestaurants #####');
    });
  }

  Future<bool> changeStatusOpenBusiness(
      {@required String idBusiness, String status}) {
    return business
        .doc(idBusiness)
        .update({'statusBusinessOpen': status})
        .then((value) => true)
        .catchError((error) => false)
        .timeout(
          Duration(seconds: 8),
          onTimeout: () => false,
        );
  }

  Future<bool> changePriorityProduct(String idProduct, double priority) {
    print('-----------');
    return products
        .doc(idProduct)
        .update({'rangePriority': priority})
        .then((value) => true)
        .catchError((error) => false)
        .timeout(
          Duration(seconds: 8),
          onTimeout: () => false,
        );
  }

  StreamSubscription<QuerySnapshot> listenReadProduct;
  void readProducts(String idBusiness, ProviderListPlatos providerListPlatos) {
    listenReadProduct = products
        .where('idBusiness', isEqualTo: idBusiness)
        .snapshots()
        .listen((QuerySnapshot querySnapshot) {
      // .get()
      // .then((QuerySnapshot querySnapshot) {
      List<ProductModelUnit> listaProductModelUnit = [];
      querySnapshot.docs.forEach((doc) {
        // print(doc.data());
        ProductModelUnit productModelUnit =
            ProductModelUnit.createByJson(doc.data());
        listaProductModelUnit.add(productModelUnit);
      });
      providerListPlatos.listaProducts = listaProductModelUnit;
      // return true;
    });
    // cancelOnError: true,
    // onError: () {
    //   providerListPlatos.listaProducts = null;
    // },
    // onDone: () {
    //   print('######## ON DONE READ PRODUCTS ');
    //   providerListPlatos.listaProducts = null;
    // });
    // .catchError((error) => false)
    // .timeout(
    //   Duration(seconds: 8),
    //   onTimeout: () => false,
    // );
  }

  void disposeListenReadProduct() {
    try {
      if (listenReadProduct != null) listenReadProduct.cancel();
    } catch (e) {
      print(e);
    }
  }

  void disposeListenReadOrders() {
    try {
      if (listenReadOrders != null) listenReadOrders.cancel();
    } catch (e) {
      print(e);
    }
  }

  StreamSubscription<QuerySnapshot> listenReadOrders;
  void readOrders(String idBusiness, ProviderListPlatos providerListPlatos,
      {String state = 'EN PROGRESO', @required DateTime dateNow}) {
    var today = DateTime.utc(
        dateNow.year, dateNow.month, dateNow.day + 1, 4, 0, 0, 0, 0);
    today = today.toLocal();
    var todayMAS1 =
        DateTime.utc(dateNow.year, dateNow.month, dateNow.day, 4, 0, 0, 0, 0);
    todayMAS1 = todayMAS1.toLocal();
    print('''
    ==== $idBusiness
    ==== $providerListPlatos
    ==== $state
    ==== $today
    ==== $todayMAS1
    ''');

    listenReadOrders = FirebaseFirestore.instance
        .collection('Orders')
        .where('tokenBusiness', arrayContains: idBusiness)
        .where('state', isEqualTo: state)
        .where('date', isLessThanOrEqualTo: today)
        .where('date', isGreaterThanOrEqualTo: todayMAS1)
        .snapshots()
        .listen((QuerySnapshot querySnapshot) {
      // .get()
      // .then((QuerySnapshot querySnapshot) {
      // providerListPlatos.listaOrders = [];
      print('______ antes de datos _____');
      List<OrderFinalModel> listaOrderFinalModel = [];
      providerListPlatos.listaOrders = null;
      Timer(Duration(seconds: 1), () {
        querySnapshot.docs.forEach((doc) {
          print('______ ${doc.data()} _____');
          OrderFinalModel orderFinalModel =
              OrderFinalModel.createByJson(doc.data());
          listaOrderFinalModel.add(orderFinalModel);
        });
        providerListPlatos.listaOrders = listaOrderFinalModel;
      });
      // return true;
    });
    // .catchError((error) => false)
    // .timeout(
    //   Duration(seconds: 8),
    //   onTimeout: () => false,
    // );

    // FirebaseFirestore.instance
    //     .collection('Orders')
    //     .where('tokenBusiness', arrayContains: idBusiness)
    //     .snapshots()
    //     .listen((querySnapshot) {
    //   print('''
    //   ================================================================
    //   ''');
    //   List<OrderFinalModel> listaOrderFinalModel = [];
    //   querySnapshot.docs.forEach((doc) {
    //     OrderFinalModel orderFinalModel =
    //         OrderFinalModel.createByJson(doc.data());
    //     print(orderFinalModel);
    //     listaOrderFinalModel.add(orderFinalModel);
    //   });
    //   providerListPlatos.listaOrders = listaOrderFinalModel;
    //   providerListPlatos.ordenOrdersByDate();
    //   providerListPlatos.ordenOrdersByState();
    //   // return listaOrderFinalModel;
    // }).onError((c) {
    //   print('ERROR $c');
    //   providerListPlatos.listaOrders = [];
    //   // return listaOrderFinalModel;
    // });

    // .then((QuerySnapshot querySnapshot) {
    //   List<OrderFinalModel> listaOrderFinalModel = [];
    //     querySnapshot.docs.forEach((doc) {
    //       print('______ ${doc.data()} _____');
    //       OrderFinalModel orderFinalModel = OrderFinalModel.createByJson(doc.data());
    //       listaOrderFinalModel.add(orderFinalModel);
    //     });
    //   providerListPlatos.listaOrders = listaOrderFinalModel;
    // });
  }

  // changeStatusBusiness({String idBusiness, bool status}) {
  //   business
  //       .doc(idBusiness)
  //       .update({'emergency': status})
  //       .then((value) => print("business Edited"))
  //       .catchError((error) => print("Failed to add business: $error"));
  // }

  deleteProduct(String idProduct) {
    products
        .doc(idProduct)
        .delete()
        .then((value) => print("products Deleted"))
        .catchError((error) => print("Failed to delete products: $error"));
  }

  updateBusiness(
      {String idBusiness,
      String city,
      List<String> days,
      String direction,
      String email,
      String name,
      String nameOwner,
      String numberOwner,
      String photo,
      int preparationTime,
      List<String> schedule,
      String zone,
      String ubication}) {
    business
        .doc(idBusiness)
        .update({
          'city': city,
          'days': days,
          'direction': direction,
          'email': email,
          'name': name,
          'nameOwner': nameOwner,
          'numberOwner': numberOwner,
          'photo': photo,
          'preparationTime': preparationTime,
          'schedule': schedule,
          'zone': zone,
          'ubication': ubication,
        })
        .then((value) => print("business Updated"))
        .catchError((error) => print("Failed to update business: $error"));
  }

  createBusiness(
      {String city,
      List<String> days,
      String direction,
      String email,
      String name,
      String nameOwner,
      String numberOwner,
      String photo,
      int preparationTime,
      List<String> schedule,
      String zone,
      String ubication}) {
    business.add({
      'businessPercentage': 10,
      'category': 'Restaurante',
      'city': city,
      'days': days,
      'description': 'sin descripcion',
      'direction': direction,
      'email': email,
      'emergency': false,
      'factorBaseDistance': 1000,
      'factorBasePrice': 7,
      'factorDistancePrice': 5,
      'factorExtraDistance': 1000,
      'googleId': ['${prefs.userGoogleId}'],
      'name': name,
      'nameOwner': nameOwner,
      'numberOwner': numberOwner,
      'photo': photo,
      'preparationTime': preparationTime,
      'schedule': schedule,
      'showingInApp': false,
      'slogan': 'sin slogan',
      'statusBusinessOpen': 'Automatic',
      'tokenNotificationBusiness': ['${prefs.userTokenNotification}'],
      'type': ['comida'],
      'ubication': ubication,
      'zone': zone,
    }).then((value) {
      business.doc(value.id).update({'idBusiness': value.id});
    }).catchError((error) => print("Failed to update business: $error"));
  }

  Future<bool> changeEmergencyProduct(String idProduct, bool status) {
    return products.doc(idProduct).update({'emergency': status}).then((value) {
      return true;
    }).catchError((error) {
      return false;
    }).timeout(Duration(seconds: 3), onTimeout: () {
      return false;
    });
  }

  Future<bool> readStatusUser() {
    return users
        .doc(prefs.userGoogleId)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        return documentSnapshot['status'];
      } else {
        return false;
      }
    });
  }
}
