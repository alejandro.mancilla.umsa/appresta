import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Auth/LoginScreen.dart';
import 'package:restorant_app/Core/Utils.dart';
import 'package:restorant_app/Pages/AdminRestPage.dart';
import 'package:restorant_app/Pages/HomeScreen.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';
import 'package:restorant_app/Providers/ProviderUser.dart';
import 'package:restorant_app/Providers/PushNotificationProvider.dart';
import 'package:restorant_app/SplashScreen.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  print("############### Handling a background message: ${message.messageId}");
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  final prefs = UserPreferences();
  await prefs.initPreferences();

  runApp(RestartWidget(
    child: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  void initState() {
    // print("==##== ${_initialization}");
    _initialization.then((value) {
      PushNotificationProvider notificationProvider =
          PushNotificationProvider();
      notificationProvider.initNotifications();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return CircularProgressIndicator();
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          // return MyAwesomeApp();
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(
                create: (BuildContext context) => ProviderListPlatos(),
              ),
              ChangeNotifierProvider(
                create: (BuildContext context) => ProviderUser(),
              ),
              ChangeNotifierProvider(
                create: (BuildContext context) => ProviderRestaurant(),
              ),
            ],
            child: MaterialApp(
              title: 'Chaski Restaurante',
              initialRoute: "/splash",
              builder: EasyLoading.init(),
              routes: {
                "/splash": (_) => ChaskiSplashScreen(),
                "/home": (_) => HomeScreen(),
                // "/login": (_) => LoginScreen(),
                // // "/AdminRestPage": (_) => AdminRestPage()
              },
              theme:
                  ThemeData(primaryColor: Colors.orange, fontFamily: 'Poppins'),
            ),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return CircularProgressIndicator();
      },
    );
  }
}

String format(double n) {
  return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 2);
}

///////////////////////
class RestartWidget extends StatefulWidget {
  RestartWidget({this.child});

  final Widget child;

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_RestartWidgetState>().restartApp();
  }

  @override
  _RestartWidgetState createState() => _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: widget.child,
    );
  }
}
