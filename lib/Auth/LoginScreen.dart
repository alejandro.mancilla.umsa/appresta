import 'package:country_pickers/country.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Core/Utils.dart';
import 'package:restorant_app/Pages/HomeScreen.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:restorant_app/Providers/ProviderUser.dart';
import 'package:restorant_app/privacy/privacy_policy.dart';
import 'package:verify_code_input/verify_code_input.dart';

// ignore: must_be_immutable
class LoginScreen extends StatelessWidget {
  final _phoneController = TextEditingController();
  final _codeController = TextEditingController();
  final _nameController = TextEditingController();
  // final ApiConection _api = new ApiConection();
  final ApiFirebase _api = new ApiFirebase();
  UserPreferences prefs = UserPreferences();
  ///////////////////
  String extencionNumber = "+591";
  Widget _buildDropdownItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}"),
          ],
        ),
      );
/////////////

  String codeVerification = '';
  // ignore: missing_return
  Future<bool> loginUser(String phone, BuildContext context) async {
    FirebaseAuth _auth = FirebaseAuth.instance;
    print(phone);
    _auth.verifyPhoneNumber(
      phoneNumber: phone,
      timeout: Duration(seconds: 60),
      verificationCompleted: (AuthCredential credential) async {
        print("#### ### ### $credential");
        Navigator.of(context).pop();
        UserCredential result = await _auth.signInWithCredential(credential);
        User user = result.user;
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return AlertDialog(
                title: Text("Verifica tu número"),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("Numero $phone Verificado Exitosamente"),
                    Icon(
                      Icons.check_circle,
                      color: Colors.green,
                      size: 80.0,
                    ),
                  ],
                ),
                actions: <Widget>[
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text("Continuar"),
                    textColor: Colors.white,
                    color: Colors.green,
                    onPressed: () async {
                      ProviderUser _userProv =
                          Provider.of<ProviderUser>(context, listen: false);
                      // enviar a backend el usuario
                      //
                      _api.addUserParner(
                          name: _userProv.name,
                          number: _userProv.phone,
                          fechaNac: _userProv.getfechaNac(),
                          idFirebase: user.uid,
                          tokenNotificationsParner:
                              prefs.userTokenNotification);

                      prefs.userName = _userProv.name;
                      prefs.userPhone = _userProv.phone;
                      prefs.userGoogleId = user.uid;

                      print("""
                      => ${_userProv.name}
                      => ${_userProv.phone}
                      => ${_userProv.getfechaNac()}
                      => ${user.uid}
                      """);
                      // Navigator.of(context).pop();
                      if (user != null) {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => HomeScreen(
                                    user: user,
                                  )),
                          ModalRoute.withName('/home'),
                        );
                      } else {
                        print("Error");
                      }
                    },
                  )
                ],
              );
            });
        //This callback would gets called when verification is done auto maticlly
      },
      verificationFailed: (FirebaseAuthException exception) {
        print("_______SverificationFailed EXEPTION# ${exception.message}");
      },
      codeSent: (String verificationId, [int forceResendingToken]) {
        print("###########");
        print(verificationId);
        print("###########");
        print("$forceResendingToken");
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return AlertDialog(
                insetPadding: EdgeInsets.all(0),
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 45),
                // titlePadding: EdgeInsets.only(top: 200),
                title: Text(
                  'Esperando el SMS con el código de verificación',
                  // textAlign: TextAlign.center,
                ),
                content: Column(
                  children: [
                    Expanded(child: Container()),
                    VerifyCodeInput(
                      onValueChanged: (value) {
                        codeVerification = value;
                      },
                      onComplete: (String value) {
                        codeVerification = value;
                      },
                    ),
                    Expanded(child: Container()),
                  ],
                ),
                actions: <Widget>[
                  // ignore: deprecated_member_use
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      codeVerification = '';
                    },
                    child: Text('Cancelar'),
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text("Confirm"),
                    textColor: Colors.white,
                    color: Colors.green,
                    onPressed: () async {
                      _codeController.text = codeVerification;
                      final code = _codeController.text.trim();
                      // print("###########  ########### ");
                      // print(code);
                      AuthCredential credential = PhoneAuthProvider.credential(
                          verificationId: verificationId, smsCode: code);
                      // print("###########  ########### ");
                      // print(credential);
                      UserCredential result =
                          await _auth.signInWithCredential(credential);

                      User user = result.user;
                      ProviderUser _userProv =
                          Provider.of<ProviderUser>(context, listen: false);
                      //enviar a backend el usuario
                      _api.addUserParner(
                          name: _userProv.name,
                          number: _userProv.phone,
                          fechaNac: _userProv.getfechaNac(),
                          idFirebase: user.uid,
                          tokenNotificationsParner:
                              prefs.userTokenNotification);
                      prefs.userName = _userProv.name;
                      prefs.userPhone = _userProv.phone;
                      prefs.userGoogleId = user.uid;

                      print("""
                      => ${_userProv.name}
                      => ${_userProv.phone}
                      => ${_userProv.getfechaNac()}
                      => ${user.uid}
                      """);

                      if (user != null) {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => HomeScreen(
                                    user: user,
                                  )),
                          ModalRoute.withName('/home'),
                        );
                      } else {
                        print("Error");
                      }
                    },
                  )
                ],
              );
            });
      },
      codeAutoRetrievalTimeout: (verificationId) {
        print("#### ###verificationId ### $verificationId");
      },
      // (String verificationId) {
      //   print("object verificationId $verificationId");
      //   Future.delayed(Duration(seconds: 2));
      // },
    );
  }

  @override
  Widget build(BuildContext context) {
    ProviderUser _userProv = Provider.of<ProviderUser>(context);
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Ingresa tu número de teléfono",
              style: TextStyle(color: Colors.white),
            ),
          ),
          body: Container(
            alignment: Alignment.center,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(32),
                child: Form(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _nombreCompleto(context),
                      _fechaNacimiento(context),
                      // Text("VERIFICAR NÚMERO DE TELÉFONO", style: TextStyle(color: Colors.lightBlue, fontSize: 36, fontWeight: FontWeight.w500),),
                      Row(
                        children: <Widget>[
                          CountryPickerDropdown(
                            initialValue: 'BO',
                            isExpanded: false,
                            itemBuilder: _buildDropdownItem,
                            // itemFilter:  ['AR', 'DE', 'GB', 'CN'].contains(c.isoCode),
                            priorityList: [
                              CountryPickerUtils.getCountryByIsoCode('BO'),
                              CountryPickerUtils.getCountryByIsoCode('CN'),
                            ],
                            sortComparator: (Country a, Country b) =>
                                a.isoCode.compareTo(b.isoCode),
                            onValuePicked: (Country country) {
                              extencionNumber = "+${country.phoneCode}";
                              // print("#### $extencionNumber");
                            },
                          ),
                          Expanded(
                            // flex: 2,
                            child: TextFormField(
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    borderSide:
                                        BorderSide(color: Colors.grey[200])),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    borderSide:
                                        BorderSide(color: Colors.grey[300])),
                                filled: true,
                                fillColor: Colors.grey[100],
                                labelText: "Número de teléfono",
                              ),
                              onChanged: (value) {
                                _userProv.phone = value;
                              },
                              keyboardType: TextInputType.number,
                              controller: _phoneController,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 8.0),
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: FlatButton(
                          color: Colors.green,
                          child: Text("VERIFICAR NÚMERO DE TELÉFONO"),
                          textColor: Colors.white,
                          padding: EdgeInsets.all(16),
                          onPressed: () {
                            // ignore: null_aware_before_operator
                            if (_userProv.fechaNac != null &&
                                _userProv.name != null &&
                                _userProv.phone?.length >= 8) {
                              print("## entro ");
                              final phone = extencionNumber +
                                  _phoneController.text.trim();
                              _userProv.phone = phone;

                              Flushbar(
                                title: "Cargando",
                                message:
                                    "Espere un momento mientras se completa la accion",
                                backgroundColor: Colors.green,
                                margin: EdgeInsets.all(8),
                                borderRadius: 8,
                                duration: Duration(seconds: 2),
                              ).show(context);
                              loginUser(phone, context);
                              // print("ACTIVO");
                            } else {
                              //                   Scaffold.of(context).showSnackBar(SnackBar(
                              //   content: Text("Sending Message"),
                              // ));
                            }
                          },
                          // color: Colors.blue,
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: PrivacyPolicy(),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }

  _fechaNacimiento(BuildContext context) {
    ProviderUser _userProv = Provider.of<ProviderUser>(context);
    // ignore: deprecated_member_use
    return FlatButton(
        onPressed: () {
          DatePicker.showDatePicker(context,
              showTitleActions: true,
              minTime: DateTime(1900, 1, 1),
              maxTime: DateTime(2020, 1, 1), onChanged: (date) {
            print('change $date');
          }, onConfirm: (date) {
            print('confirm $date');
            _userProv.fechaNac = date;
          }, currentTime: DateTime(1995, 9, 3), locale: LocaleType.es);
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 15.0),
          width: double.infinity,
          height: 50.0,
          decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.1),
              borderRadius: BorderRadius.circular(10.0)),
          child: _userProv.fechaNac != null
              ? Text(
                  "Fecha de nac: ${_userProv.getfechaNac()}",
                  style: TextStyle(color: Colors.grey[900]),
                )
              : Text(
                  "Fecha de nacimiento",
                  style: TextStyle(color: Colors.grey[600]),
                ),
          alignment: Alignment.center,
        ));
  }

  _nombreCompleto(BuildContext context) {
    ProviderUser _userProv = Provider.of<ProviderUser>(context);
    return TextFormField(
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            borderSide: BorderSide(color: Colors.grey[200])),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            borderSide: BorderSide(color: Colors.grey[300])),
        filled: true,
        fillColor: Colors.grey[100],
        labelText: "Nombre completo",
      ),
      keyboardType: TextInputType.text,
      controller: _nameController,
      onChanged: (value) {
        _userProv.name = value;
      },
    );
  }
}
