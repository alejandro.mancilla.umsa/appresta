import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:restorant_app/Api/ApiLocation.dart';
import 'package:restorant_app/Auth/LoginScreen.dart';
import 'package:restorant_app/Core/Utils.dart';
import 'package:restorant_app/Pages/HomeScreen.dart';

// ignore: must_be_immutable
class ChaskiSplashScreen extends StatefulWidget {
  @override
  _ChaskiSplashScreenState createState() => _ChaskiSplashScreenState();
}

class _ChaskiSplashScreenState extends State<ChaskiSplashScreen> {
  Widget one;
  ApiLocation apiLocation = ApiLocation();
  final prefs = UserPreferences();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen.navigate(
      name: 'assets/animations/chaski.flr',
      next: (_) => one,
      until: () async {
        one = await _verificaAuth();
        return Future.delayed(Duration(seconds: 2));
      },
      fit: BoxFit.contain,
      alignment: Alignment.center,
      startAnimation: "load",
      backgroundColor: Colors.white,
    );
  }

  Future<Widget> _verificaAuth() async {
    FirebaseAuth auth;
    Stream<User> usuario;
    User nu;
    print("###verifi# ");
    try {
      auth = FirebaseAuth.instance;
      usuario = auth.authStateChanges();
      nu = await usuario.first;
    } catch (e) {
      print("ERROR $e");
    }
    // print(" ####=> ${nu.uid} ####=> ${nu.phoneNumber} ####=> ${nu.displayName}");
    print(nu ?? "no pos nada");
    if (nu != null) {
      var partners = FirebaseFirestore.instance.collection('Parners');
      partners
          .doc(prefs.userGoogleId)
          .get()
          .then((DocumentSnapshot documentSnapshot) {
        if (documentSnapshot.exists) {
          partners.doc(prefs.userGoogleId).update({
            'tokenNotificationsParner': prefs.userTokenNotification,
          }).then((value) {});
        }
      });

      return HomeScreen(
        user: nu,
      );
    }
    return LoginScreen();
  }
}
