import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PrivacyPolicy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: TextStyle(fontSize: 13.0),
        children: [
          TextSpan(
            text: 'Al presionar "VERIFICAR NÚMERO DE TELÉFONO", indicas que aceptas nuestros ',
            style: TextStyle(color: Colors.black45),
          ),
          TextSpan(
            text: 'Terminos y Condiciones',
            style: TextStyle(
              color: Colors.green,
              decoration: TextDecoration.underline,
            ),
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                WidgetsBinding.instance.addPostFrameCallback((_){
                  _launchURL("https://remmcal.blogspot.com/2019/01/politica-de-privacidad-el-chaski.html");
                });
              },
          ),
          TextSpan(
            text: '. Es posible que se te envíe un SMS. Podrían aplicarse las tarifas de mensajes y datos',
            style: TextStyle(color: Colors.black45),
          ),
        ],
      ),
    );
  }

  _launchURL(String mUrl) async {
    if (!(mUrl.contains("http://") || mUrl.contains("https://"))) {
      mUrl = "http://" + mUrl;
    }
    if (await canLaunch(mUrl)) {
      await launch(mUrl);
    } else {
      throw 'Could not launch $mUrl';
    }
  }
}
