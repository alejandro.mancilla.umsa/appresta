import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Core/Models/ProductModelUnit.dart';
import 'package:restorant_app/Pages/EditProductPage.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';
import 'package:restorant_app/Widgets/CacheImageWidget.dart';

// ignore: must_be_immutable
class CardPlato extends StatefulWidget {
  final ProductModelUnit productModel;

  const CardPlato({Key key, this.productModel}) : super(key: key);

  @override
  _CardPlatoState createState() => _CardPlatoState();
}

class _CardPlatoState extends State<CardPlato> {
  ProviderRestaurant providerRestaurant;
  ApiFirebase apiFirebase = ApiFirebase();

  @override
  void initState() {
    providerRestaurant =
        Provider.of<ProviderRestaurant>(context, listen: false);
    print('''_________________________________
    ${this.widget.productModel}
    _____________________________________''');
    emergency = this.widget.productModel.emergency;
    obtenerDia();
    super.initState();
  }

  String dia = '';
  obtenerDia() async {
    dia = await providerRestaurant.getDia();
    Future.delayed(Duration.zero, () {
      if (this.widget.productModel.days.contains(dia)) {
        switchState = emergency ? false : true;
      } else {
        switchState = emergency ? true : false;
      }
      try {
        setState(() {});
      } catch (e) {}
    });
  }

  bool emergency = false;
  String zonee = '';
  String cityy = '';
  bool switchState = false; //TODO CAMBIAR DINAMICAMENTE

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black12, offset: Offset(0, 1), blurRadius: 10)
          ]),
      margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      contentPadding: EdgeInsets.all(0),
                      titlePadding: EdgeInsets.all(0),
                      buttonPadding: EdgeInsets.all(0),
                      actionsPadding: EdgeInsets.all(0),
                      insetPadding: EdgeInsets.all(30),
                      content:
                          EditProductPage(productModel: widget.productModel),
                    );
                  },
                );
              },
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: CacheImageWidget(
                      urlPhoto: widget.productModel.photo,
                      height: 100,
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(widget.productModel.name),
                        Slider(
                          value: widget.productModel.rangePriority,
                          onChanged: (value) async {
                            EasyLoading.show(
                              status: 'Cargando...',
                              dismissOnTap: false,
                            );
                            setState(() {});
                            widget.productModel.rangePriority = value;
                            await apiFirebase.changePriorityProduct(
                                widget.productModel.idProduct,
                                widget.productModel.rangePriority);
                            EasyLoading.dismiss();
                            // bool response =
                            //     await apiFirebase.changePriorityProduct(
                            //         widget.productModel.idProduct,
                            //         widget.productModel.rangePriority);
                            // print(response);
                            // if (response) {
                            //   EasyLoading.dismiss();
                            //   print('object1');
                            // } else {
                            //   print('object2');
                            //   EasyLoading.showError(
                            //       'Fallo el cambio de estado, revisa tu conexión a internet');
                            // }
                            // setState(() {});
                          },
                          min: 1,
                          max: 3,
                          divisions: 2,
                          label:
                              levelPriority(widget.productModel.rangePriority),
                        ),
                      ],
                    ),
                  )),
                ],
              ),
            ),
          ),
          Switch(
            value: switchState,
            onChanged: (value) async {
              bool stateResponse = await apiFirebase.changeEmergencyProduct(
                  this.widget.productModel.idProduct, !emergency);
              if (stateResponse) {
                switchState = value;
                emergency = !emergency;
              } else {
                Flushbar(
                  title: "Error",
                  message:
                      "No se pudo cambiar el estado, revise su coneccion a internet",
                  backgroundColor: Colors.red,
                  margin: EdgeInsets.all(8),
                  borderRadius: 8,
                  duration: Duration(seconds: 3),
                ).show(context);
              }
              setState(() {});
            },
          ),
          SizedBox(
            width: 10.0,
          )
        ],
      ),
    );
  }

  String levelPriority(double number) {
    switch (number.toInt()) {
      case 1:
        return "Normal";
        break;
      case 2:
        return "Prioridad";
        break;
      case 3:
        return "Promoción";
        break;
      default:
        return "Nivel de prioridad $number";
    }
  }
}
