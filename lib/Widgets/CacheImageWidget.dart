import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CacheImageWidget extends StatelessWidget {
  final String urlPhoto;
  final double width;
  final double height;
  final BoxFit fit;

  const CacheImageWidget(
      {Key key,
      this.urlPhoto,
      this.width,
      this.height,
      this.fit = BoxFit.cover})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: '$urlPhoto',
      width: width,
      height: height,
      fit: fit,
      placeholder: (context, url) => Image.asset(
        "assets/images/tenor.gif",
        width: width,
        height: height,
        fit: fit,
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
