import 'dart:async';

import 'package:bubble/bubble.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
// import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Core/Models/OrderFinalModel.dart';
import 'package:restorant_app/Core/Utils.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/Widgets/ConectInternet.dart';
import 'package:restorant_app/resources/App_styles.dart';
import 'package:restorant_app/resources/app_colors.dart';
import 'package:toast/toast.dart';
// import 'package:keyboard_visibility/keyboard_visibility.dart';

UserPreferences prefs = UserPreferences();

class ChatPage extends StatefulWidget {
  final OrderFinalModel orderFinalModel;
  final String nameBusiness;
  final String idBusiness;
  final double priceProducts;

  const ChatPage(
      {this.orderFinalModel,
      this.nameBusiness,
      this.priceProducts,
      @required this.idBusiness});

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final TextEditingController _messageController = TextEditingController();
  ProviderListPlatos providerListPlatos;

  @override
  void initState() {
    providerListPlatos =
        Provider.of<ProviderListPlatos>(context, listen: false);
    print('##### INIT STATE ##${widget.idBusiness}###');
    groupChatId =
        '${widget.orderFinalModel.tokenUser}-${widget.orderFinalModel.idOrder}';
    peerId = '${prefs.userGoogleId}-${widget.idBusiness}';
    id = 'Negocio - ${widget.nameBusiness}';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: _appBar(context),
        // bottomSheet: _bottomShet(),
        // body: _statusOrder(context),
        body: Column(
          children: [
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Container(
                color: Colors.blueGrey[900],
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(
                        'Detalles del pedido',
                        style: TextStyle(
                          color: lightPrimary,
                          fontSize: 14.0,
                        ),
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(
                        '${(widget.priceProducts)} Bs.',
                        style: TextStyle(
                            color: lightPrimary,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
            ),
            ConectInternet(),
            Expanded(
              child: ChatScreen(
                idOrder: '${widget.orderFinalModel.idOrder}',
                idChatOrder:
                    '${widget.orderFinalModel.tokenUser}-${widget.orderFinalModel.idOrder}',
                idBusiness: widget.idBusiness,
              ),
              // child: _userInformation(
              //   idOrder: '${this.widget.orderFinalModel.idOrder}',
              //   idChatOrder: '${prefs.useridUser}-${this.widget.orderFinalModel.idOrder}',
              // )
            ),
            if (!(widget.orderFinalModel.state == 'FINALIZADO' ||
                widget.orderFinalModel.state == 'CANCELADO'))
              _bottomShet()
          ],
        ),
      ),
    );
  }

  // print('####################################################');
  // Widget _statusOrder(BuildContext context) {
  //   return Container(
  //     // color: Colors.grey,
  //     height: MediaQuery.of(context).size.height-220-altoTeclado,
  //     child: _userInformation(idOrder: this.widget.orderFinalModel.idOrder,idChatOrder: '${prefs.useridUser}-${this.widget.orderFinalModel.idOrder}',)
  //   );
  // }

  AppBar _appBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.blueGrey[900],
      elevation: 0,
      centerTitle: true,
      iconTheme: IconThemeData(color: lightPrimary),
      title: Text(
        'Estado del pedido',
        style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(
            fontSize: 16.0, color: lightPrimary, fontWeight: FontWeight.w600)),
        overflow: TextOverflow.ellipsis,
      ),
      // actions: [
      //   Padding(
      //     padding: const EdgeInsets.all(16.0),
      //     child: GestureDetector(
      //       child: Icon(icon.error_outline),
      //       onTap: () {
      //         // HistoryOrdenDetails
      //         Navigator.push(
      //           context, MaterialPageRoute(builder: (BuildContext context) => HistoryOrdenDetails(orderFinalModel: this.widget.orderFinalModel,))
      //         );
      //       },
      //     ),
      //   )
      // ],
      // bottom: PreferredSize(
      //   preferredSize: Size.fromHeight(1.0),
      //   child: Container(color: Color(0xFFEAECEF), height: 1.0),
      // ),
    );
  }

  InkWell _itemStatus({String texto, bool isFinal = false, String aux}) {
    return InkWell(
      onTap: () {
        // CollectionReference users = FirebaseFirestore.instance.collection(this.widget.orderFinalModel.tokenUser);
        _enviandoPrimerMensaje(
            id: widget.orderFinalModel.idOrder,
            idCollection:
                '${widget.orderFinalModel.tokenUser}-${widget.orderFinalModel.idOrder}',
            type: 3,
            idFrom: 'status',
            role: 'Negocio - ${widget.nameBusiness}',
            content: '$texto');

        // if (isFinal) {
        //   var firestoreConsultas = FirestoreConsultas();

        //   // var ordenProvider = Provider.of<OrdenProvider>(context,listen: false);
        //   providerListPlatos.listaOrders = [];
        //   firestoreConsultas.consultaPedido(state: 'EN PROGRESO',ordenProvider: providerListPlatos);
        //   var order = FirebaseFirestore.instance.collection('Orders');
        //   order
        //   .doc(widget.orderFinalModel.idOrder)
        //   .update({'state': 'FINALIZADO'})
        //   .then((value) {
        //     print('Orders Updated');
        //     Future.delayed(
        //       Duration(milliseconds: 500),
        //       (){
        //         Navigator.pop(context);
        //         Navigator.pop(context);
        //       }
        //     );
        //   })
        //   .catchError((error) => print('Failed to update Orders: $error'));
        // }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
            color: isFinal ? Colors.red : Colors.grey.withOpacity(0.3),
            borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Text(
          '$aux - $texto',
          style: TextStyle(
              fontSize: 12, color: isFinal ? Colors.white : Colors.black),
        ),
      ),
    );
  }

  void _enviandoPrimerMensaje(
      {String id,
      String idCollection,
      String role,
      String idFrom,
      String content,
      int type}) {
    /////////////////////////////////////////////////////////////
    ///[ENVIANDO MENSAJES]
    /////////////////////////////////////////////////////////////
    var documentReference = FirebaseFirestore.instance
        .collection('Orders')
        .doc(id)
        .collection('$idCollection')
        .doc(DateTime.now().millisecondsSinceEpoch.toString());
    // var documentReferenceMess = FirebaseFirestore.instance
    //     .collection('messagesRef')
    //     .doc(peerId);

    FirebaseFirestore.instance.runTransaction((transaction) async {
      transaction.set(
        documentReference,
        {
          'role': role,
          'idFrom': idFrom,
          'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
          'content': content,
          'type': type
        },
      );
    });
    /////////////////////////////////////////////////////////////
    ///[ENVIANDO MENSAJES]
    //////////////////////////////////////////////////////////////
  }

  Container _bottomShet() {
    return Container(
      height: 150,
      // color: Colors.red,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            height: 75,
            child: Wrap(
              alignment: WrapAlignment.center,
              children: [
                _itemStatus(texto: 'Pedido aceptado', aux: '1'),
                _itemStatus(texto: 'Preparando pedido', aux: '2'),
                _itemStatus(texto: 'Pedido preparado', aux: '3'),
                _itemStatus(texto: 'Pedido despachado', aux: '4'),
              ],
            ),
          ),
          // GestureDetector(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Container(
          //     color: Colors.blue,
          //     child: Row(
          //       children: [
          //         Padding(
          //           padding: const EdgeInsets.all(16.0),
          //           child: Text(
          //             'Detalles del pedido',
          //             style: TextStyle(color: lightPrimary, fontSize: 14.0),
          //           ),
          //         ),
          //         Spacer(),
          //         Padding(
          //           padding: const EdgeInsets.all(16.0),
          //           child: Text(
          //             '${widget.priceProducts} Bs.',
          //             style: TextStyle(
          //               color: lightPrimary,
          //               fontSize: 14.0,
          //             ),
          //           ),
          //         )
          //       ],
          //     ),
          //   ),
          // ),
          // if (widget.orderFinalModel.state != 'FINALIZADO')
          Expanded(
            child: Container(
              color: lightPrimary,
              child: Padding(
                // padding: const EdgeInsets.only(top: 8, left: 8,bottom: 8,right: 8),
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                            color: Colors.grey[100],
                            borderRadius: BorderRadius.circular(10)),
                        child: TextField(
                          cursorColor: darkAccent,
                          maxLength: 80,
                          decoration: InputDecoration(
                            counterText: '',
                            errorMaxLines: 2,
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 10),
                            labelText: 'Pregunta lo que quieras',
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: lightPrimary),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: lightPrimary),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: lightAccent),
                            ),
                            errorStyle:
                                TextStyle(fontSize: 10.0, color: Colors.red),
                          ),
                          controller: _messageController,
                          maxLines: null,
                          style: TextStyle(
                            fontSize: 14.0,
                          ),
                          keyboardType: TextInputType.text,
                          /*onChanged: (value) {
                             changeInput(value);
                           }*/
                        ),
                      ),
                    ),
                    Container(
                        // height: 44.0,
                        margin: EdgeInsets.only(left: 8.0),
                        // ignore: deprecated_member_use
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100)),
                          child: IconButton(
                              color: Colors.white,
                              icon: Icon(Icons.send_sharp),
                              onPressed: () {
                                onSendMessage('${_messageController.text}', 1);
                              }),
                        )),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String groupChatId = 'pruebaprueba';
  String peerId = 'peerId';
  String peerAvatar = 'peerAvatar';
  String id = 'id';
  double altoTeclado = 0;
  final ScrollController listScrollController = ScrollController();
  final TextEditingController textEditingController = TextEditingController();

  // @override
  // void initState() {
  //   print('##### INIT STATE #####');
  //   groupChatId = '${widget.orderFinalModel.tokenUser}-${widget.orderFinalModel.idOrder}';
  //   peerId = '${prefs.userGoogleId}';
  //   id = 'Repartidor';
  //   super.initState();
  // }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('##### DIDCHANGE #####');
  }

  void onSendMessage(String content, int type) {
    // content = 'mensaje de prueba';
    // type: 1 = normal message,
    // type: 2 = status,
    // type: 3 = origin, [RESTAURANTE, REPARTIDOR, ]
    // type: 4 = ,
    if (content.trim() != '') {
      _messageController.clear();

      var documentReference = FirebaseFirestore.instance
          .collection('Orders')
          .doc(widget.orderFinalModel.idOrder)
          .collection(groupChatId)
          .doc(DateTime.now().millisecondsSinceEpoch.toString());

      FirebaseFirestore.instance.runTransaction((transaction) async {
        transaction.set(
          documentReference,
          {
            'role': id,
            'idFrom': peerId,
            'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
            'content': content,
            'type': type,
          },
        );
      });
    } else {
      Toast.show('No hay nada que enviar.', context,
          duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM,
          backgroundColor: Colors.black54);
    }
  }

  @override
  void dispose() {
    print('#### DISPOSE ######');
    listScrollController.dispose();
    super.dispose();
  }

  Container itemTarget(BuildContext context,
      {String svgImage, String role, String name}) {
    return Container(
        child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 44.0,
          width: 44.0,
          //padding: EdgeInsets.symmetric(horizontal: 2.0),
          child: SvgPicture.asset(
            '$svgImage',
            // 'assets/icon/restaurant_chaski.svg',
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '$role',
                // ignore: deprecated_member_use
                style: Theme.of(context)
                    .textTheme
                    .title
                    .merge(TextStyle(fontSize: 16.0)),
              ),
              Text(
                '$name',
                // ignore: deprecated_member_use
                style: Theme.of(context)
                    .textTheme
                    .title
                    .merge(TextStyle(fontSize: 16.0)),
              ),
            ],
          ),
        ),
        Spacer(),
      ],
    ));
  }
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
///

class ChatScreen extends StatefulWidget {
  final String idOrder;
  final String idChatOrder;
  final String idBusiness;

  const ChatScreen({Key key, this.idOrder, this.idChatOrder, this.idBusiness})
      : super(key: key);

  @override
  State createState() => ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> {
  String id;

  // List<QueryDocumentSnapshot> listMessage = new List.from([]);
  int _limit = 20;
  final int _limitIncrement = 20;
  String groupChatId;

  double altoTeclado = 0;
  bool isLoading;
  bool isShowSticker;
  // String imageUrl;

  final TextEditingController textEditingController = TextEditingController();
  final ScrollController listScrollController = ScrollController();
  final FocusNode focusNode = FocusNode();

  void _scrollListener() {
    if (listScrollController.offset >=
            listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange) {
      print('reach the bottom');
      setState(() {
        print('reach the bottom');
        _limit += _limitIncrement;
      });
    }
    if (listScrollController.offset <=
            listScrollController.position.minScrollExtent &&
        !listScrollController.position.outOfRange) {
      print('reach the top');
      setState(() {
        print('reach the top');
      });
    }
  }

  @override
  void initState() {
    super.initState();
    focusNode.addListener(onFocusChange);
    listScrollController.addListener(_scrollListener);

    // groupChatId = '';
    print(
        ' ####XXX### ${widget.idOrder} ####XXX### ${widget.idChatOrder} ####XXX###');
    isLoading = false;
    isShowSticker = false;
  }

  void onFocusChange() {
    if (focusNode.hasFocus) {
      // Hide sticker when keyboard appear
      setState(() {
        isShowSticker = false;
      });
    }
  }

  // ignore: missing_return
  Widget buildItem({int index, DocumentSnapshot document}) {
    // print('''
    // #################################
    // ${document.data()}
    // #################################
    // ''');

    var fecha = DateTime.fromMillisecondsSinceEpoch(
        int.parse(document.data()['timestamp']));
    var month = fecha.month.toString().padLeft(2, '0');
    var day = fecha.day.toString().padLeft(2, '0');
    var year = fecha.year.toString().padLeft(2, '0');
    var hour = fecha.hour.toString().padLeft(2, '0');
    var minute = fecha.minute.toString().padLeft(2, '0');

    var fechaNow = DateTime.now();
    var monthNow = fechaNow.month.toString().padLeft(2, '0');
    var dayNow = fechaNow.day.toString().padLeft(2, '0');
    var yearNow = fechaNow.year.toString().padLeft(2, '0');

    var fechaVisible = '';

    if ('$day/$month/$year' == '$dayNow/$monthNow/$yearNow') {
      fechaVisible = '$hour:$minute';
    } else {
      fechaVisible = '$day/$month/$year - $hour:$minute';
    }

    if (document.data()['idFrom'] ==
        '${prefs.userGoogleId}-${widget.idBusiness}') {
      return ListTile(
        title: Bubble(
          margin: BubbleEdges.only(top: 20),
          alignment: Alignment.topRight,
          nipWidth: 8,
          nipHeight: 10,
          nip: BubbleNip.rightTop,
          color: Color.fromRGBO(225, 255, 199, 1.0),
          child: Text(document.data()['content']),
        ),
        subtitle: Row(
          children: [
            Expanded(child: Container()),
            Text('$fechaVisible    ', style: dateText),
          ],
        ),
      );
    } else if (document.data()['idFrom'] == 'ADMIN') {
      String svgImg;
      Color colorStatus;

      switch (document.data()['content']) {
        case 'Negocio':
          svgImg = 'assets/icon/restaurant_chaski.svg';
          colorStatus = Colors.blue[50];
          break;
        case 'Repartidor':
          svgImg = 'assets/icon/rider_chaski.svg';
          colorStatus = Colors.orange[50];
          break;
        case 'Cliente':
          svgImg = 'assets/icon/boy.svg';
          colorStatus = Colors.green[50];
          break;
        default:
          svgImg = 'assets/icon/restaurant_chaski.svg';
          colorStatus = Colors.blue[50];
          break;
      }

      if (document.data()['content'] != 'Repartidor') {
        return ListTile(
          title: itemTarget(context,
              role: '${document.data()['content']}',
              name: '${document.data()['role']}',
              svgImage: svgImg,
              colorpoint: colorStatus,
              coordinates: '${document.data()['coordenates']}'),
        );
      } else {
        return Container();
      }
    } else {
      if (document.data()['type'] == 1) {
        return ListTile(
          title: Bubble(
            margin: BubbleEdges.only(top: 10),
            alignment: Alignment.topLeft,
            nipWidth: 8,
            nipHeight: 10,
            nip: BubbleNip.leftTop,
            child: RichText(
              text: TextSpan(style: defaltText15, children: <TextSpan>[
                TextSpan(
                  text: '${document.data()['role']}\n',
                  style: defaltTextBold10,
                ),
                TextSpan(
                  text: '${document.data()['content']}',
                  style: defaltText15,
                ),
              ]),
            ),
            // Text('hola\n${document.data()['content']}'),
          ),
          subtitle: Row(
            children: [
              Text(
                '    $fechaVisible',
                style: dateText,
              ),
              Expanded(child: Container()),
            ],
          ),
        );
      } else if (document.data()['type'] == 2) {
        var colorStatus = Colors.white;
        // switch ('${document.data()['content']}') {
        //   case 'Pedido iniciado':
        //     colorStatus = Colors.white;
        //     break;
        //   case 'Preparando el pedido':
        //     colorStatus = Colors.blue[50];
        //     break;
        //   case 'Pedido en camino':
        //     colorStatus = Colors.green[50];
        //     break;
        //   default:
        //     colorStatus = Colors.orange[50];
        //     break;
        // }
        return itemBuble(colorStatus, document, fechaVisible);
      } else if (document.data()['type'] == 3) {
        var colorStatus = Colors.blue[50];
        // switch ('${document.data()['content']}') {
        //   case 'Pedido iniciado':
        //     colorStatus = Colors.white;
        //     break;
        //   case 'Preparando el pedido':
        //     colorStatus = Colors.blue[50];
        //     break;
        //   case 'Pedido en camino':
        //     colorStatus = Colors.green[50];
        //     break;
        //   default:
        //     colorStatus = Colors.orange[50];
        //     break;
        // }
        return itemBuble(colorStatus, document, fechaVisible);
      } else if (document.data()['type'] == 4) {
        var colorStatus = Colors.orange[50];
        // switch ('${document.data()['content']}') {
        //   case 'Pedido iniciado':
        //     colorStatus = Colors.white;
        //     break;
        //   case 'Preparando el pedido':
        //     colorStatus = Colors.blue[50];
        //     break;
        //   case 'Pedido en camino':
        //     colorStatus = Colors.green[50];
        //     break;
        //   default:
        //     colorStatus = Colors.orange[50];
        //     break;
        // }
        return itemBuble(colorStatus, document, fechaVisible);
      }
    }
  }

  Bubble itemBuble(
      Color colorStatus, DocumentSnapshot document, String fechaVisible) {
    return Bubble(
      margin: BubbleEdges.only(top: 10, left: 15, right: 15),
      padding: BubbleEdges.symmetric(vertical: 10),
      alignment: Alignment.center,
      nip: BubbleNip.no,
      color: colorStatus,
      child: Container(
        width: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: Column(
                  children: [
                    Text(
                      '${document.data()['role']}',
                      textAlign: TextAlign.start,
                      style: defaltText,
                    ),
                    Text(
                      '${document.data()['content']}',
                      textAlign: TextAlign.center,
                      style: defaltTextSemiBold15,
                    ),
                  ],
                )),
            Positioned(
                bottom: 0,
                right: 0,
                child: Text(
                  '$fechaVisible',
                  style: dateText,
                )),
          ],
        ),
      ),
    );
  }

  // Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Container itemTarget(BuildContext context,
      {String svgImage,
      String role,
      String name,
      Color colorpoint,
      String coordinates = ''}) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12, blurRadius: 2, offset: Offset(1, 1))
            ]),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 44.0,
              width: 44.0,
              //padding: EdgeInsets.symmetric(horizontal: 2.0),
              child: SvgPicture.asset(
                '$svgImage',
                // 'assets/icon/restaurant_chaski.svg',
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '$role',
                      // ignore: deprecated_member_use
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1
                          .merge(TextStyle(fontSize: 10.0)),
                    ),
                    Text(
                      '$name',
                      // ignore: deprecated_member_use
                      style: Theme.of(context)
                          .textTheme
                          .title
                          .merge(TextStyle(fontSize: 14.0)),
                    ),
                  ],
                ),
              ),
            ),
            // Container(
            //   // color: Colors.blue,
            //   // height: 20,s
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   child: ElevatedButton(
            //       onPressed: () {
            //         // Navigator.push(
            //         //   context,
            //         //   MaterialPageRoute(builder: (BuildContext context) => _MapaDirection(direction: coordinates,name: name,))
            //         // );
            //         // showDialog(
            //         //   context: context,
            //         //   builder: (context) {
            //         //     return AlertDialog(
            //         //       titlePadding: EdgeInsets.all(0),
            //         //       contentPadding: EdgeInsets.all(0),
            //         //       title: Text('data'),
            //         //       content: ,
            //         //     );
            //         //   },
            //         // );
            //       },
            //       style: ButtonStyle(
            //           backgroundColor: MaterialStateProperty.all(Colors.white),
            //           padding: MaterialStateProperty.all(
            //               EdgeInsets.symmetric(vertical: 2, horizontal: 5)),
            //           elevation: MaterialStateProperty.all(0)),
            //       child: Text(
            //         'Dirección',
            //         textAlign: TextAlign.right,
            //         style: TextStyle(
            //             fontWeight: FontWeight.bold, color: Colors.blue),
            //       )),
            // ),
            Container(
              decoration:
                  BoxDecoration(color: colorpoint, shape: BoxShape.circle),
              width: 15,
              height: 15,
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(false);
      },
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              // List of messages
              Container(
                  // color: Colors.red,
                  child: buildListMessage()),
            ],
          ),
          buildLoading()
        ],
      ),
    );
  }

  Widget buildLoading() {
    return Positioned(
      child: isLoading ? const CircularProgressIndicator() : Container(),
    );
  }

  Widget buildListMessage() {
    return Flexible(
      child: widget.idChatOrder == ''
          ? Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.orange)))
          : StreamBuilder(
              stream: FirebaseFirestore.instance
                  .collection('Orders')
                  .doc('${widget.idOrder}')
                  .collection('${widget.idChatOrder}')
                  .orderBy('timestamp', descending: true)
                  .limit(_limit)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.orange)));
                } else {
                  QuerySnapshot querySnapshot = snapshot.data;
                  // print('XXXXXXX ${querySnapshot.} XXXXXXXXXXX');
                  // listMessage.addAll(snapshot.data.documents);
                  return ListView.builder(
                    // padding: EdgeInsets.all(10.0),
                    itemBuilder: (context, index) {
                      // print('HHHHHHHHHHHHHHH  $index  HHHHHHHHHHHHHHHHH');
                      return buildItem(document: querySnapshot.docs[index]);
                    },
                    // buildItem(index, snapshot.data.documents[index]),
                    itemCount: querySnapshot.docs.length,
                    // itemCount: snapshot.data.documents.length,
                    reverse: true,
                    controller: listScrollController,
                  );
                }
              },
            ),
    );
  }
}

// class _MapaDirection extends StatefulWidget {
//   final String direction;
//   final String name;
//   const _MapaDirection({Key key, @required this.direction, this.name}) : super(key: key);
//   @override
//   __MapaDirectionState createState() => __MapaDirectionState();
// }

// class __MapaDirectionState extends State<_MapaDirection> {

//   final Set<Marker> _markers = {};
//   final Completer<GoogleMapController> _controller = Completer();
//   List lista;
//   LatLng latLng;
//   MarkerId markerId = MarkerId('restaurant');
//   Marker marker ;

//   @override
//   void initState() {
//     lista = widget.direction.replaceAll(' ', '').split(',');
//     marker = Marker(
//       markerId: markerId,
//       position: latLng
//     );
//     latLng = LatLng( double.parse(lista[0]),  double.parse(lista[1]));
//     super.initState();
//   }
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Dirección ${widget.name}'),
//       ),
//       body: Container(
//       child: GoogleMap(
//         mapType: MapType.normal,
//         initialCameraPosition: CameraPosition(
//           target: latLng,
//           zoom: 14.7,
//         ),
//         // scrollGesturesEnabled: false,
//         markers: _markers,
//         onMapCreated: (GoogleMapController controller) {
//           _controller.complete(controller);
//           setState(() {
//             _markers.add(
//                 Marker(
//                   markerId: MarkerId('Rest'),
//                   position: latLng,
//                 )
//             );
//           });
//         }
//       ),
//     ),
//     );

//   }
// }
