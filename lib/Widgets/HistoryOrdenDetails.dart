import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:restorant_app/Core/Models/OrderFinalModel.dart';
import 'package:restorant_app/Core/Models/ProductModel.dart';
import 'package:restorant_app/Widgets/Chat/ChatPage.dart';
import 'package:restorant_app/Widgets/ConectInternet.dart';
import 'package:restorant_app/main.dart';
import 'package:restorant_app/resources/App_styles.dart';

class HistoryOrdenDetails extends StatefulWidget {
  final String idBusiness;
  final String nameBusiness;
  final OrderFinalModel orderFinalModel;

  const HistoryOrdenDetails(
      {Key key,
      this.orderFinalModel,
      this.idBusiness,
      @required this.nameBusiness})
      : super(key: key);
  @override
  _HistoryOrdenDetailsState createState() => _HistoryOrdenDetailsState();
}

class _HistoryOrdenDetailsState extends State<HistoryOrdenDetails> {
  // OrderModelProvider orderModelProvider ;
  final Size size = Size(double.infinity, 50);

  @override
  void initState() {
    print('''
    __________________________________________
    ${this.widget.idBusiness}
    ${this.widget.orderFinalModel}
    __________________________________________
    ''');
    super.initState();
  }

  // void readUsuario(){
  //   // ${this.widget.orderFinalModel.tokenUser}
  //   FirebaseFirestore.instance
  //   .collection('Users')
  //   .where('idUser', isEqualTo: '${widget.orderFinalModel.tokenUser}')
  //   .limit(1)
  //   .get()
  //   .then((QuerySnapshot querySnapshot) {
  //       // querySnapshot.docs.forEach((doc) {
  //       //     print(doc['first_name']);
  //       // });
  //       print('PPPPPPPPPPPPPPPPPP ${querySnapshot.docs.single.data()}');
  //       imageUrl = querySnapshot.docs.single.data()['housePhoto']??'';
  //       print('PPPPPPPxxxxxxxxxxxxPPPPPPPPPPP ${querySnapshot.docs.single.data()['housePhoto']}');
  //       setState(() {

  //       });
  //   });
  // }
  File imageFile;
  bool isLoading;
  String imageUrl = '';
  // Future getImage() async {
  //   var imagePicker = ImagePicker();
  //   PickedFile pickedFile;

  //   try {
  //     pickedFile = await imagePicker.getImage(source: ImageSource.camera,imageQuality: 40);
  //     imageFile = File(pickedFile.path);
  //   // ignore: empty_catches
  //   } catch (e) {
  //   }

  //   if (imageFile != null) {
  //     setState(() {
  //       imageUrl = '';
  //       isLoading = true;
  //     });
  //     await uploadFile();
  //   }
  // }
  CollectionReference users = FirebaseFirestore.instance.collection('Users');

  Future uploadFile() async {
    var fileName = DateTime.now().millisecondsSinceEpoch.toString();

    var reference =
        firebase_storage.FirebaseStorage.instance.ref().child(fileName);

    try {
      await reference.putFile(imageFile).then((valor) {
        var storageTaskSnapshot = valor;

        storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
          imageUrl = downloadUrl;
          users
              .doc(widget.orderFinalModel.tokenUser)
              .update({'housePhoto': imageUrl})
              .then((value) => print('User Updated'))
              .catchError((error) => print('Failed to update user: $error'));

          print('################ $imageUrl ################3');
          setState(() {
            isLoading = false;
            //   onSendMessage(imageUrl, 1);
          });
        }, onError: (err) {
          // setState(() {
          isLoading = false;
          // });
          // Fluttertoast.showToast(msg: 'This file is not an image');
        });

        print('''
            ### $valor
            ''');
      });
    } on FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
      print(e);
    }

    // UploadTask uploadTask = reference.putFile(imageFile);
    // // TaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    // TaskSnapshot storageTaskSnapshot = uploadTask.snapshot;
    // storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
    //   imageUrl = downloadUrl;

    //   print('################ $imageUrl ################3');
    //   // setState(() {
    //     isLoading = false;
    //   //   onSendMessage(imageUrl, 1);
    //   // });
    // }, onError: (err) {
    //   // setState(() {
    //     isLoading = false;
    //   // });
    //   // Fluttertoast.showToast(msg: 'This file is not an image');
    // });
  }

  List<Orden> listaPedidos = [];

  List<String> precioFinal = [];
  double precioProduct = 0.0;
  @override
  void didChangeDependencies() {
    // orderModelProvider = Provider.of<OrderModelProvider>(context,listen: true);
    listaPedidos = widget.orderFinalModel.orden;
    // print('#######$precioFinal#########xx##########xx##############');
    // precioProduct = (widget.orderFinalModel.totalCost - widget.orderFinalModel.shippingCost).toString();
    // precioFinal = precioProduct.split('.');//(orderModelProvider.conteoPrecio() + orderModelProvider.deliveryPrice).toString().split('.');
    // precioProduct = '${precioFinal[0]}.${(precioFinal[1]+'000').toString().substring(0,2)}';
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    precioProduct = 0.0;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        // appBar: PreferredSize(
        //     child: AppBarPedido(),
        //     preferredSize: size,
        //   ),
        appBar: AppBar(
          title: Text('Detalles del pedido'),
          textTheme: TextTheme(
              headline6: TextStyle(color: Colors.white, fontSize: 20)),
        ),
        body: Container(
          color: Colors.white,
          // padding: EdgeInsets.symmetric(horizontal: ),
          child: SingleChildScrollView(
            child: Column(children: [
              ConectInternet(),
              // Text('${widget.orderFinalModel.direction}',style: TextStyle(fontSize: 16),),
              // Divider(),
              ..._widgetByRestaurant() ??
                  [
                    Container(
                      color: Colors.orange,
                    )
                  ],
              SizedBox(
                height: 100,
              )
            ]),
          ),
        ),
        bottomSheet: Container(
          height: 100,
          // color: Colors.red,
          child: Column(
            children: [
              Expanded(
                  child: Container(
                width: double.infinity,
                // color: Colors.red,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          'Productos',
                          style: defaltTextBold15,
                        )),
                        // Text('${orderModelProvider.conteoPrecio()} Bs.'),//conteoPrecio
                        Text(
                          '${format(precioProduct)} Bs.',
                          style: defaltTextBold15,
                        ), //conteoPrecio
                      ],
                    ),

                    // Row(
                    //   children: [
                    //     Expanded(child: Text('Costo Delivery')),
                    //     Text('${widget.orderFinalModel.shippingCost} Bs.'),
                    //     // Text('${orderModelProvider.deliveryPrice} Bs.'),
                    //   ],
                    // ),

                    // Row(
                    //   children: [
                    //     Expanded(child: Text('Total',style: defaltTextBold15,)),
                    //     Text('${widget.orderFinalModel.totalCost} Bs.',style: defaltTextBold15,),
                    //     // Text('${orderModelProvider.deliveryPrice} Bs.',style: defaltTextBold,),
                    //   ],
                    // ),
                  ],
                ),
              )),
              Expanded(
                  child: CupertinoButton(
                onPressed: () {
                  // print('''
                  // ${this.widget.orden.product.options.}
                  // ''');
                  // ChatPage
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => ChatPage(
                                orderFinalModel: widget.orderFinalModel,
                                nameBusiness: widget.nameBusiness,
                                priceProducts: precioProduct,
                                idBusiness: widget.idBusiness,
                              )));
                },
                color: Colors.green,
                child: Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child: Text('CHAT')),
              ))
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _widgetByRestaurant() {
    precioProduct = 0.0;
    var ret = <Widget>[];
    // print('## => $listaPedidos');
    listaPedidos?.forEach((Orden element) {
      if (element.idBusiness == widget.idBusiness) {
        // print('### => $element');
        ret.add(Column(
          children: [
            SizedBox(
              height: 5,
            ),
            Text(
              '${element.nameBusiness}',
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  color: Colors.blueGrey[900]),
            ),
            ...element.order.map((PedidoDetails e) {
                  precioProduct = precioProduct + (e.price);

                  print('#### => ${e.price}');
                  return ItemPlatoAux(
                    orden: e,
                  );
                }).toList() ??
                [],
            Divider()
          ],
        ));
      }
    });
    return ret;
  }
}

class ItemPlatoAux extends StatefulWidget {
  final PedidoDetails orden;
  // final OrderModelProvider orderModelProvider;
  ItemPlatoAux({@required this.orden});

  @override
  _ItemPlatoAuxState createState() => _ItemPlatoAuxState();
}

class _ItemPlatoAuxState extends State<ItemPlatoAux> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.blueGrey.withOpacity(0.1),
          borderRadius: BorderRadius.circular(10)),
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                // child: Icon(Icons.close_rounded,color: Colors.grey,),
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                // margin: EdgeInsets.only(right: 5),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${widget.orden.amount} x ${widget.orden.product.name}',
                      style: pedidoItemTitle,
                    ),
                    Text(
                      '${widget.orden.amount} x ${widget.orden.product.price} Bs.',
                      style: pedidoItemSubTitle,
                    ),
                  ],
                ),
              ),
              Text(
                '${widget.orden.price} Bs.',
                style: pedidoItemPrice,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          ...listOpciones()
        ],
      ),
    );
  }

  listOpciones() {
    print(
        '====== options ${this.widget.orden.product.options.length} == ${this.widget.orden.product.name}');
    List<Widget> listaOptions = [];
    this.widget.orden.product.options.forEach((opcion) {
      String option = '${opcion.title}: ';
      List<Widget> optionList = [];
      opcion.options.forEach((element) {
        // option = option + '${element.name}, ';
        optionList.add(Container(
          padding: EdgeInsets.all(2.5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.grey)),
          margin: EdgeInsets.all(2.5),
          child: Text(element.name,
              style: TextStyle(color: Colors.grey, fontSize: 12)),
        ));
      });
      // listaOptions.add(
      //     Text(option, style: TextStyle(color: Colors.grey, fontSize: 12)));
      listaOptions.add(Container(
        child: Row(
          children: [
            Text(option, style: TextStyle(color: Colors.grey, fontSize: 12)),
            ...optionList
          ],
        ),
      ));
    });
    return listaOptions;
  }
}
