import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class ConectInternet extends StatefulWidget {
  @override
  _ConectInternetState createState() => _ConectInternetState();
}

class _ConectInternetState extends State<ConectInternet> {
  Connectivity connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool conectivity = false;
  Widget estaRed = Container(
    child: CircularProgressIndicator(),
  );

  @override
  void initState() {
    connectivity = Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      print("object $result");
      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile ||
          result == ConnectivityResult.none) {
        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {
          estaRed = Container(
            height: 2,
            width: double.infinity,
            color: Colors.green.withOpacity(0.2),
          );
        } else {
          estaRed = Container(
            alignment: Alignment.center,
            color: Colors.red,
            width: double.infinity,
            height: 30,
            child: Text(
              "REVISA TU CONECCION A INTERNET",
              style: TextStyle(color: Colors.white),
            ),
          );
        }
        setState(() {});
      }
    });
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    ConnectivityResult result = await connectivity.checkConnectivity();
    if (result == ConnectivityResult.wifi ||
        result == ConnectivityResult.mobile) {
      estaRed = Container(
        height: 2,
        width: double.infinity,
        color: Colors.green.withOpacity(0.2),
      );
    } else {
      estaRed = Container(
        alignment: Alignment.center,
        color: Colors.red,
        width: double.infinity,
        height: 30,
        child: Text(
          "REVISA TU CONECCION A INTERNET",
          style: TextStyle(color: Colors.white),
        ),
      );
    }
    setState(() {});
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return estaRed;
  }
}
