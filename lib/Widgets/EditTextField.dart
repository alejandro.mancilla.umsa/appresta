import 'package:flutter/material.dart';

// ignore: must_be_immutable
class EditTextField extends StatefulWidget {
  TextEditingController tituloController;
  final Color color;
  final String label;
  final String helpText;
  final bool isNumber;
  final IconData icono;
  final bool showIcon;
  bool stateEdit;
  EditTextField(
      {Key key,
      this.color = Colors.grey,
      @required this.label,
      @required this.tituloController,
      this.stateEdit = true,
      this.helpText,
      this.isNumber = false,
      this.icono = Icons.edit,
      this.showIcon = true})
      : super(key: key);

  @override
  _EditTextFieldState createState() => _EditTextFieldState();
}

class _EditTextFieldState extends State<EditTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, right: 15),
      child: Row(
        children: [
          if (widget.showIcon)
            IconButton(
              icon: Icon(this.widget.icono),
              // icon: Icon(this.widget.stateEdit ? Icons.check : Icons.edit),
              // padding: EdgeInsets.symmetric(vertical: 10,horizontal: 1),
              onPressed: () {
                // print("object");
                // this.widget.stateEdit = !this.widget.stateEdit;
                // setState(() {});
              },
            ),
          Expanded(
            child: TextFormField(
              // enabled: this.widget.stateEdit,
              // maxLength: 20,
              keyboardType: this.widget.isNumber
                  ? TextInputType.number
                  : TextInputType.text,
              controller: this.widget.tituloController,
              cursorColor: this.widget.color.withOpacity(0.2),
              decoration: InputDecoration(
                  // enabled: this.widget.stateEdit,
                  fillColor: Colors.black.withOpacity(0),
                  filled: true,
                  // prefixIcon: Icon(Icons.email, color: Colors.green.shade900),
                  // suffixIcon: IconButton(icon: Icon(Icons.check), onPressed: (){
                  //   print("object");
                  //   this.widget.stateEdit = !this.widget.stateEdit;
                  //   setState(() {

                  //   });
                  // }) ,
                  // Icon(Icons.clear, color: Colors.green.shade100),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        color: this.widget.color.withOpacity(0.2),
                        style: BorderStyle.solid,
                        width: 2),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        color: this.widget.color.withOpacity(0.2),
                        style: BorderStyle.solid,
                        width: 2),
                  ),
                  labelText: this.widget.label,
                  labelStyle: TextStyle(color: Colors.blueGrey),
                  helperText: this.widget.helpText),
            ),
          ),
        ],
      ),
    );
  }
}
