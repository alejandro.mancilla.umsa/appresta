import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/ApiConection.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';

// import 'package:provider/provider.dart';
// import 'package:restorant_app/Providers/ProviderListPlatos.dart';
// ignore: must_be_immutable
class AppBarWidget extends StatefulWidget {
  final String idRest;
  final String name;
  bool statusEmergency;
  bool statusRes;
  AppBarWidget(
      {@required this.idRest,
      @required this.statusRes,
      @required this.name,
      @required this.statusEmergency});
  // bool _estadoTienda = this.statusRes;

  @override
  _AppBarWidgetState createState() => _AppBarWidgetState();
}

class _AppBarWidgetState extends State<AppBarWidget> {
  ProviderRestaurant providerRestaurant;
  ApiFirebase firestoreApi = ApiFirebase();
  ProviderListPlatos providerListPlatos;

  @override
  void initState() {
    providerListPlatos =
        Provider.of<ProviderListPlatos>(context, listen: false);
    providerRestaurant =
        Provider.of<ProviderRestaurant>(context, listen: false);
    super.initState();
    // print('STATUS ______________ ${widget.statusEmergency}');
  }

  @override
  Widget build(BuildContext context) {
    // ProviderListPlatos _listPlatos = Provider.of<ProviderListPlatos>(context);
    // Widget _textoStateAbierto = Text(
    //   "ABIERTO",
    //   style: TextStyle(
    //       color: Theme.of(context).primaryColor,
    //       fontSize: 18.0,
    //       fontWeight: FontWeight.normal),
    // );
    // Widget _textoStateCerrado = Text(
    //   "CERRADO",
    //   style: TextStyle(
    //       color: Colors.grey, fontSize: 18.0, fontWeight: FontWeight.normal),
    // );
    return Card(
      margin: EdgeInsets.all(0),
      elevation: 0.0,
      child: Row(
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                // providerListPlatos.listaOrders = [];
                // providerListPlatos.listaOrdersCancelado = [];
                // providerListPlatos.listaOrdersEnProgreso = [];
                // providerListPlatos.listaOrdersFinalizado = [];

                Navigator.pop(context);
              }),
          Expanded(
              child: Text(
            "${widget.name}",
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 18.0),
          )),
          // InkWell(
          //   onTap: () {
          //     //

          //     showDialog(
          //         context: context,
          //         barrierDismissible: false,
          //         builder: (context) {
          //           return AlertDialog(
          //             title: Text(
          //                 "Estas seguro que quieres ${widget.statusRes ? "Cerrar" : "Abrir"} tu restaurante?"),
          //             content: Container(
          //               height: 1.0,
          //             ),
          //             actions: <Widget>[
          //               // ignore: deprecated_member_use
          //               FlatButton(
          //                 child: Text("CONFIRMAR"),
          //                 textColor: Colors.white,
          //                 color: Colors.green,
          //                 onPressed: () {
          //                   //changeStatusBusiness
          //                   // print(
          //                   //     '______${widget.idRest} __ ${widget.statusRes}_________');
          //                   // widget.statusRes = !widget.statusRes;
          //                   // widget.statusEmergency = !widget.statusEmergency;
          //                   // firestoreApi.changeStatusBusiness(
          //                   //     idBusiness: widget.idRest,
          //                   //     status: widget.statusEmergency);
          //                   // // apiConection.changeStatusRest(
          //                   // //   idRest: widget.idRest,
          //                   // //   status: widget.statusRes
          //                   // // );
          //                   // print(
          //                   //     " ## ${this.widget.idRest} ## ${widget.statusRes} ##");
          //                   // Navigator.of(context).pop();
          //                   // setState(() {});
          //                   // firestoreApi.readRestaurants(providerRestaurant);
          //                   // // providerRestaurant.changeRestaurantStatus(
          //                   // //   idRest: widget.idRest,
          //                   // //   status: widget.statusRes
          //                   // // );
          //                 },
          //               )
          //             ],
          //           );
          //         });
          //     //
          //   },
          //   child: Container(
          //     margin: EdgeInsets.all(8),
          //     decoration: BoxDecoration(
          //         borderRadius: BorderRadius.circular(20.0),
          //         color: Colors.transparent,
          //         border: Border.all(
          //             color: widget.statusRes ? Colors.orange : Colors.grey)),
          //     // width: 100,
          //     padding: EdgeInsets.symmetric(horizontal: 15.0),
          //     alignment: Alignment.center,
          //     height: 40,
          //     child: widget.statusRes ? _textoStateAbierto : _textoStateCerrado,
          //   ),
          // )
        ],
      ),
    );
  }
}
