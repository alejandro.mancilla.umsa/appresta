import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restorant_app/Core/Models/OrderFinalModel.dart';
import 'package:restorant_app/Widgets/CacheImageWidget.dart';
import 'package:restorant_app/Widgets/HistoryOrdenDetails.dart';
import 'package:restorant_app/main.dart';

class ItemPedido extends StatefulWidget {
  final OrderFinalModel orden;
  final String nameBusiness;
  final String idNegocio;

  const ItemPedido({Key key, this.orden, this.idNegocio, this.nameBusiness})
      : super(key: key);

  @override
  _ItemPedidoState createState() => _ItemPedidoState();
}

class _ItemPedidoState extends State<ItemPedido> {
  var precioProduct = 0.0;
  @override
  Widget build(BuildContext context) {
    // List producto = (widget.orden.totalCost - widget.orden.shippingCost).toString().split('.');
    // ProviderDataPages provData = Provider.of<ProviderDataPages>(context);
    return GestureDetector(
      onTap: () {
        // HistoryOrdenDetails
        print('########========= ${widget.orden.orden.length}');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => HistoryOrdenDetails(
                      orderFinalModel: widget.orden,
                      idBusiness: widget.idNegocio,
                      nameBusiness: widget.nameBusiness,
                    )));
      },
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15),
        decoration: BoxDecoration(
            color: this.widget.orden.state == 'FINALIZADO'
                ? Colors.grey[100]
                : Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black12, offset: Offset(0, 1), blurRadius: 10),
            ],
            borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        // height: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'CODIGO: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 20)),
                  TextSpan(
                      text: ' ${widget.orden.codOrder}',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 20)),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'ESTADO: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 14)),
                  TextSpan(
                      text: ' ${widget.orden.state}',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 14)),
                ],
              ),
            ),
            // Text('CODIGO: ${orden.idOrder}',style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),),
            Container(
                width: double.infinity, height: 65, child: listaImagenes()),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'Nombre de cliente: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 12)),
                  TextSpan(
                      text: ' ${widget.orden.nameUser}',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 14)),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'Dirección: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 12)),
                  TextSpan(
                      text: ' ${widget.orden.direction}',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 14)),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'Fecha: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 12)),
                  TextSpan(
                      text: ' ${obtenerFecha(widget.orden.date)}',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 14)),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'Estado de pedido: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 12)),
                  TextSpan(
                      text: ' ${widget.orden.state}',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 14)),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'Repartidor: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 12)),
                  TextSpan(
                      text:
                          ' ${widget.orden.nameRider.isNotEmpty ? widget.orden.nameRider : '__SIN REPARTIDOR__'}',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 14)),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'Productos: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 12)),
                  TextSpan(
                      text: ' ${format(precioProduct)} Bs.',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 14)),
                ],
              ),
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     RichText(
            //       text: TextSpan(
            //         children: <TextSpan>[
            //           TextSpan(text: 'Envio: ', style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey,fontSize: 12)),
            //           TextSpan(text: ' ${orden.shippingCost}', style: TextStyle(fontWeight: FontWeight.normal,color: Colors.black,fontSize: 14)),
            //         ],
            //       ),
            //     ),

            //     RichText(
            //       text: TextSpan(
            //         children: <TextSpan>[
            //           TextSpan(text: 'Productos: ', style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey,fontSize: 12)),
            //           TextSpan(text: ' $precioProduct', style: TextStyle(fontWeight: FontWeight.normal,color: Colors.black,fontSize: 14)),
            //         ],
            //       ),
            //     ),

            //     RichText(
            //       text: TextSpan(
            //         children: <TextSpan>[
            //           TextSpan(text: 'Total: ', style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey,fontSize: 12)),
            //           TextSpan(text: ' ${orden.totalCost}', style: TextStyle(fontWeight: FontWeight.normal,color: Colors.black,fontSize: 14)),
            //         ],
            //       ),
            //     ),
            //   ],
            // )
          ],
        ),
      ),
    );
  }

  String obtenerFecha(Timestamp hora) {
    var fechaActual = DateTime.now();
    var fecha = DateTime.fromMillisecondsSinceEpoch(
        int.parse(hora.millisecondsSinceEpoch.toString()));
    var month = fecha.month;
    var day = fecha.day;
    var year = fecha.year;
    var hour = fecha.hour;
    var minute = fecha.minute;
    var mes = '';
    var result = '';

    if (fechaActual.day == day &&
        fechaActual.month == month &&
        fechaActual.year == year) {
      print('###############HOOOOOY##########'); //.toString().padLeft(2, '0')
      result =
          'Hoy - ${hour.toString().padLeft(2, '0')}:${minute.toString().padLeft(2, '0')}';
    } else {
      switch (month) {
        case 1:
          mes = 'ENE';
          break;
        case 2:
          mes = 'FEB';
          break;
        case 3:
          mes = 'MAR';
          break;
        case 4:
          mes = 'ABR';
          break;
        case 5:
          mes = 'MAY';
          break;
        case 6:
          mes = 'JUN';
          break;
        case 7:
          mes = 'JUL';
          break;
        case 8:
          mes = 'AGO';
          break;
        case 9:
          mes = 'SEP';
          break;
        case 10:
          mes = 'OCT';
          break;
        case 11:
          mes = 'NOV';
          break;
        case 12:
          mes = 'DIC';
          break;
        default:
          mes = '-';
      }
      result = '$day - $mes - $hour:$minute';
    }
    return result;
  }

  Widget listaImagenes() {
    var posicion = 0.0;
    var listaImagenes = <Widget>[];
    precioProduct = 0.0;
    print("""

        ______ ${widget.orden}  ______

        """);
    widget.orden.orden.forEach((Orden orden) {
      orden.order.forEach((element) {
        if (orden.idBusiness == widget.idNegocio) {
          print("""

          ______ ${element.product.idBusiness}  ${widget.idNegocio}_______ 

          """);
          precioProduct =
              precioProduct + (element.product.price * element.amount);
          listaImagenes.add(Positioned(
              left: posicion,
              child: Container(
                width: 60,
                height: 60,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    // CircleAvatar(
                    //   radius: 30,
                    //   backgroundImage: NetworkImage(element.product.photo),
                    // ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: CacheImageWidget(
                        height: 60,
                        width: 60,
                        fit: BoxFit.cover,
                        urlPhoto: element.product.photo,
                      ),
                    ),
                    Positioned(
                        bottom: 0,
                        child: Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                              color: Colors.blueGrey.withOpacity(0.7),
                              shape: BoxShape.circle),
                          alignment: Alignment.center,
                          child: Text(
                            '${element.amount}',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ))
                  ],
                ),
              )));
          posicion = posicion + 40;
        }
      });
    });

    return Stack(
      children: listaImagenes,
    );
  }
}
