import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';

class ButtomBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProviderListPlatos _listPlatos = Provider.of<ProviderListPlatos>(context);

    return Card(
      elevation: 0.0,
      margin: EdgeInsets.all(0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _itemButtom(_listPlatos, 1,
              icono: Icons.list_alt_rounded, title: "Pedidos"),
          _itemButtom(_listPlatos, 2,
              icono: Icons.shopping_cart, title: "Productos"),
          // _itemButtom(_listPlatos,3, icono: Icons.add_box,title: "Nuevo Plato"),
          _itemButtom(_listPlatos, 4, icono: Icons.person, title: "Perfil"),
          // _itemButtom(),
        ],
      ),
    );
  }

  _itemButtom(ProviderListPlatos provPlatos, int index,
      {@required IconData icono, @required String title}) {
    int _indexPage = provPlatos.indexPage;
    bool activate = index == _indexPage;
    Color colFin = activate ? Colors.orange : Colors.grey;
    return InkWell(
      onTap: () {
        provPlatos.indexPage = index;
      },
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Icon(
              icono,
              color: colFin,
            ),
            Text(
              "$title",
              style: TextStyle(color: colFin),
            )
          ],
        ),
      ),
    );
  }
}
