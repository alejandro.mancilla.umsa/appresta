import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Core/Models/BusinessModel.dart';
import 'package:restorant_app/Core/TimeApi.dart';
import 'package:restorant_app/Pages/AdminRestPage.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';
import 'package:restorant_app/Widgets/CacheImageWidget.dart';

final ApiFirebase apiFirebase = ApiFirebase();

class CardRestaurant extends StatelessWidget {
  final BusinessModel businessModel;

  CardRestaurant({@required this.businessModel});

  // ProviderListPlatos providerListPlatos;

  @override
  Widget build(BuildContext context) {
    ProviderListPlatos providerListPlatos =
        Provider.of<ProviderListPlatos>(context);
    var date = DateTime.now();
    final String dia = timeApi.getDias(DateFormat('EEEE').format(date));

    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, offset: Offset(0, 1), blurRadius: 10)
              ]),
          // margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
          margin: EdgeInsets.only(left: 15, right: 15, top: 10),
          child: Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(15.0),
                        topLeft: Radius.circular(15.0),
                      ),
                      child: CacheImageWidget(
                          height: 100,
                          fit: BoxFit.cover,
                          urlPhoto: this.businessModel.photo)
                      // FadeInImage(
                      //   placeholder: AssetImage("assets/images/tenor.gif"),
                      // ),
                      )),
              Expanded(
                  flex: 2,
                  child: InkWell(
                    onTap: () {
                      // print('object');
                      providerListPlatos.indexPage = 1;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => AdminRestPage(
                                    businessModel: businessModel,
                                    status: businessModel.emergency
                                        ? !businessModel.statusOpenLocal
                                        : businessModel.statusOpenLocal,
                                  )));
                    },
                    child: ListTile(
                      title: Text(this.businessModel.name),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${this.businessModel.direction}",
                            style: TextStyle(fontSize: 10),
                          ),
                          Text(
                              "${this.businessModel.scheduleNow.isEmpty ? 'Sin horario hoy' : this.businessModel.scheduleNow} - $dia"),
                          Container(
                            // margin: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: !this.businessModel.statusOpenLocal
                                  ? Colors.grey.shade300
                                  : Colors.orange.withAlpha(30),
                              // border: Border.all(
                              //     color: this.emergency
                              //         ? Colors.red
                              //         : (this.businessModel.statusOpenLocal
                              //             ? Colors.orange
                              //             : Colors.grey))
                            ),
                            // width: 100,
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            alignment: Alignment.center,
                            // height: 25,
                            child: this.businessModel.statusOpenLocal
                                ? _textState('Abierto')
                                : _textState(
                                    'Cerrado',
                                    isOpen: false,
                                  ),
                            // this.businessModel.statusOpenLocal ? _textoStateAbierto : _textoStateCerrado,
                          ),
                        ],
                      ),
                    ),
                  )),
            ],
          ),
        ),
        SwitchHorario(
          idBusiness: this.businessModel.idBusiness,
          statusStatusOpenLocal: this.businessModel.statusBusinessOpen,
        )
      ],
    );
  }

  Widget _textState(String data, {bool isOpen = true}) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5, top: 5),
      child: Text(
        data,
        style: TextStyle(
            color: isOpen ? Colors.orange : Colors.black,
            fontSize: 16.0,
            fontWeight: FontWeight.normal),
      ),
    );
  }

  final TimeApi timeApi = TimeApi();

  // final Widget _textoStateAbierto = Padding(
  //   padding: EdgeInsets.only(bottom: 5, top: 5),
  //   child: Text(
  //     "Abierto",
  //     style: TextStyle(
  //         color: Colors.orange, fontSize: 16.0, fontWeight: FontWeight.normal),
  //   ),
  // );
  // final Widget _textoStateCerrado = Padding(
  //   padding: EdgeInsets.only(bottom: 5, top: 5),
  //   child: Text(
  //     "Cerrado",
  //     style: TextStyle(
  //         color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.normal),
  //   ),
  // );
}

class SwitchHorario extends StatefulWidget {
  // const SwitchHorario({Key key}) : super(key: key);
  final String idBusiness;
  final String statusStatusOpenLocal;

  const SwitchHorario({Key key, this.idBusiness, this.statusStatusOpenLocal})
      : super(key: key);

  @override
  _SwitchHorarioState createState() => _SwitchHorarioState();
}

class _SwitchHorarioState extends State<SwitchHorario> {
  ProviderRestaurant providerRestaurant;

  @override
  void initState() {
    providerRestaurant =
        Provider.of<ProviderRestaurant>(context, listen: false);
    super.initState();
    print('------------- ${this.widget.statusStatusOpenLocal}');
    if (this.widget.statusStatusOpenLocal == 'AlwaysClosed') {
      leftPosition = 0;
    }
    if (this.widget.statusStatusOpenLocal == 'Automatic') {
      leftPosition = 105;
    }
    if (this.widget.statusStatusOpenLocal == 'AlwaysOpen') {
      leftPosition = 210;
    }
  }

  double leftPosition = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 320,
      margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
      decoration: BoxDecoration(
        color: Colors.blueGrey[100],
        borderRadius: BorderRadius.circular(100),
      ),
      child: Stack(children: [
        AnimatedPositioned(
          left: leftPosition,
          duration: Duration(milliseconds: 200),
          child: Container(
            width: 110,
            height: 31,
            decoration: BoxDecoration(
                color: Colors.blueGrey[900],
                borderRadius: BorderRadius.circular(100),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black38,
                    blurRadius: 5,
                    offset: Offset(-1, 0),
                  ),
                  BoxShadow(
                    color: Colors.black38,
                    blurRadius: 5,
                    offset: Offset(0, 1),
                  )
                ]),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            selectItem('Permanecer\ncerrado', 0, StatusOpenLocal.AlwaysClosed),
            selectItem('Automatico', 105, StatusOpenLocal.Automatic),
            selectItem('Permanecer\nabierto', 210, StatusOpenLocal.AlwaysOpen),
          ],
        ),
      ]),
    );
  }

  Widget selectItem(String data, double position, StatusOpenLocal enumStatus) {
    return Expanded(
      child: GestureDetector(
        onTap: () async {
          EasyLoading.show(
            status: 'Cargando...',
            dismissOnTap: false,
          );
          bool response = false;
          //changeStatusOpenBusiness
          switch (enumStatus) {
            case StatusOpenLocal.AlwaysClosed:
              response = await apiFirebase.changeStatusOpenBusiness(
                  idBusiness: this.widget.idBusiness, status: "AlwaysClosed");
              break;
            case StatusOpenLocal.AlwaysOpen:
              response = await apiFirebase.changeStatusOpenBusiness(
                  idBusiness: this.widget.idBusiness, status: "AlwaysOpen");
              break;
            case StatusOpenLocal.Automatic:
              response = await apiFirebase.changeStatusOpenBusiness(
                  idBusiness: this.widget.idBusiness, status: "Automatic");
              break;
          }

          if (response) {
            switch (enumStatus) {
              case StatusOpenLocal.AlwaysClosed:
                providerRestaurant.changeStatusOpenLocalBusiness(
                    this.widget.idBusiness, false);
                break;
              case StatusOpenLocal.AlwaysOpen:
                providerRestaurant.changeStatusOpenLocalBusiness(
                    this.widget.idBusiness, true);
                break;
              case StatusOpenLocal.Automatic:
                providerRestaurant.changeStatusOpenLocalBusiness(
                    this.widget.idBusiness, null);
                break;
            }
            EasyLoading.showSuccess('Estado cambiado con exito!');

            leftPosition = position;

            setState(() {});
          } else {
            EasyLoading.showError(
                'Fallo el cambio de estado, revisa tu conexión a internet');
          }
        },
        child: Container(
          alignment: Alignment.center,
          // height: double.maxFinite,
          decoration: BoxDecoration(
            // color: Colors.blueGrey[900],
            borderRadius: BorderRadius.circular(100),
          ),
          child: Text(
            data,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: position == leftPosition ? Colors.white : Colors.blueGrey,
              fontSize: 10,
            ),
          ),
          // padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        ),
      ),
    );
  }
}

enum StatusOpenLocal { AlwaysOpen, AlwaysClosed, Automatic }
