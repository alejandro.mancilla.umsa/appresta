import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PriceEdit extends StatefulWidget {
  TextEditingController priceController;
  PriceEdit({this.priceController});
  @override
  _PriceEditState createState() => _PriceEditState();
}

class _PriceEditState extends State<PriceEdit> {
  bool stateEdit = true;
  // ignore: avoid_init_to_null
  String error = null;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, right: 0),
      // padding: EdgeInsets.symmetric(horizontal: 10),
      child: TextFormField(
        // enabled: stateEdit,
        // maxLength: 20,
        controller: this.widget.priceController,
        cursorColor: Colors.black,
        keyboardType: TextInputType.number,
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        validator: (value) {
          if (value.contains(",")) {
            print(" # entro = $value");
            return ('Por favor remplace la coma por un punto.');
          }
          return null;
        },
        onChanged: (value) {
          if (value.contains(",")) {
            print(" # entro = $value");
            error = 'Use punto.';
            // return ('Por favor remplace la coma por un punto.');
          } else {
            error = null;
          }
          setState(() {});
        },
        decoration: InputDecoration(
          enabled: stateEdit,
          errorText: error,
          // hintStyle: TextStyle(color: Colors.red),
          // counterStyle: TextStyle(color: Colors.orange),
          fillColor: Colors.white,
          filled: true,
          // prefixIcon: Icon(Icons.email, color: Colors.green.shade900),
          // suffixIcon: IconButton(icon: Icon(Icons.check), onPressed: (){
          //   print("object");
          //   stateEdit = !stateEdit;
          //   setState(() {

          //   });
          // }) ,
          // Icon(Icons.clear, color: Colors.green.shade100),
          suffix: Text("Bs"),
          suffixStyle:
              TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
                color: Colors.grey.shade200,
                style: BorderStyle.solid,
                width: 2),
          ),

          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
                color: Colors.grey.shade200,
                style: BorderStyle.solid,
                width: 2),
          ),
          labelText: 'Precio',
          labelStyle:
              TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.normal),
        ),
      ),
    );
  }
}
