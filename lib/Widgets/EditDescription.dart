import 'package:flutter/material.dart';

// ignore: must_be_immutable
class EditDescription extends StatefulWidget {
  TextEditingController descriptionController;
  EditDescription({this.descriptionController});
  @override
  _EditDescriptionState createState() => _EditDescriptionState();
}

class _EditDescriptionState extends State<EditDescription> {
  bool stateEdit = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Row(
        children: [
          // IconButton(
          //     icon: Icon(stateEdit ? Icons.check : Icons.edit),
          //     // padding: EdgeInsets.symmetric(vertical: 10,horizontal: 1),
          //     onPressed: () {
          //       print("object");
          //       stateEdit = !stateEdit;
          //       setState(() {});
          //     }),
          Expanded(
            child: TextFormField(
              // enabled: stateEdit,
              // maxLength: 20,
              controller: this.widget.descriptionController,
              cursorColor: Colors.orange,
              //  validator: (value) {
              //   if (value.contains(",")) {
              //     print(" # entro = $value");
              //     error = 'Por favor remplace la coma por un punto.';
              //     // return ('Por favor remplace la coma por un punto.');
              //   }else{

              //   }
              //   // return null;
              // },

              decoration: InputDecoration(
                  enabled: stateEdit,
                  fillColor: Colors.black.withOpacity(0),
                  filled: true,
                  // prefixIcon: Icon(Icons.email, color: Colors.green.shade900),
                  // suffixIcon: IconButton(icon: Icon(Icons.check), onPressed: (){
                  //   print("object");
                  //   stateEdit = !stateEdit;
                  //   setState(() {

                  //   });
                  // }) ,
                  // Icon(Icons.clear, color: Colors.green.shade100),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        color: Colors.grey.shade200,
                        // style: BorderStyle.solid,
                        width: 2),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        color: Colors.grey.shade200,
                        // style: BorderStyle.solid,
                        width: 2),
                  ),
                  labelText: 'Descripción',
                  labelStyle: TextStyle(color: Colors.blueGrey)),

              minLines: 2,
              maxLines: 5,
            ),
          ),
        ],
      ),
    );
  }
}
