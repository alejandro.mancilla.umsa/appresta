import 'package:flutter/material.dart';
import 'package:material_tag_editor/tag_editor.dart';
import 'package:restorant_app/Pages/AddNewProduct.dart';
import 'package:restorant_app/Widgets/EditTextField.dart';

// ignore: must_be_immutable
class OptionEdit extends StatefulWidget {
  // final int number;
  List<String> values = [];
  TextEditingController controller = new TextEditingController();
  bool limit = true;

  // OptionEdit({Key key, this.number = 0}) : super(key: key);
  @override
  _OptionEditState createState() => _OptionEditState();
}

class _OptionEditState extends State<OptionEdit> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.green.withOpacity(0.1),
          borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      margin: EdgeInsets.only(left: 15, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Text('Opcion ${widget.number}'),
          Container(
            height: 50,
            child: EditTextField(
              showIcon: false,
              color: Colors.green,
              label: "Nombre",
              tituloController: this.widget.controller,
            ),
          ),
          Row(
            children: [
              Text("¿Se puede seleccionar varios?"),
              Checkbox(
                activeColor: Colors.blue,
                value: this.widget.limit,
                onChanged: (bool value) {
                  setState(() {
                    this.widget.limit = value;
                  });
                },
              ),
              Text(this.widget.limit ? "SI" : "NO"),
            ],
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.6),
                borderRadius: BorderRadius.circular(10)),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: TagEditor(
              length: this.widget.values.length,
              delimiters: [',', ' '],
              hasAddButton: true,
              inputDecoration: const InputDecoration(
                border: InputBorder.none,
                hintText: 'Ingrese una opción',
              ),
              onTagChanged: (newValue) {
                setState(() {
                  this.widget.values.add(newValue);
                });
              },
              tagBuilder: (context, index) => _Chip(
                index: index,
                label: this.widget.values[index],
                onDeleted: //onDelete //(value) => value
                    () {
                  print("$index numero");
                  print(this.widget.values);
                  this.widget.values.removeAt(index);
                  setState(() {});
                  // return value;
                },
              ),
            ),
          ),
          // Divider(
          //   color: Colors.green.withOpacity(0.5),
          //   thickness: 1,
          // )
        ],
      ),
    );
  }
}

//////////////////////////////////////
///[nuevos options]
class _Chip extends StatelessWidget {
  const _Chip({
    @required this.label,
    @required this.onDeleted,
    @required this.index,
  });

  final String label;
  // final ValueChanged<int> onDeleted;
  final Function onDeleted;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Chip(
      backgroundColor: Colors.green[300],
      labelPadding: const EdgeInsets.only(left: 8.0),
      label: Text(
        label,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      deleteIcon: Icon(
        Icons.close,
        size: 18,
      ),
      onDeleted: onDeleted,
    );
  }
}
