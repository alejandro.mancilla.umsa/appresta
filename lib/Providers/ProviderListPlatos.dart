import 'package:flutter/material.dart';
import 'package:restorant_app/Core/Models/OrderFinalModel.dart';
import 'package:restorant_app/Core/Models/ProductModelUnit.dart';

class ProviderListPlatos with ChangeNotifier {
  bool _openState = false;
  bool get openState => this._openState;
  set openState(bool stado) {
    this._openState = stado;
    notifyListeners();
  }

  int _indexPage = 1;
  int get indexPage => this._indexPage;
  set indexPage(int i) {
    this._indexPage = i;
    notifyListeners();
  }

  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  ///////////////////////////////////////////////
  List<ProductModelUnit> _listaProducts;
  List<ProductModelUnit> get listaProducts => this._listaProducts;
  set listaProducts(List<ProductModelUnit> lista) {
    this._listaProducts = lista;
    notifyListeners();
  }

  // ///////////////////////////////////////////////
  List<OrderFinalModel> _listaOrders;
  // List<OrderFinalModel> _listaOrdersEnProgreso = [];
  // List<OrderFinalModel> _listaOrdersFinalizado = [];
  // List<OrderFinalModel> _listaOrdersCancelado = [];

  List<OrderFinalModel> get listaOrders => this._listaOrders;
  set listaOrders(List<OrderFinalModel> lista) {
    this._listaOrders = lista;
    notifyListeners();
  }

  // ordenOrdersByDate() {
  //   // this._listaOrders.forEach((OrderFinalModel element) {

  //   // });
  //   this._listaOrders.sort((a, b) {
  //     // print("------------ ${a.deliveryTime} ----------- ${b.deliveryTime}");
  //     return (a.date).compareTo(b.date);
  //   });
  //   notifyListeners();
  // }

  // List<OrderFinalModel> get listaOrdersCancelado => this._listaOrdersCancelado;
  // set listaOrdersCancelado(List<OrderFinalModel> lista) {
  //   this._listaOrders = lista;
  //   notifyListeners();
  // }

  // List<OrderFinalModel> get listaOrdersEnProgreso =>
  //     this._listaOrdersEnProgreso;
  // set listaOrdersEnProgreso(List<OrderFinalModel> lista) {
  //   this._listaOrders = lista;
  //   notifyListeners();
  // }

  // List<OrderFinalModel> get listaOrdersFinalizado =>
  //     this._listaOrdersFinalizado;
  // set listaOrdersFinalizado(List<OrderFinalModel> lista) {
  //   this._listaOrders = lista;
  //   notifyListeners();
  // }

  // ordenOrdersByState() {
  //   _listaOrdersCancelado = [];
  //   _listaOrdersEnProgreso = [];
  //   _listaOrdersFinalizado = [];
  //   this._listaOrders.forEach((OrderFinalModel element) {
  //     if (element.state == 'CANCELADO') {
  //       _listaOrdersCancelado.add(element);
  //     }
  //     if (element.state == 'EN PROGRESO') {
  //       _listaOrdersEnProgreso.add(element);
  //     }
  //     if (element.state == 'FINALIZADO') {
  //       _listaOrdersFinalizado.add(element);
  //     }
  //   });

  //   this._listaOrders.sort((a, b) {
  //     // print("------------ ${a.deliveryTime} ----------- ${b.deliveryTime}");
  //     return (a.state).compareTo(b.state);
  //   });
  //   notifyListeners();
  // }
}
