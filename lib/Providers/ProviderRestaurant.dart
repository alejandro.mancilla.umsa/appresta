import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:restorant_app/Core/Models/BusinessModel.dart';
import 'package:restorant_app/Core/TimeApi.dart';

class ProviderRestaurant with ChangeNotifier {
  // ProductModel _oneProductModel;
  // ProductModel get oneProductModel => this._oneProductModel;
  // set oneProductModel(ProductModel data) {
  //   this._oneProductModel = data;
  //   notifyListeners();
  // }

  // List<ModelRestaurante> _listaRest;

  // List<ModelRestaurante> get listaRest => this._listaRest;
  // set listaRest(List<ModelRestaurante> lista) {
  //   this._listaRest = lista;
  //   notifyListeners();
  // }

  // changeRestaurantStatus({String idRest, bool status}) {
  //   listaRest = this._listaRest.map((ModelRestaurante e) {
  //     if (idRest == e.id) {
  //       e.statusProvider = status;
  //     }
  //     return e;
  //   }).toList();
  // }

  // changeProductStatus(String idProduct, bool status) {
  //   listaRest = this._listaRest.map((ModelRestaurante rest) {
  //     rest.products = rest.products.map((Product producto) {
  //       if (producto.id == idProduct) {
  //         producto.hasProduct = status;
  //       }
  //       return producto;
  //     }).toList();

  //     return rest;
  //   }).toList();
  // }

  // String _idRestaurante = '';

  // String get idRestaurante => this._idRestaurante;
  // set idRestaurante(String id) {
  //   this._idRestaurante = id;
  // }

  // //////////////////////////////////////////////////////////////////////
  // //////////////////////////////////////////////////////////////////////
  // //////////////////////////////////////////////////////////////////////
  // ///
  List<BusinessModel> _listaBusiness = [];
  List<BusinessModel> get listaBusiness => this._listaBusiness;
  set listaBusiness(List<BusinessModel> lista) {
    this._listaBusiness = lista;
    changeNotifierListener();
  }

  changeNotifierListener() {
    notifyListeners();
  }

  TimeApi timeApi = TimeApi();
  DateTime tiempoInternet;
  obtenerHoraSegunDia() async {
    tiempoInternet = await timeApi.getTime();
    var date = DateTime.now();
    String dia = timeApi.getDias(DateFormat('EEEE').format(date));
    String month = tiempoInternet.month.toString().padLeft(2, '0');
    String days = tiempoInternet.day.toString().padLeft(2, '0');

    if (_listaBusiness != null) {
      _listaBusiness?.forEach((BusinessModel restaurantModel) {
        if (restaurantModel.statusBusinessOpen == 'Automatic') {
          int i = 0;
          restaurantModel.days.forEach((element) {
            // print('====== $dia ===== $element === ${restaurantModel.name}==');
            if (dia == element) {
              List<String> horario = restaurantModel.schedule[i].split('-');
              List<String> horaIngreso = horario[0].split(':');
              List<String> horaSalida = horario[1].split(':');
              DateTime tiempoRestaurantIngreso = DateTime.parse(
                  "${tiempoInternet.year}-$month-$days ${horaIngreso[0]}:${horaIngreso[1]}:00.000");
              DateTime tiempoRestaurantSalida = DateTime.parse(
                  "${tiempoInternet.year}-$month-$days ${horaSalida[0]}:${horaSalida[1]}:00.000");
              // print(
              //     "####ENTRO ${restaurantModel.name}, ${restaurantModel.schedule[i].split('-')} = ${tiempoInternet.year}-$month-$days ${horaIngreso[0]}:${horaIngreso[1]}:00.000 ${tiempoInternet.year}-$month-$days  ${horaSalida[0]}:${horaSalida[1]}:00.000 ");
              restaurantModel.scheduleNow =
                  '${restaurantModel.schedule[i].split('-')}';
              if (tiempoInternet.isAfter(tiempoRestaurantIngreso) &&
                  tiempoInternet.isBefore(tiempoRestaurantSalida)) {
                //AFTER ES DESPUES
                // print("esta en horario");
                restaurantModel.statusOpenLocal = true;
              } else {
                restaurantModel.statusOpenLocal = false;

                // print(" no esta en horario");
              }
            }
            i = i + 1;
          });
        } else {
          if (restaurantModel.statusBusinessOpen == 'AlwaysClosed') {
            restaurantModel.statusOpenLocal = false;
          }
          if (restaurantModel.statusBusinessOpen == 'AlwaysOpen') {
            restaurantModel.statusOpenLocal = true;
          }
        }
      });
      changeNotifierListener();
    }
  }

  changeStatusOpenLocalBusiness(String idBusiness, bool status) {
    if (_listaBusiness != null) {
      print('object###########');
      _listaBusiness?.forEach((BusinessModel restaurantModel) {
        if (restaurantModel.idBusiness == idBusiness) {
          if (status == null) {
            print('Automatic---------------');
            restaurantModel.statusBusinessOpen = 'Automatic';
            // changeNotifierListener();

            obtenerHoraSegunDia();
          } else {
            restaurantModel.statusOpenLocal = status;
            restaurantModel.statusBusinessOpen =
                status ? 'AlwaysOpen' : 'AlwaysClosed';
          }
        }
      });
      changeNotifierListener();
    }
  }

  Future<String> getDia() async {
    // tiempoInternet = await timeApi.getTime();
    var date = DateTime.now();
    String dia = timeApi.getDias(DateFormat('EEEE').format(date));
    return dia;
  }
}
