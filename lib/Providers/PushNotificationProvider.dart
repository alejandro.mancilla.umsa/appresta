import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:restorant_app/Core/Utils.dart';

class PushNotificationProvider{
  UserPreferences prefs = UserPreferences();

  initNotifications(){
    try {
      FirebaseMessaging _messaging = FirebaseMessaging.instance;
      _messaging.requestPermission();
      _messaging.getToken().then((value) {
        print("====== FCM ==== $value");
        prefs.userTokenNotification = value;
        //e1ZkYtoPSNqFSZkASezNKN:APA91bEnJQmqReAkKzrP4j7YlDdHtnlYQd0FGkasqi2v1rIfisY-ggDybHf_p4UW6kE_WqTf8AK2BQqTryC5NF7jBCbjlHcG__Q7HvhATwAxjsb1GrisDFIL70t5xbnW7iS3xmBRb1Rs
      });
    } catch (e) {
      print("====== error: $e");
    }

  }
}