import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Core/Models/BusinessModel.dart';
import 'package:restorant_app/Core/TimeApi.dart';
import 'package:restorant_app/Pages/AdminRestPage.dart';
import 'package:restorant_app/Pages/EditPageBusiness.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';
// import 'package:restorant_app/Rest/Endpoints.dart';
import 'package:restorant_app/Widgets/CardRestaurant.dart';
import 'package:restorant_app/Widgets/ConectInternet.dart';

class HomeScreen extends StatefulWidget {
  final User user;

  HomeScreen({this.user});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ApiFirebase apiFirebase = ApiFirebase();
  // ApiConection apiData = new ApiConection();
  ProviderRestaurant providerRestaurant;
  ProviderListPlatos _listPlatos;

  @override
  void initState() {
    // llamada a la lista de restaurantes
    _listPlatos = Provider.of<ProviderListPlatos>(context, listen: false);
    providerRestaurant =
        Provider.of<ProviderRestaurant>(context, listen: false);
    Future.delayed(Duration.zero, () {
      readRest();
    });
    readUsuarioStatus();
    // _obteniendoDatos();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    providerRestaurant = Provider.of<ProviderRestaurant>(context, listen: true);
  }

  bool statusUser;
  readUsuarioStatus() async {
    statusUser = await apiFirebase.readStatusUser();
    print('=== $statusUser ===');
  }

  readRest() async {
    // List<BusinessModel> listaBusinnes = [];
    apiFirebase.readRestaurants(providerRestaurant);
    // providerRestaurant.listaBusiness = listaBusinnes;
    // print(listaBusinnes);
  }
  // _obteniendoDatos()async{
  //   List<ModelRestaurante> listaRest=await apiData.getListRestAdmin(widget.user.uid);
  //   providerRestaurant.listaRest = listaRest;
  // }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // backgroundColor: Colors.grey.shade200,
        // floatingActionButton: FloatingActionButton(
        //     onPressed: (){
        //       _cerrarCesion(context);
        //     }
        // ),
        appBar: AppBar(
          elevation: 0,
          title: Container(
            width: double.infinity,
            child: Text(
              "Elige un restaurante",
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            ),
          ),
          backgroundColor: Colors.white,
          // textTheme: TextTheme(title: TextStyle(color: Colors.black)),
        ),
        body: statusUser == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : statusUser
                ? SingleChildScrollView(
                    child: Column(
                      children: [
                        ..._listaRestaurantes(context),
                        CupertinoButton(
                          // color: Colors.blue,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Crea un negocio ahora"),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: Icon(Icons.add),
                              )
                            ],
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        EditPageBusiness()));
                          },
                        )
                      ],
                    ),
                  )
                // Column(
                //   children: [
                //     // RefreshIndicator(
                //     //   child: Container(
                //     //     width: 50,
                //     //     height: 50,
                //     //   ),
                //     //   onRefresh: () {
                //     //     return Future.delayed(Duration(seconds: 3),(){
                //     //       return;
                //     //     });
                //     //   },
                //     // ),
                //     ConectInternet(),
                //     Expanded(
                //       child: Container(
                //         child: SingleChildScrollView(
                //           child: Builder(
                //             builder: (context) {
                //               return Column(
                //                 mainAxisAlignment: MainAxisAlignment.center,
                //                 crossAxisAlignment: CrossAxisAlignment.center,
                //                 children: [
                //                   SizedBox(
                //                     height: 5.0,
                //                   ),
                //                   ..._listaRestaurantes(context),
                //                   if (_listaRestaurantes(context).length == 0)
                //                     Text(
                //                         "Aun no eres administrador de ningun negocio."),
                //                   CupertinoButton(
                //                     // color: Colors.blue,
                //                     child: Row(
                //                       mainAxisAlignment:
                //                           MainAxisAlignment.center,
                //                       children: [
                //                         Text("Crea un negocio ahora"),
                //                         Padding(
                //                           padding: const EdgeInsets.symmetric(
                //                               horizontal: 5),
                //                           child: Icon(Icons.add),
                //                         )
                //                       ],
                //                     ),
                //                     onPressed: () {
                //                       Navigator.push(
                //                           context,
                //                           MaterialPageRoute(
                //                               builder:
                //                                   (BuildContext context) =>
                //                                       EditPageBusiness()));
                //                     },
                //                   )
                //                 ],
                //               );
                //             },
                //             // child: ,
                //           ),
                //         ),
                //       ),
                //     ),
                //   ],
                // )
                : Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Center(
                      child: Text(
                        "EL EQUIPO ADMINISTRATIVO TE DARA ACCESO",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 16, color: Colors.blueGrey),
                      ),
                    ),
                  ),
      ),
    );
  }

  List<Widget> _listaRestaurantes(BuildContext context) {
    // ProviderListPlatos provPlatos = Provider.of<ProviderListPlatos>(context);
    // ProviderRestaurant providerRestaurant = Provider.of<ProviderRestaurant>(context);

    return providerRestaurant.listaBusiness?.map((BusinessModel businessModel) {
          return CardRestaurant(
            businessModel: businessModel,
          );
        })?.toList() ??
        [Text("No tienes negocios registrados")].toList();
  }
}
