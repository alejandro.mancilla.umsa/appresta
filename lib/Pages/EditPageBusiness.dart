import 'dart:async';
import 'dart:io';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_time_range/flutter_time_range.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path/path.dart';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Core/Models/BusinessModel.dart';
import 'package:restorant_app/Core/Utils.dart';
import 'package:restorant_app/Core/styleMapa.dart';
import 'package:restorant_app/Pages/ChangeMapPosition.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';
import 'package:restorant_app/Widgets/CacheImageWidget.dart';
import 'package:restorant_app/Widgets/EditTextField.dart';

class EditPageBusiness extends StatefulWidget {
  final bool iCreate;
  final BusinessModel businessModel;

  const EditPageBusiness({Key key, this.businessModel, this.iCreate = false})
      : super(key: key);
  @override
  _EditPageBusinessState createState() => _EditPageBusinessState();
}

class _EditPageBusinessState extends State<EditPageBusiness> {
  TextEditingController controllerName;
  TextEditingController controllerEmail;
  TextEditingController controllerNameOwner;
  TextEditingController controllerNumberOwner;
  TextEditingController controllerDirection;
  TextEditingController controllerCity;
  TextEditingController controllerPreparationTime;
  TextEditingController controllerZone;

  // GoogleMapController _controllerGoogleMaps;
  List<String> days = [];
  List<String> schedule = [];

  TextEditingController controllerlunes;
  TextEditingController controllermartes;
  TextEditingController controllermiercoles;
  TextEditingController controllerjueves;
  TextEditingController controllerviernes;
  TextEditingController controllersabado;
  TextEditingController controllerdomingo;

  bool lunes;
  bool martes;
  bool miercoles;
  bool jueves;
  bool viernes;
  bool sabado;
  bool domingo;

  ApiFirebase apiFirebase = ApiFirebase();
  ProviderRestaurant providerRestaurant;

  CameraPosition positionMap;
  List<Placemark> placemarks;
  Completer<GoogleMapController> _controller = Completer();

  bool banderaEdit = false;
  List<String> coord = ['0', '0'];

  Position position;
  UserPreferences prefs = UserPreferences();

  void readLocalizationInit() async {
    try {
      position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      // position = Position(
      //     latitude: double.parse(coord[0]), longitude: double.parse(coord[1]));
      positionMap = CameraPosition(
          target: LatLng(position.latitude, position.longitude), zoom: 15);
      _obtenerDireccion(position.latitude, position.longitude);
      // setState(() async{
      // });
      await _moveTo(positionMap);
    } catch (e) {
      print(e);
    }
  }

  Future<void> _moveTo(CameraPosition position) async {
    final controller = await _controller.future;
    await controller.animateCamera(CameraUpdate.newCameraPosition(position));
  }

  _readCoordenates() {
    if (widget.businessModel != null) {
      coord = widget.businessModel.ubication.replaceAll(' ', '').split(',');
      positionMap = CameraPosition(
          target: LatLng(double.parse(coord[0]), double.parse(coord[1])),
          zoom: 15);
    } else {
      readLocalizationInit();
    }
  }

  @override
  void initState() {
    if (!prefs.acceptLocation) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _permissionLocation();
      });
    } else {
      _readCoordenates();
    }

    providerRestaurant =
        Provider.of<ProviderRestaurant>(this.context, listen: false);
    controllerCity = TextEditingController();
    controllerName = TextEditingController();
    controllerEmail = TextEditingController();
    controllerNameOwner = TextEditingController();
    controllerNumberOwner = TextEditingController();
    controllerDirection = TextEditingController();
    controllerPreparationTime = TextEditingController();
    controllerZone = TextEditingController();

    controllerlunes = TextEditingController();
    controllermartes = TextEditingController();
    controllermiercoles = TextEditingController();
    controllerjueves = TextEditingController();
    controllerviernes = TextEditingController();
    controllersabado = TextEditingController();
    controllerdomingo = TextEditingController();

    lunes = false;
    martes = false;
    miercoles = false;
    jueves = false;
    viernes = false;
    sabado = false;
    domingo = false;

    controllerlunes.text = '';
    controllermartes.text = '';
    controllermiercoles.text = '';
    controllerjueves.text = '';
    controllerviernes.text = '';
    controllersabado.text = '';
    controllerdomingo.text = '';

    if (this.widget.businessModel != null) {
      banderaEdit = true;
      controllerCity.text = this.widget.businessModel.city;
      controllerName.text = this.widget.businessModel.name;
      controllerEmail.text = this.widget.businessModel.email;
      controllerNameOwner.text = this.widget.businessModel.nameOwner;
      controllerNumberOwner.text = this.widget.businessModel.numberOwner;
      controllerDirection.text = this.widget.businessModel.direction;
      controllerPreparationTime.text =
          this.widget.businessModel.preparationTime.toString();
      controllerZone.text = this.widget.businessModel.zone;
      photo = this.widget.businessModel.photo;

      int i = 0;

      this.widget.businessModel.days.forEach((element) {
        if (element == "Lunes") {
          lunes = true;
          controllerlunes.text = this.widget.businessModel.schedule[i];
          i = i + 1;
        }
        if (element == "Martes") {
          martes = true;
          controllermartes.text = this.widget.businessModel.schedule[i];
          i = i + 1;
        }
        if (element == "Miercoles") {
          miercoles = true;
          controllermiercoles.text = this.widget.businessModel.schedule[i];
          i = i + 1;
        }
        if (element == "Jueves") {
          jueves = true;
          controllerjueves.text = this.widget.businessModel.schedule[i];
          i = i + 1;
        }
        if (element == "Viernes") {
          viernes = true;
          controllerviernes.text = this.widget.businessModel.schedule[i];
          i = i + 1;
        }
        if (element == "Sabado") {
          sabado = true;
          controllersabado.text = this.widget.businessModel.schedule[i];
          i = i + 1;
        }
        if (element == "Domingo") {
          domingo = true;
          controllerdomingo.text = this.widget.businessModel.schedule[i];
          i = i + 1;
        }
      });

      List ubication = this.widget.businessModel.ubication.trim().split(',');
      positionMap = new CameraPosition(
          target:
              LatLng(double.parse(ubication[0]), double.parse(ubication[1])),
          zoom: 16);
    } else {
      controllerlunes.text = '00:00-00:00';
      controllermartes.text = '00:00-00:00';
      controllermiercoles.text = '00:00-00:00';
      controllerjueves.text = '00:00-00:00';
      controllerviernes.text = '00:00-00:00';
      controllersabado.text = '00:00-00:00';
      controllerdomingo.text = '00:00-00:00';
    }

    super.initState();
  }

  // ignore: always_declare_return_types
  _permissionLocation() {
    showDialog(
        barrierDismissible: false,
        context: this.context,
        builder: (_) => AlertDialog(
              title: Text(
                'Permisos necesarios',
                style: Theme.of(this.context)
                    .textTheme
                    .headline6
                    .merge(TextStyle(fontSize: 18.0)),
              ),
              content: Container(
                height: 155,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Usamos tu ubicación para mejorar tu experiencia y calidad de las entregas',
                      style: Theme.of(this.context)
                          .textTheme
                          .subtitle1
                          .merge(TextStyle(fontSize: 14.0)),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 25),
                    //   child: SvgPicture.asset(
                    //     'src/icon/map.svg',
                    //     width: 65,
                    //     height: 65,
                    //   ),
                    // )
                  ],
                ),
              ),
              actions: <Widget>[
                // ignore: deprecated_member_use
                FlatButton(
                  onPressed: () {
                    prefs.setAcceptLocation();
                    _readCoordenates();
                    setState(() {});
                    Navigator.pop(this.context);
                  },
                  child: Text('ACEPTAR'),
                )
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  // CacheImageWidget(
                  //   width: double.infinity,
                  //   height: 300,
                  //   urlPhoto: this.widget.businessModel.photo,
                  // ),

                  _uploadImage(context),
                  EditTextField(
                    icono: Icons.store,
                    label: 'Nombre del negocio',
                    tituloController: controllerName,
                    stateEdit: false,
                  ),
                  EditTextField(
                    icono: Icons.mail,
                    label: 'Email',
                    tituloController: controllerEmail,
                    stateEdit: false,
                  ),
                  EditTextField(
                    icono: Icons.face_outlined,
                    label: 'Nombre del propietario',
                    tituloController: controllerNameOwner,
                    stateEdit: false,
                  ),
                  EditTextField(
                    icono: Icons.phone,
                    label: 'Numero del propietario',
                    tituloController: controllerNumberOwner,
                    stateEdit: false,
                    isNumber: true,
                  ),
                  EditTextField(
                    icono: Icons.timer,
                    label: 'Tiempo de preparacion promedio',
                    tituloController: controllerPreparationTime,
                    stateEdit: false,
                    isNumber: true,
                  ),
                  // Container(
                  //   width: double.infinity,
                  //   height: 300,
                  //   child: _changeGoogleMap(),
                  // ),
                  Container(
                    width: double.infinity,
                    height: 300,
                    child: _changeGoogleMap(),
                  ),
                  CupertinoButton(
                      // child: Text(statusMap ? "Finalizar edicion" : "Editar mapa"),
                      child: Text(
                        "Editar direccion del negocio",
                        style: TextStyle(fontSize: 16),
                      ),
                      color: Colors.green,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                      onPressed: () async {
                        TextEditingController textControllerLatLong =
                            TextEditingController();
                        TextEditingController textControllerDireccion =
                            TextEditingController();
                        TextEditingController textControllerCiudad =
                            TextEditingController();
                        await showDialog(
                          context: context,
                          builder: (context) {
                            return ChangeMapPosition(
                              // controller: _controller,
                              controllerTextLatLong: textControllerLatLong,
                              controllerTextDirection: textControllerDireccion,
                              controllerTextCiudad: textControllerCiudad,
                              positionMap: positionMap,
                            );
                          },
                        );
                        print('=======================');
                        // print(_controller);
                        print(textControllerLatLong.text);
                        print(textControllerDireccion.text);
                        print(textControllerCiudad.text);
                        print('=======================');
                        if (textControllerLatLong.text != '' &&
                            textControllerCiudad.text != '') {
                          controllerDirection.text =
                              textControllerDireccion.text;
                          controllerCity.text = textControllerCiudad.text;
                          positionMap = new CameraPosition(
                              target: LatLng(
                                  double.parse(
                                      textControllerLatLong.text.split(',')[0]),
                                  double.parse(textControllerLatLong.text
                                      .split(',')[1])),
                              zoom: 16);
                          await _moveTo(positionMap);
                          setState(() {});
                        } else {
                          Flushbar(
                            title: "Cancelado",
                            message: "Se cancelo el editado de direccion",
                            backgroundColor: Colors.orange,
                            margin: EdgeInsets.all(8),
                            borderRadius: 8,
                            duration: Duration(seconds: 3),
                            // boxShadows: [
                            //   BoxShadow(
                            //     color: Colors.red[800],
                            //     offset: Offset(0.0, 2.0),
                            //     blurRadius: 3.0,
                            //   )
                            // ],
                          )..show(context);
                        }

                        // await _moveTo(CameraPosition(
                        //     target:
                        //         LatLng(-16.487422106993908, -68.12178684717517),
                        //     zoom: 16));

                        // statusMap = !statusMap;
                        // setState(() {});
                      }),

                  EditTextField(
                    icono: Icons.streetview,
                    label: 'Direccion',
                    tituloController: controllerDirection,
                    stateEdit: false,
                  ),
                  EditTextField(
                    icono: Icons.location_city,
                    label: 'Ciudad',
                    tituloController: controllerCity,
                    stateEdit: false,
                  ),
                  EditTextField(
                    icono: Icons.directions,
                    label: 'Zona',
                    tituloController: controllerZone,
                    stateEdit: false,
                  ),
                  _labelDias(),
                  if (lunes)
                    _scheduleByDay(controller: controllerlunes, day: 'Lunes'),
                  if (martes)
                    _scheduleByDay(controller: controllermartes, day: 'Martes'),
                  if (miercoles)
                    _scheduleByDay(
                        controller: controllermiercoles, day: 'Miercoles'),
                  if (jueves)
                    _scheduleByDay(controller: controllerjueves, day: 'Jueves'),
                  if (viernes)
                    _scheduleByDay(
                        controller: controllerviernes, day: 'Viernes'),
                  if (sabado)
                    _scheduleByDay(controller: controllersabado, day: 'Sabado'),
                  if (domingo)
                    _scheduleByDay(
                        controller: controllerdomingo, day: 'Domingo'),
                  CupertinoButton(
                    child: Text('Guardar cambios'),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    color: Colors.blue,
                    onPressed: () async {
                      updateFirebaseBusiness(context);
                      // await _moveTo(CameraPosition(target: LatLng(-16.487422106993908, -68.12178684717517)))
                    },
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  updateFirebaseBusiness(BuildContext context) {
    print('${positionMap.target.latitude}, ${positionMap.target.longitude}');
    List<String> listaDays = [];
    List<String> listaSchudle = [];
    if (lunes) {
      listaDays.add('Lunes');
      listaSchudle.add(controllerlunes.text);
    }
    if (martes) {
      listaDays.add('Martes');
      listaSchudle.add(controllermartes.text);
    }
    if (miercoles) {
      listaDays.add('Miercoles');
      listaSchudle.add(controllermiercoles.text);
    }
    if (jueves) {
      listaDays.add('Jueves');
      listaSchudle.add(controllerjueves.text);
    }
    if (viernes) {
      listaDays.add('Viernes');
      listaSchudle.add(controllerviernes.text);
    }
    if (sabado) {
      listaDays.add('Sabado');
      listaSchudle.add(controllersabado.text);
    }
    if (domingo) {
      listaDays.add('Domingo');
      listaSchudle.add(controllerdomingo.text);
    }
    if (this.widget.businessModel != null) {
      apiFirebase.updateBusiness(
          idBusiness: this.widget.businessModel.idBusiness,
          city: controllerCity.text,
          direction: controllerDirection.text,
          email: controllerEmail.text,
          name: controllerName.text,
          nameOwner: controllerNameOwner.text,
          numberOwner: controllerNumberOwner.text,
          photo: photo,
          preparationTime: int.parse(controllerPreparationTime.text),
          ubication:
              '${positionMap.target.latitude}, ${positionMap.target.longitude}',
          zone: controllerZone.text,
          days: listaDays,
          schedule: listaSchudle);
    } else {
      apiFirebase.createBusiness(
          city: controllerCity.text,
          direction: controllerDirection.text,
          email: controllerEmail.text,
          name: controllerName.text,
          nameOwner: controllerNameOwner.text,
          numberOwner: controllerNumberOwner.text,
          photo: photo,
          preparationTime: int.parse(controllerPreparationTime.text),
          ubication:
              '${positionMap.target.latitude}, ${positionMap.target.longitude}',
          zone: controllerZone.text,
          days: listaDays,
          schedule: listaSchudle);
    }

    apiFirebase.readRestaurants(providerRestaurant);
    Navigator.pop(context);
  }

  bool statusMap = false;
  // _changeGoogleMap() {
  //   return Stack(
  //     alignment: Alignment.center,
  //     children: [
  //       if (positionMap != null)
  //         GoogleMap(
  //           mapType: MapType.normal,
  //           initialCameraPosition: positionMap,
  //           rotateGesturesEnabled: false,
  //           myLocationButtonEnabled: true,
  //           myLocationEnabled: true,
  //           zoomControlsEnabled: false,
  //           scrollGesturesEnabled: true,
  //           onCameraIdle: () {
  //             // print('hhhhhhhhh $positionMap');
  //             print('========= $banderaEdit ');

  //             _obtenerDireccion(
  //                 positionMap.target.latitude, positionMap.target.longitude);
  //             // if (!banderaEdit) {
  //             //   banderaEdit = true;
  //             // }
  //           },
  //           onCameraMove: (value) {
  //             // print("###### $value");
  //             positionMap = value;
  //           },
  //           onCameraMoveStarted: () {
  //             // print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
  //           },
  //           onMapCreated: (GoogleMapController controller) {
  //             _controller.complete(controller);
  //             // _controllerGoogleMaps = controller;
  //             controller.setMapStyle(dataMapStyle);
  //           },

  //           // scrollGesturesEnabled: statusMap,
  //           // indoorViewEnabled: true,
  //           // markers: _markers,
  //           // initialCameraPosition: kGooglePlex,
  //           // onMapCreated: (GoogleMapController controller) {
  //           //   _controller.complete(controller);
  //           //   mapController = controller;

  //           // },
  //           gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{}
  //             ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
  //             ..add(
  //               Factory<VerticalDragGestureRecognizer>(
  //                   () => VerticalDragGestureRecognizer()),
  //             )
  //             ..add(
  //               Factory<HorizontalDragGestureRecognizer>(
  //                   () => HorizontalDragGestureRecognizer()),
  //             )
  //             ..add(
  //               Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()),
  //             ),
  //         ),
  //       Image.asset(
  //         'assets/icon/marker.png',
  //         width: 40,
  //       ),
  //       Positioned(
  //         bottom: 0,
  //         right: 0,
  //         child: CupertinoButton(
  //             // child: Text(statusMap ? "Finalizar edicion" : "Editar mapa"),
  //             child: Text(
  //               "Mueve el mapa para editar la ubicacion",
  //               style: TextStyle(fontSize: 16),
  //             ),
  //             color: Colors.green,
  //             padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
  //             onPressed: () {
  //               statusMap = !statusMap;
  //               setState(() {});
  //             }),
  //       ),
  //     ],
  //   );
  // }

  _changeGoogleMap() {
    return Stack(
      alignment: Alignment.center,
      children: [
        if (positionMap != null)
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 5)]),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: GoogleMap(
                mapType: MapType.normal,
                initialCameraPosition: positionMap,
                rotateGesturesEnabled: false,
                myLocationButtonEnabled: false,
                myLocationEnabled: false,
                zoomControlsEnabled: false,
                scrollGesturesEnabled: false,
                zoomGesturesEnabled: false,

                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                  // _controllerGoogleMaps = controller;
                  controller.setMapStyle(dataMapStyle);
                  //await controller.animateCamera(CameraUpdate.newCameraPosition(position));
                },

                // onCameraIdle: () {
                //   // print('hhhhhhhhh $positionMap');
                //   print('========= $banderaEdit ');

                //   _obtenerDireccion(
                //       positionMap.target.latitude, positionMap.target.longitude);
                //   // if (!banderaEdit) {
                //   //   banderaEdit = true;
                //   // }
                // },
                // onCameraMove: (value) {
                //   // print("###### $value");
                //   positionMap = value;
                // },
                // onCameraMoveStarted: () {
                //   // print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                // },
                // onMapCreated: (GoogleMapController controller) {
                //   _controller.complete(controller);
                //   // _controllerGoogleMaps = controller;
                //   controller.setMapStyle(dataMapStyle);
                // },

                // scrollGesturesEnabled: statusMap,
                // indoorViewEnabled: true,
                // markers: _markers,
                // initialCameraPosition: kGooglePlex,
                // onMapCreated: (GoogleMapController controller) {
                //   _controller.complete(controller);
                //   mapController = controller;

                // },
                gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{}
                  ..add(Factory<PanGestureRecognizer>(
                      () => PanGestureRecognizer()))
                  ..add(Factory<VerticalDragGestureRecognizer>(
                      () => VerticalDragGestureRecognizer()))
                  ..add(Factory<HorizontalDragGestureRecognizer>(
                      () => HorizontalDragGestureRecognizer()))
                  ..add(
                    Factory<ScaleGestureRecognizer>(
                        () => ScaleGestureRecognizer()),
                  ),
              ),
            ),
          ),
        Image.asset(
          'assets/icon/marker.png',
          width: 40,
        ),
        Container(
          color: Colors.transparent,
        )
        // Positioned(
        //   bottom: 0,
        //   right: 0,
        //   child: CupertinoButton(
        //       // child: Text(statusMap ? "Finalizar edicion" : "Editar mapa"),
        //       child: Text(
        //         "Mueve el mapa para editar la ubicacion",
        //         style: TextStyle(fontSize: 16),
        //       ),
        //       color: Colors.green,
        //       padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
        //       onPressed: () {
        //         statusMap = !statusMap;
        //         setState(() {});
        //       }),
        // ),
      ],
    );
  }

  _scheduleByDay({TextEditingController controller, String day}) {
    return GestureDetector(
      onTap: () {
        // showTimeRangePicker(
        //   this.context,
        //   controller,
        // );
        if (controller.text.length > 0) {
          showTimeRangePicker(
            this.context,
            controller,
            initialFromHour:
                int.parse(controller.text.split('-')[0].split(':')[0]),
            initialFromMinutes:
                int.parse(controller.text.split('-')[0].split(':')[1]),
            initialToHour:
                int.parse(controller.text.split('-')[1].split(':')[0]),
            initialToMinutes:
                int.parse(controller.text.split('-')[1].split(':')[1]),
          );
        } else {
          showTimeRangePicker(
            this.context,
            controller,
          );
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        margin: EdgeInsets.symmetric(vertical: 2.5),
        child: Column(
          children: [
            Text(
              "Horario del día $day: ",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
            ),
            // Text(
            //   "${controller.text.split('-')[0].split(':')} - ${controller.text.split('-')[1].split(':')}",
            //   style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
            // ),
            if (controller.text.length > 0)
              Text(
                "${controller.text.replaceAll('-', ' - ')}",
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.w200),
              ),
            if (controller.text.length == 0)
              Text(
                "00:00 - 00:00",
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.w200),
              ),
          ],
        ),
      ),
    );
  }

  Future<void> showTimeRangePicker(
    BuildContext context,
    TextEditingController controller, {
    int initialFromHour = 8,
    int initialFromMinutes = 0,
    int initialToHour = 20,
    int initialToMinutes = 0,
  }) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Escoge el horario"),
            content: TimeRangePicker(
              initialFromHour: initialFromHour,
              initialFromMinutes: initialFromMinutes,
              initialToHour: initialToHour,
              initialToMinutes: initialToMinutes,
              backText: "Atraz",
              nextText: "Siguiente",
              cancelText: "Cancelar",
              selectText: "Seleccionar",
              tabFromText: "Desde",
              tabToText: "Hasta",
              editable: false,
              is24Format: true,
              disableTabInteraction: true,
              iconCancel: Icon(Icons.cancel_presentation, size: 12),
              iconNext: Icon(Icons.arrow_forward, size: 12),
              iconBack: Icon(Icons.arrow_back, size: 12),
              iconSelect: Icon(Icons.check, size: 12),
              separatorStyle: TextStyle(color: Colors.grey[900], fontSize: 30),
              onSelect: (from, to) {
                print(
                    '_______ From : ${from.hour.toString().padLeft(2, "0")}:${from.minute.toString().padLeft(2, "0")}, To : ${to.hour.toString().padLeft(2, "0")}:${to.minute.toString().padLeft(2, "0")} ________');
                controller.text =
                    '${from.hour.toString().padLeft(2, "0")}:${from.minute.toString().padLeft(2, "0")}-${to.hour.toString().padLeft(2, "0")}:${to.minute.toString().padLeft(2, "0")}';
                setState(() {});
                Navigator.pop(context);
              },
              onCancel: () => Navigator.pop(context),
              indicatorColor: Colors.green,
              activeLabelColor: Colors.green,
              selectedTimeStyle: TextStyle(color: Colors.green, fontSize: 30),
            ),
          );
        });
  }

  _obtenerDireccion(double lat, double long) async {
    if (banderaEdit) {
      banderaEdit = !banderaEdit;
    } else {
      placemarks = await placemarkFromCoordinates(lat, long);

      controllerCity.text = placemarks[0].locality;
      controllerDirection.text = placemarks[0].street;
      if (controllerDirection.text.contains('Unnamed')) {
        controllerDirection.text = '';
      }
    }
    // controllerDirecction.text = "${placemarks[0].locality}, ${placemarks[0].street}";
    // _directionController.text = "${placemarks[0].street}";
    // _ciudadController.text = placemarks[0].locality;
    setState(() {});
  }

  _labelDias() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
      width: double.infinity,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 25, top: 10),
            child: Text(
              "¿Cual es el horario del negocio?",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Lun"),
                    Checkbox(
                      value: lunes,
                      activeColor: Colors.orange,
                      onChanged: (bool value) {
                        setState(() {
                          lunes = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Tuesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Mar"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: martes,
                      onChanged: (bool value) {
                        setState(() {
                          martes = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Wednesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Mie"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: miercoles,
                      onChanged: (bool value) {
                        setState(() {
                          miercoles = value;
                        });
                      },
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Jue"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: jueves,
                      onChanged: (bool value) {
                        setState(() {
                          jueves = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Tuesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Vie"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: viernes,
                      onChanged: (bool value) {
                        setState(() {
                          viernes = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Wednesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Sab"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: sabado,
                      onChanged: (bool value) {
                        setState(() {
                          sabado = value;
                        });
                      },
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Dom"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: domingo,
                      onChanged: (bool value) {
                        setState(() {
                          domingo = value;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  ///////////////////////////////////////////////////

  ///[Upload Image]
  File _imageFile;
  String photo;

  ///NOTE: Only supported on Android & iOS
  ///Needs image_picker plugin {https://pub.dev/packages/image_picker}
  final picker = ImagePicker();

  Future pickImage(BuildContext context) async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 15);
    _actionsPickImage(pickedFile, context);
  }

  Future pickGallery(BuildContext context) async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 15);
    _actionsPickImage(pickedFile, context);
  }

  _actionsPickImage(PickedFile pickedFile, BuildContext context) {
    setState(() {
      try {
        _imageFile = File(pickedFile.path);
        uploadImageToFirebase(context, controllerName.text);
      } catch (e) {}
    });
  }

  Future<String> uploadImageToFirebase(
      BuildContext context, String carpeta) async {
    String fileName = basename(_imageFile.path);
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('$carpeta/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
    TaskSnapshot taskSnapshot = await uploadTask;
    print("################# Done: xx");

    return taskSnapshot.ref.getDownloadURL().then(
      (value) {
        print("################# Done: $value");
        photo = value;
        // setState(() { });
        return value.toString();
      },
    );
    // return photo;
  }

  // Widget cargando = Text("Presione aqui para subir");
  _uploadImage(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Text("Suba una imagen de su producto"),
          // SizedBox(
          //   height: 5,
          // ),
          Stack(
            // mainAxisAlignment: MainAxisAlignment.center,
            alignment: photo != null ? Alignment.bottomRight : Alignment.center,
            children: [
              Container(
                width: double.infinity,
                height: 300,
                child: _imageFile != null
                    ? Image.file(
                        _imageFile,
                        fit: BoxFit.cover,
                      )
                    : CacheImageWidget(
                        width: double.infinity,
                        height: 300,
                        urlPhoto: photo != null
                            ? photo
                            : 'https://cdn.iconscout.com/icon/free/png-256/no-image-1771002-1505134.png',
                      ),
              ),
              Row(
                mainAxisAlignment: photo != null
                    ? MainAxisAlignment.end
                    : MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                    color: Colors.white54,
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.add_a_photo,
                      size: 40,
                    ),
                    onPressed: () => pickImage(context),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  // ignore: deprecated_member_use
                  RaisedButton(
                    color: Colors.white54,
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.image_search,
                      size: 40,
                    ),
                    onPressed: () => pickGallery(context),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
