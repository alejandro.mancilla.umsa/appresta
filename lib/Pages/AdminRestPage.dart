import 'dart:ui';

import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_lifecycle_base/base_state_widget.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Core/Models/BusinessModel.dart';
import 'package:restorant_app/Core/Models/ProductModelUnit.dart';
import 'package:restorant_app/Pages/AddNewProduct.dart';
import 'package:restorant_app/Pages/EditPageBusiness.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
// import 'package:restorant_app/Rest/Endpoints.dart';
import 'package:restorant_app/Widgets/AppBarWidget.dart';
import 'package:restorant_app/Widgets/ButtomBarWidget.dart';
import 'package:restorant_app/Widgets/CardPlato.dart';
import 'package:restorant_app/Widgets/ConectInternet.dart';
import 'package:restorant_app/Widgets/ItemPedido.dart';

class AdminRestPage extends BaseStatefulWidget {
  final BusinessModel businessModel;
  final bool status;

  AdminRestPage({this.businessModel, this.status = false});

  @override
  _AdminRestPageState createState() => _AdminRestPageState();
}

class _AdminRestPageState extends BaseWidgetState<AdminRestPage> {
  final ApiFirebase apiFirebase = ApiFirebase();
  ProviderListPlatos providerListPlatos;

  String zone = '';
  String city = '';

  String coord = 'xx';
  DateTime today = DateTime.now();
  DateTime minfech;
  int diasCalendar = 365;

  @override
  void initState() {
    // _cargarciudad();
    providerListPlatos =
        Provider.of<ProviderListPlatos>(context, listen: false);
    minfech = DateTime.utc(
        today.year, today.month, today.day - diasCalendar, 4, 0, 0, 0, 0);
    minfech = minfech.toLocal();
    // apiFirebase.disposeListenReadProduct();
    _cargarPlatos();
    _cargarOrdenes();
    super.initState();
    print(
        '================= initState ======== Estado de inicio ==============');
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    providerListPlatos = Provider.of<ProviderListPlatos>(context, listen: true);
    print(
        '================= didChangeDependencies ======== cambió las dependencias = ${providerListPlatos.listaOrders}=============');
  }

  @override
  void didAppear() {
    apiFirebase.disposeListenReadProduct();
    apiFirebase.disposeListenReadOrders();

    _cargarOrdenes();
    _cargarPlatos();
    print('================= didAppear ========= apareció =============');
    super.didAppear();
  }

  @override
  void disappear() {
    apiFirebase.disposeListenReadProduct();
    apiFirebase.disposeListenReadOrders();
    // _cargarPlatos();
    print('================= disappear ========== desaparecer ============');
    super.disappear();
  }

  @override
  void onForeground() {
    apiFirebase.disposeListenReadProduct();
    apiFirebase.disposeListenReadOrders();
    _cargarPlatos();

    _cargarOrdenes();
    print(
        '================= onForeground ======= en primer plano ===============');
    super.onForeground();
  }

  @override
  void onBackground() {
    apiFirebase.disposeListenReadProduct();
    apiFirebase.disposeListenReadOrders();
    // _cargarPlatos();
    print('================= onBackground ======= en el fondo ===============');
    super.onBackground();
  }

  @override
  void dispose() {
    apiFirebase.disposeListenReadProduct();
    providerListPlatos.listaProducts = [];
    providerListPlatos.listaOrders = [];
    // _cargarPlatos();
    print('================= dispose ====== disponer ================');
    super.dispose();
  }

  _cargarPlatos() async {
    apiFirebase.readProducts(
        this.widget.businessModel.idBusiness, providerListPlatos);
  }

  // bool responseOrders;
  bool responseProducts;
  _cargarOrdenes() async {
    apiFirebase.readOrders(
        this.widget.businessModel.idBusiness, providerListPlatos,
        state: stateFirebase, dateNow: timeNowFirebase);
    // if (responseOrders != null) {
    //   setState(() {});
    // }
  }

  @override
  Widget build(BuildContext context) {
    ProviderListPlatos _listPlatos = Provider.of<ProviderListPlatos>(context);
    int _indexPage = _listPlatos.indexPage;
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            child: AppBarWidget(
              idRest: this.widget.businessModel.idBusiness,
              statusRes: widget.status,
              name: this.widget.businessModel.name,
              statusEmergency: this.widget.businessModel.emergency,
            ),
            preferredSize: Size.fromHeight(100)),
        body: Column(
          children: <Widget>[
            ConectInternet(),
            Expanded(
                child: Container(
              child: _seleccionarBody(_indexPage, context),
            )),
            ButtomBarWidget()
          ],
        ),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     conection.makeGetRequest();
        //   },
        // ),
      ),
    );
  }

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  _seleccionarBody(int index, BuildContext context) {
    switch (index) {
      case 1:
        // return Container(color: Colors.blue,);
        // // RefreshIndicator(
        //   child: Container(
        //     width: 50,
        //     height: 50,
        //   ),
        //   onRefresh: () {
        //     return Future.delayed(Duration(seconds: 3),(){
        //       return;
        //     });
        //   },
        // ),
        return Column(children: [
          DropDownDemo(
            idBusinnes: this.widget.businessModel.idBusiness,
          ),
          Container(
            height: 90,
            child: DatePicker(
              minfech,
              initialSelectedDate: today,
              selectionColor: Colors.black,
              selectedTextColor: Colors.white,
              locale: 'es_ES',
              daysCount: diasCalendar,
              onDateChange: (date) {
                apiFirebase.disposeListenReadOrders();

                // New date selected
                // setState(() {
                //   _selectedValue = date;
                // });
                timeNowFirebase = date;

                firestoreConsultas.readOrders(
                    this.widget.businessModel.idBusiness, providerListPlatos,
                    state: stateFirebase, dateNow: timeNowFirebase);
                // setState(() {

                // });
                print(date);
              },
            ),
          ),
          providerListPlatos.listaOrders != null
              ? providerListPlatos.listaOrders.isNotEmpty
                  ? Expanded(
                      child: SingleChildScrollView(
                          child: Column(
                      children: _listaOrders(),
                    )))
                  : Padding(
                      padding: const EdgeInsets.only(top: 50),
                      child: Text('Aún no tienes pedidos en este dia.'),
                    )
              : Container(
                  margin: EdgeInsets.symmetric(vertical: 40),
                  width: 100,
                  height: 100,
                  child: CircularProgressIndicator(),
                )
        ]

            // _listaOrders().isEmpty
            //     ? [Center(child: CircularProgressIndicator())]
            //     : [
            //         DropDownDemo(
            //           idBusinnes: this.widget.businessModel.idBusiness,
            //         ),
            //         Container(
            //           height: 90,
            //           child: DatePicker(
            //             minfech,
            //             initialSelectedDate: today,
            //             selectionColor: Colors.black,
            //             selectedTextColor: Colors.white,
            //             locale: 'es_ES',
            //             daysCount: diasCalendar,
            //             onDateChange: (date) {
            //               apiFirebase.disposeListenReadOrders();

            //               // New date selected
            //               // setState(() {
            //               //   _selectedValue = date;
            //               // });
            //               timeNowFirebase = date;

            //               firestoreConsultas.readOrders(
            //                   this.widget.businessModel.idBusiness,
            //                   providerListPlatos,
            //                   state: stateFirebase,
            //                   dateNow: timeNowFirebase);
            //               // setState(() {

            //               // });
            //               print(date);
            //             },
            //           ),
            //         ),
            //         ..._listaOrders()
            //       ],
            );
        break;
      case 2:
        return SingleChildScrollView(
          child: Column(
            children: [
              ..._listaPlatos(),
              CupertinoButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('CREAR NUEVO PRODUCTO'),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(Icons.add)
                  ],
                ),
                onPressed: () {
                  //AddNewProduct
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => AddNewProduct(
                                idRest: this.widget.businessModel.idBusiness,
                                name: this.widget.businessModel.name,
                              )));
                },
              )
            ],
          ),
        );
        // return responseProducts != null
        //     ? responseProducts
        //         ? SingleChildScrollView(
        //             child: Column(
        //               children: [
        //                 ..._listaPlatos(),
        //                 CupertinoButton(
        //                   child: Row(
        //                     mainAxisAlignment: MainAxisAlignment.center,
        //                     children: [
        //                       Text('CREAR NUEVO PRODUCTO'),
        //                       SizedBox(
        //                         width: 5,
        //                       ),
        //                       Icon(Icons.add)
        //                     ],
        //                   ),
        //                   onPressed: () {
        //                     //AddNewProduct
        //                     Navigator.push(
        //                         context,
        //                         MaterialPageRoute(
        //                             builder: (BuildContext context) =>
        //                                 AddNewProduct(
        //                                   idRest: this
        //                                       .widget
        //                                       .businessModel
        //                                       .idBusiness,
        //                                   name: this.widget.businessModel.name,
        //                                 )));
        //                   },
        //                 )
        //               ],
        //             ),
        //           )
        //         : Padding(
        //             padding: const EdgeInsets.symmetric(
        //                 horizontal: 15, vertical: 40),
        //             child: Text(
        //               "Aún no tienes pedidos.",
        //               textAlign: TextAlign.center,
        //               style: TextStyle(fontSize: 18, color: Colors.grey),
        //             ),
        //           )
        //     : Center(
        //         child: CircularProgressIndicator(),
        //       );

        // SingleChildScrollView(
        //   child: Column(
        //     children: <Widget>[
        //       ..._listaPlatos(),
        //       CupertinoButton(
        //         child: Row(
        //           mainAxisAlignment: MainAxisAlignment.center,
        //           children: [
        //             Text('CREAR NUEVO PRODUCTO'),
        //             SizedBox(
        //               width: 5,
        //             ),
        //             Icon(Icons.add)
        //           ],
        //         ),
        //         onPressed: () {
        //           //AddNewProduct
        //           // Navigator.push(
        //           //     context,
        //           //     MaterialPageRoute(
        //           //         builder: (BuildContext context) => AddNewProduct(
        //           //               idRest: this.widget.businessModel.idBusiness,
        //           //               name: this.widget.businessModel.name,
        //           //             )));
        //         },
        //       )
        //     ],
        //   ),
        // );
        break;
      case 3:
        return Container(color: Colors.orange);
        break;
      case 4:
        return editBusiness();
        // return _newProduct(this.widget.idRest);
        break;
      default:
    }
  }

  List<Widget> _listaOrders() {
    if (providerListPlatos.listaOrders != null) {
      return providerListPlatos.listaOrders
          .map((e) => ItemPedido(
                orden: e,
                idNegocio: widget.businessModel.idBusiness,
                nameBusiness: widget.businessModel.name,
              ))
          .toList();
    } else {
      return [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 40),
          child: Text(
            "Aún no tienes pedidos.",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 18, color: Colors.grey),
          ),
        )
      ];
    }
  }

  // _auxBodyOne(){
  //   return ListView(
  //     children: <Widget>[
  //       ..._listaPlatos()
  //     ],
  //   );
  // }

  List<Widget> _listaPlatos() {
    if (providerListPlatos.listaProducts != null) {
      return providerListPlatos.listaProducts
          .map((ProductModelUnit productModel) {
        return CardPlato(
          productModel: productModel,
        );
      }).toList();
    } else {
      return [
        Center(
          child: CircularProgressIndicator(),
        )
      ];
    }
  }

  // _listaOrders() {
  //   List<Widget> listaOrdenes = [];

  //   listaOrdenes.add(_textStatus('PEDIDOS EN PROGRESO'));

  //   providerListPlatos.listaOrdersEnProgreso
  //       .forEach((OrderFinalModel orderFinalModel) {
  //     listaOrdenes.add(ItemPedido(
  //       orden: orderFinalModel,
  //       idNegocio: widget.businessModel.idBusiness,
  //       nameBusiness: widget.businessModel.name,
  //     ));
  //   });
  //   listaOrdenes.add(Divider());
  //   listaOrdenes.add(_textStatus('PEDIDOS FINALIZADOS'));
  //   listaOrdenes.add(Divider());
  //   providerListPlatos.listaOrdersFinalizado
  //       .forEach((OrderFinalModel orderFinalModel) {
  //     listaOrdenes.add(ItemPedido(
  //       orden: orderFinalModel,
  //       idNegocio: widget.businessModel.idBusiness,
  //       nameBusiness: widget.businessModel.name,
  //     ));
  //   });
  //   listaOrdenes.add(Divider());
  //   listaOrdenes.add(_textStatus('PEDIDOS CANCELADOS'));
  //   listaOrdenes.add(Divider());
  //   providerListPlatos.listaOrdersCancelado
  //       .forEach((OrderFinalModel orderFinalModel) {
  //     listaOrdenes.add(ItemPedido(
  //       orden: orderFinalModel,
  //       idNegocio: widget.businessModel.idBusiness,
  //       nameBusiness: widget.businessModel.name,
  //     ));
  //   });

  //   return listaOrdenes;
  // }

  // Widget _textStatus(String data) {
  //   return Container(
  //     margin: EdgeInsets.symmetric(vertical: 15),
  //     alignment: Alignment.center,
  //     child: Text(
  //       data,
  //       style: TextStyle(
  //           fontWeight: FontWeight.bold, fontSize: 18, color: Colors.blueGrey),
  //     ),
  //   );
  // }

  // // _cerrarCesion(BuildContext context)async{
  // //   final _firebaseAuth = FirebaseAuth.instance;
  // //    await _firebaseAuth.signOut();
  // //   Stream<User> usuario =  _firebaseAuth.authStateChanges();// onAuthStateChanged;
  // //   User nu = await usuario.first;
  // //   print(nu??"no existe");
  // //   if(nu == null){
  // //     Navigator.push(context, MaterialPageRoute(
  // //         builder: (context) => LoginScreen()
  // //     ));
  // //   }else{
  // //     print("Error Al Cerrar Session");
  // //   }
  // // }
  // //

  editBusiness() {
    return EditPageBusiness(businessModel: this.widget.businessModel);
  }
}

DateTime timeNowFirebase = DateTime.now();
String stateFirebase = 'EN PROGRESO';
ApiFirebase firestoreConsultas = ApiFirebase();

void _leerTipoPedido(
    String value, ProviderListPlatos ordenProvider, String idBusiness) async {
  // ordenProvider.listaOrdenes = [];
  // List<OrderFinalModel> listaOrdenes= [];
  if (value == 'Pedidos Actuales') {
    stateFirebase = 'EN PROGRESO';
  } else if (value == 'Pedidos Finalizados') {
    stateFirebase = 'FINALIZADO';
  } else if (value == 'Pedidos Cancelados') {
    stateFirebase = 'CANCELADO';
  }

  Future.delayed(Duration.zero, () {
    firestoreConsultas.readOrders(idBusiness, ordenProvider,
        state: stateFirebase, dateNow: timeNowFirebase);
  });
  // ordenProvider.listaOrdenes = listaOrdenes;
  // listaOrdenes.forEach((element) {

  // });
}

class DropDownDemo extends StatefulWidget {
  final String idBusinnes;

  const DropDownDemo({Key key, this.idBusinnes}) : super(key: key);
  @override
  _DropDownDemoState createState() => _DropDownDemoState();
}

class _DropDownDemoState extends State<DropDownDemo> {
  String _chosenValue = 'Pedidos Actuales';
  ProviderListPlatos ordenProvider;
  @override
  void initState() {
    ordenProvider = Provider.of<ProviderListPlatos>(context, listen: false);
    // _leerTipoPedido(_chosenValue,ordenProvider);
    super.initState();
  }

  // @override
  // void didChangeDependencies() {
  //   ordenProvider = Provider.of<OrdenProvider>(context, listen: true);
  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueGrey[900],
      width: double.infinity,
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Mostrar los ',
              style: TextStyle(
                color: Colors.white,
              )),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              height: 35,
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.3),
                  borderRadius: BorderRadius.circular(5)),
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.only(right: 5, left: 5),
              child: DropdownButton<String>(
                elevation: 1,
                underline: Container(),
                value: _chosenValue,
                dropdownColor: Colors.blueGrey[900],
                icon: Icon(
                  Icons.arrow_drop_down_outlined,
                  color: Colors.white70,
                ),
                //elevation: 5,
                style: TextStyle(
                    color: Colors.white70, fontWeight: FontWeight.bold),

                items: <String>[
                  'Pedidos Actuales',
                  'Pedidos Finalizados',
                  'Pedidos Cancelados'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                hint: Text(
                  'Seleccione el tipo de pedido',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),

                onChanged: (String value) {
                  setState(() {
                    _chosenValue = value;
                    print('#######');
                    _leerTipoPedido(
                        _chosenValue, ordenProvider, widget.idBusinnes);
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
