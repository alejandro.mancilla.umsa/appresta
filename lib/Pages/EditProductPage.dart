import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/ApiConection.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:restorant_app/Core/Models/ProductModelUnit.dart';
import 'package:restorant_app/Pages/EditPageUnit.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/main.dart';

class EditProductPage extends StatefulWidget {
  final ProductModelUnit productModel;

  const EditProductPage({Key key, this.productModel}) : super(key: key);

  @override
  _EditProductPageState createState() => _EditProductPageState();
}

class _EditProductPageState extends State<EditProductPage> {
  ApiFirebase apiFirebase = ApiFirebase();
  ProviderListPlatos providerListPlatos;

  @override
  void initState() {
    providerListPlatos =
        Provider.of<ProviderListPlatos>(context, listen: false);
    super.initState();
  }

  // // _cargarciudad()async{
  // //   CityResp resp = await  api.getCityDate(this.widget.city);
  // //   // print(resp);
  // //   // print(resp.name);
  // //   controllerCiudad.text = resp.name;
  // //   resp.zones.forEach((element) {
  // //     // print("## ${element.id} = ${this.widget.zone} ##");
  // //     if(element.id == this.widget.zone){
  // //       controllerZone.text = element.name;
  // //       // print(element.id);
  // //       // print(element.name);
  // //     }
  // //   });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        // margin: EdgeInsets.symmetric(horizontal: 40,vertical: 60),
        padding: EdgeInsets.all(30),
        alignment: Alignment.center,
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                this.widget.productModel.name,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Container(
                  width: MediaQuery.of(context).size.width - 120,
                  height: MediaQuery.of(context).size.width - 120,
                  child: Image.network(
                    this.widget.productModel.photo,
                    fit: BoxFit.contain,
                    width: double.infinity,
                  )),
              Row(
                children: [
                  Expanded(
                    // width: double.infinity,
                    child: CupertinoButton(
                      onPressed: () async {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    EdidProductPageUnit(
                                      producto: this.widget.productModel,
                                    )));
                      },
                      color: Colors.green,
                      child: Text("Editar"),
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    // width: double.infinity,
                    child: CupertinoButton(
                      onPressed: () {
                        _showDialog(
                            this.widget.productModel.idProduct, context);
                        // apiFirebase.deleteProduct(this.widget.productModel.idProduct);
                      },
                      color: Colors.red,
                      child: Text("Eliminar"),
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
      ),
    );
  }

  void _showDialog(String idPlato, BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alerta"),
          content: new Text("Estas seguro que quieres eliminar el plato?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            // ignore: deprecated_member_use
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
                print("Bien Echo");
                Flushbar(
                  title: "OK",
                  message: "Que bueno que NO eliminaste el plato",
                  backgroundColor: Colors.orange,
                  margin: EdgeInsets.all(8),
                  borderRadius: 8,
                  duration: Duration(seconds: 3),
                  boxShadows: [
                    BoxShadow(
                      color: Colors.red[800],
                      offset: Offset(0.0, 2.0),
                      blurRadius: 3.0,
                    )
                  ],
                )..show(context);
              },
            ),
            // usually buttons at the bottom of the dialog
            // ignore: deprecated_member_use
            new FlatButton(
              child: new Text("Si"),
              onPressed: () async {
                // Navigator.of(context).pop();
                apiFirebase.deleteProduct(idPlato);
                // apiFirebase.readProducts(
                //     this.widget.productModel.idBusiness, providerListPlatos);
                Flushbar(
                  title: "OK",
                  message: "Producto eliminado correctamente",
                  backgroundColor: Colors.orange,
                  margin: EdgeInsets.all(8),
                  borderRadius: 8,
                  duration: Duration(seconds: 2),
                  // boxShadows: [
                  //   BoxShadow(
                  //     color: Colors.red[800],
                  //     offset: Offset(0.0, 2.0),
                  //     blurRadius: 3.0,
                  //   )
                  // ],
                )..show(context);

                Future.delayed(Duration(milliseconds: 2001), () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  // RestartWidget.restartApp(context);
                });
              },
            ),
          ],
        );
      },
    );
  }
}
