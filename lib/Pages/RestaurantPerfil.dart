// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:google_geocoding/google_geocoding.dart';
// import 'package:restorant_app/Api/ApiConection.dart';
// import 'package:restorant_app/Providers/ProviderListPlatos.dart';
// import 'package:restorant_app/Providers/ProviderRestaurant.dart';
// class RestaurantPerfil extends StatefulWidget {
//   final String description;
//   final String slogan;
//   final String horary;
//   final int    percent;
//   final String name;
//   final String photo;
//   final String ubicacion;
//   final String preparationTime;
//   final String categori;
//   final String numberOwner;
//   final String horario;
//   final String zone;
//   final String city;

//   final List<String> tags;

//   const RestaurantPerfil({Key key, this.description, this.slogan, this.horary, this.percent, this.name, this.photo, this.ubicacion, this.preparationTime, this.categori, this.numberOwner, this.horario, this.tags, this.zone, @required this.city}) : super(key: key);
//   @override
//   _RestaurantPerfilState createState() => _RestaurantPerfilState();
// }

// class _RestaurantPerfilState extends State<RestaurantPerfil> {
//   ProviderListPlatos _listPlatos ;
//   ProviderRestaurant providerRestaurant ;
//   String idRestaurant;
//   @override
//   void initState() {
//     providerRestaurant = Provider.of<ProviderRestaurant>(context,listen: false);
//     _listPlatos = Provider.of<ProviderListPlatos>(context, listen: false);
//     super.initState();
//     print("""
//     description = ${this.widget.description}
//     slogan = ${this.widget.slogan}
//     horary = ${this.widget.horary}
//     percent = ${this.widget.percent}
//     name = ${this.widget.name}
//     photo = ${this.widget.photo}
//     ubicacion = ${this.widget.ubicacion}
//     city = ${this.widget.city}
//     vzone = ${this.widget.zone}
//     """);
//     controllerPorcentaje.text = this.widget.percent.toString();
//     controllerCategoria.text = 'Restaurant';
//     controllername.text = this.widget.name;
//     controllerNumberOwner.text = this.widget.numberOwner ;
//     controllerphoto.text = this.widget.photo ;
//     controllerPreparationTime.text = this.widget.preparationTime ;
//     controllerType.text = this.widget.tags.toString().replaceAll("[", "").replaceAll("]", "") ;
//     controllerUbication.text = this.widget.ubicacion ;
//     controllerSchudle.text = this.widget.horary;

//     controllerCiudad.text = this.widget.city;
//     controllerZone.text = this.widget.zone ;

//     controllerDireccion.text = 'Sin informacion';
//     controllerEmail.text = 'Sin Informacion';
//     controllernameOwner.text = 'Anonimo';
//     _cargandoDireccion(this.widget.ubicacion);

//   }

//   // _cargarciudad()async{
//   //   CityResp resp = await  api.getCityDate(this.widget.city);
//   //   // print(resp);
//   //   // print(resp.name);
//   //   controllerCiudad.text = resp.name;
//   //   resp.zones.forEach((element) {
//   //     // print("## ${element.id} = ${this.widget.zone} ##");
//   //     if(element.id == this.widget.zone){
//   //       controllerZone.text = element.name;
//   //       // print(element.id);
//   //       // print(element.name);
//   //     }
//   //   });

//   //   setState(() {

//   //   });
//   // }
//   _cargandoDireccion(String coord)async{
//     List cordenates = this.widget.ubicacion.replaceAll(" ", "").split(',');
//     print(cordenates);
//     risult = await googleGeocoding.geocoding.getReverse(LatLon(double.parse(cordenates[0]),double.parse(cordenates[1])));
//     // risult.results.forEach((GeocodingResult element) {
//     //   print("""
//     //   -------------------------------------------------
//     //   risult = ${element.addressComponents}
//     //   risult = ${element.formattedAddress}
//     //   risult = ${element.geometry}
//     //   risult = ${element.partialMatch}
//     //   risult = ${element.placeId}
//     //   risult = ${element.plusCode}
//     //   risult = ${element.postcodeLocalities}
//     //   risult = ${element.types}
//     //   -------------------------------------------------
//     //   """);
//     // });
//     controllerDireccion.text = risult.results[0].formattedAddress;
//     setState(() {

//     });
//   }
//   GoogleGeocoding googleGeocoding = GoogleGeocoding("AIzaSyAtgAQ9Kzo-YkxZG_hAWuJNf16lMwyiJEI");
//   GeocodingResponse risult ;// = await googleGeocoding.geocoding.getReverse(LatLon(40.714224,-73.961452));

//   bool lunes    =false;
//   bool martes   =false;
//   bool miercoles=false;
//   bool jueves   =false;
//   bool viernes  =false;
//   bool sabado   =false;
//   bool domingo  =false;
//   TextEditingController controllerPorcentaje = new TextEditingController();
//   TextEditingController controllerCategoria = new TextEditingController();
//   TextEditingController controllerCiudad = new TextEditingController();
//   TextEditingController controllerDireccion = new TextEditingController();
//   TextEditingController controllerEmail = new TextEditingController();
//   TextEditingController controllername = new TextEditingController();
//   TextEditingController controllernameOwner = new TextEditingController();
//   TextEditingController controllerNumberOwner = new TextEditingController();
//   TextEditingController controllerphoto = new TextEditingController();
//   TextEditingController controllerPreparationTime = new TextEditingController();
//   TextEditingController controllerType = new TextEditingController();
//   TextEditingController controllerUbication = new TextEditingController();
//   TextEditingController controllerZone = new TextEditingController();
//   TextEditingController controllerSchudle = new TextEditingController();

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.grey.withOpacity(0.2),
//       child: SingleChildScrollView(
//         child: Column(
//           children: [
//             _EditTextField(
//               title: "porsentaje del negocio:",
//               isNumber: true,
//               tituloController: controllerPorcentaje,
//             ),

//             _EditTextField(
//               title: "categoria",
//               tituloController: controllerCategoria,
//             ),

//             _EditTextField(
//               title: "Ciudad",
//               tituloController: controllerCiudad,
//             ),

//             _labelDias(),

//             _EditTextField(
//               title: "direccion",
//               tituloController: controllerDireccion,
//             ),

//             _EditTextField(
//               title: "email",
//               tituloController: controllerEmail,
//             ),

//             // emergenci

//             // googleID

//             // idNegocio

//             _EditTextField(
//               title: "Nombre de negocio",
//               tituloController: controllername,
//             ),

//             _EditTextField(
//               title: "Owner",
//               tituloController: controllernameOwner,
//             ),

//             _EditTextField(
//               title: "number Owner",
//               isNumber: true,
//               tituloController: controllerNumberOwner,
//             ),

//             _EditTextField(
//               title: "photo",
//               tituloController: controllerphoto,
//             ),

//             _EditTextField(
//               title: "preparation time",
//               isNumber: true,
//               tituloController: controllerPreparationTime,
//             ),

//             _EditTextField(
//               title: "schudle",
//               tituloController: controllerSchudle,
//             ),

//             //schedule

//             // _EditTextField(
//             //   title: "token",
//             // ),

//             _EditTextField(
//               title: "type tags por ,",
//               tituloController: controllerType,
//             ),

//             _EditTextField(
//               title: "ubicacion",
//               tituloController: controllerUbication,
//             ),

//             _EditTextField(
//               title: "zone",
//               tituloController: controllerZone,
//             ),

//             FloatingActionButton.extended(
//               onPressed: (){
//                 print("object");
//                   CollectionReference restaurante = FirebaseFirestore.instance.collection('Business');

//                   List<String> listaDias = [];
//                   List<String> listaHorarios = [];

//                   if(lunes)listaDias.add("Lunes");
//                   if(martes)listaDias.add("Martes");
//                   if(miercoles)listaDias.add("Miercoles");
//                   if(jueves)listaDias.add("Jueves");
//                   if(viernes)listaDias.add("Viernes");
//                   if(sabado)listaDias.add("Sabado");
//                   if(domingo)listaDias.add("Domingo");

//                   if(lunes)     listaHorarios.add(controllerSchudle.text);
//                   if(martes)    listaHorarios.add(controllerSchudle.text);
//                   if(miercoles) listaHorarios.add(controllerSchudle.text);
//                   if(jueves)    listaHorarios.add(controllerSchudle.text);
//                   if(viernes)   listaHorarios.add(controllerSchudle.text);
//                   if(sabado)    listaHorarios.add(controllerSchudle.text);
//                   if(domingo)   listaHorarios.add(controllerSchudle.text);

//                   print("""
//                     ${controllerPorcentaje.text}
//                     ${controllerCategoria.text}
//                     ${controllerCiudad.text}
//                      $listaDias
//                     ${controllerDireccion.text}
//                     ${controllerEmail.text}
//                     ${controllername.text}
//                     ${controllernameOwner.text}
//                     ${controllerNumberOwner.text}
//                     ${controllerphoto.text}
//                     ${controllerPreparationTime.text}
//                      $listaHorarios
//                     ${controllerType.text.split(",")}
//                     ${controllerUbication.text}
//                     ${controllerZone.text}
//                   """);
//                   restaurante
//                   .add(
//                     {
//                     'businessPercentage': int.parse(controllerPorcentaje.text),
//                     'category': controllerCategoria.text,
//                     'city': controllerCiudad.text,
//                     'days': listaDias,
//                     'direction': controllerDireccion.text,
//                     'email': controllerEmail.text,
//                     'emergency': false,
//                     'googleId':'--',
//                     'idBusiness':'--',
//                     'name':controllername.text,
//                     'nameOwner':controllernameOwner.text,
//                     'numberOwner':controllerNumberOwner.text,
//                     'photo':controllerphoto.text,
//                     'preparationTime': int.parse(controllerPreparationTime.text),
//                     'schedule': listaHorarios,
//                     'token': '--',
//                     'type': controllerType.text.replaceAll(" ", "").split(','),
//                     'ubication':controllerUbication.text,
//                     'zone': controllerZone.text
//                   }
//                   )
//                   .then((value) {
//                       print("PLATO Added $value " );
//                       idRestaurant = FirebaseFirestore.instance.collection('Businesss').doc(value.id).id;
//                       providerRestaurant.idRestaurante = idRestaurant;
//                       print("""
//                       --------------
//                       $idRestaurant
//                       ------------
//                       """);
//                       FirebaseFirestore.instance.collection('Business').doc(value.id)
//                       .update({'idBusiness': value.id})
//                       .then((value) {
//                         print("Negocio Updated");
//                         Flushbar(
//                           title: "Aceptado",
//                           message: "Negocio agregado correctamente",
//                           backgroundColor: Colors.orange,
//                           margin: EdgeInsets.all(8),
//                           borderRadius: 8,
//                           duration: Duration(seconds: 2),
//                           boxShadows: [BoxShadow(color: Colors.red[800], offset: Offset(0.0, 2.0), blurRadius: 3.0,)],
//                         )..show(context);
//                         Future.delayed(Duration(milliseconds: 2100),(){
//                           // Navigator.of(context).pop();
//                           // _listPlatos = Provider.of<ProviderListPlatos>(context, listen: true);
//                           _listPlatos.indexPage = 1;
//                         });
//                       })
//                       .catchError((error) => print("Failed to update Negocio: $error"));
//                   } )
//                   .catchError((error) => print("Failed to add user: $error"));

//               },
//               label: Text("data")
//             )
//           ],
//         ),
//       )
//     );
//   }

//   _labelDias(){
//     return Container(
//       padding: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
//       width: double.infinity,
//       child: Column(
//         children: [
//           Text("Dias disponible del Negocio:"),
//           SingleChildScrollView(
//             scrollDirection: Axis.horizontal,
//             child: Row(
//               children: [
//                  Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Lun"),
//                     Checkbox(
//                       value: lunes,
//                       activeColor: Colors.orange,
//                       onChanged: (bool value) {
//                         setState(() {
//                           lunes = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Tuesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Mar"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: martes,
//                       onChanged: (bool value) {
//                         setState(() {
//                           martes = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Wednesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Mie"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: miercoles,
//                       onChanged: (bool value) {
//                         setState(() {
//                           miercoles = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Jue"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: jueves,
//                       onChanged: (bool value) {
//                         setState(() {
//                           jueves = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Tuesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Vie"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: viernes,
//                       onChanged: (bool value) {
//                         setState(() {
//                           viernes = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Wednesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Sab"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: sabado,
//                       onChanged: (bool value) {
//                         setState(() {
//                           sabado = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Dom"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: domingo,
//                       onChanged: (bool value) {
//                         setState(() {
//                           domingo = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }

// // ignore: must_be_immutable
// class _EditTextField extends StatefulWidget {
//   final String title;
//   final bool isNumber;
//   TextEditingController tituloController = new TextEditingController();

//   _EditTextField({Key key, this.title, this.isNumber = false, this.tituloController}) : super(key: key);
//   // EditTextField({ this.title = '', this.tituloController}) ;

//   @override
//   _EditTextFieldState createState() => _EditTextFieldState();
// }

// class _EditTextFieldState extends State<_EditTextField> {
//   bool stateEdit = false;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.symmetric(
//         vertical: 10
//       ),
//       child: Row(
//         children: [
//           IconButton(
//             icon: Icon(stateEdit?Icons.check:Icons.edit),
//             // padding: EdgeInsets.symmetric(vertical: 10,horizontal: 1),
//             onPressed: (){
//               print("object");
//               stateEdit = !stateEdit;
//               setState(() {

//               });
//             }) ,
//           Expanded(
//             child: TextFormField(
//               // enabled: stateEdit,
//               // maxLength: 20,
//               controller: this.widget.tituloController,
//               cursorColor: Colors.orange,
//               keyboardType: this.widget.isNumber? TextInputType.number: TextInputType.text,
//               decoration: InputDecoration(
//                 enabled: stateEdit,
//                   fillColor: Colors.black.withOpacity(0),
//                   filled: true,
//                   // prefixIcon: Icon(Icons.email, color: Colors.green.shade900),
//                   // suffixIcon: IconButton(icon: Icon(Icons.check), onPressed: (){
//                   //   print("object");
//                   //   stateEdit = !stateEdit;
//                   //   setState(() {

//                   //   });
//                   // }) ,
//                   // Icon(Icons.clear, color: Colors.green.shade100),
//                   focusedBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(20),
//                     borderSide: BorderSide(
//                         color: Colors.orange,
//                         style: BorderStyle.solid,
//                         width: 2),
//                   ),
//                   enabledBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(20),
//                     borderSide: BorderSide(
//                         color: Colors.orange,
//                         style: BorderStyle.solid,
//                         width: 2),
//                   ),
//                   labelText: this.widget.title,
//                   labelStyle: TextStyle(color: Colors.black)
//                 ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
// //https://maps.googleapis.com/maps/api/geocode/json?address=[YOUR%20ADDRESS]&sensor=true
