// import 'dart:io';

// import 'package:firebase_storage/firebase_storage.dart';
// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:path/path.dart';
// import 'package:restorant_app/Api/ApiConection.dart';
// import 'package:restorant_app/Widgets/EditDescription.dart';
// import 'package:restorant_app/Widgets/EditTextField.dart';
// import 'package:restorant_app/Widgets/OptionEdit.dart';
// import 'package:restorant_app/Widgets/PriceEdit.dart';
// import 'package:restorant_app/main.dart';
// class NewProduct extends StatefulWidget {
//   final String idRest;
//   final String name;

//   const NewProduct({this.idRest, this.name});
//   @override
//   _NewProductState createState() => _NewProductState();
// }


// class _NewProductState extends State<NewProduct> {
//   String respuesta = "false";

//   List<String> days=[];
//   TextEditingController tituloController = new TextEditingController();
//   TextEditingController priceController = new TextEditingController();
//   TextEditingController descriptionController = new TextEditingController();
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () => FocusScope.of(context).unfocus(),
//       child: SingleChildScrollView(
//         child: Container(
//           // color: Colors.orange.withOpacity(0.2),
//           child: Column(
//             children: [
//               _uploadImage(context),
//               _editText(tituloController,priceController),
//               EditDescription(descriptionController:descriptionController),
//               _options(),
//               _labelNumberPreparation(),
//               _labelDias(),
//               CupertinoButton(
//                 child: Text("FINALIZAR PRODUCTO"), 
//                 color: Colors.green,
//                 onPressed: (){
//                   // print("""
//                   // // == tituloController == ${tituloController.text}
//                   // // == priceController == ${priceController.text}
//                   // // == descriptionController == ${descriptionController.text}
//                   // // ========================================================
//                   // // """);
//                   // listaOptions.forEach((OptionEdit element) { 
//                   //   print(" # ${element.values} -- ${element.controller.text}");
//                   // });

//                   ////////////////////
//                   if(_imageFile!=null){ 
//                     print("entro para cargar foto");
//                     Flushbar(
//                       title: "Procesando...",
//                       message: "Espere unos segundos mientras termina el proceso",
//                       backgroundColor: Colors.orange,
//                       margin: EdgeInsets.all(8),
//                       borderRadius: 8,
//                       duration: Duration(seconds: 2),
//                       boxShadows: [BoxShadow(color: Colors.red[800], offset: Offset(0.0, 2.0), blurRadius: 3.0,)],
//                     )..show(context);

                    
//                     // cargando = CircularProgressIndicator();
//                     // String url = await uploadImageToFirebase(context,this.widget.name);
//                     // print("### URL = $url");
//                     Future(()async{
//                       String txt = await uploadImageToFirebase(context,this.widget.name);
//                       print("##FOTO## $txt");

//                       /////////////////
//                       ///
//                       if(lunes) days.add("\"Lunes\"");
//                       if(martes) days.add("\"Martes\"");
//                       if(miercoles) days.add("\"Miercoles\"");
//                       if(jueves) days.add("\"Jueves\"");
//                       if(viernes) days.add("\"Viernes\"");
//                       if(sabado) days.add("\"Sabado\"");
//                       if(domingo) days.add("\"Domingo\"");
                      
//                       print(days);
//                       respuesta = await api.postProduct(
//                         name: tituloController.text, 
//                         details: descriptionController.text, 
//                         description: descriptionController.text, 
//                         price: double.parse(priceController.text), 
//                         photo: photo, 
//                         preparationTime: preparationTime, 
//                         days: days,
//                         idRest: this.widget.idRest
//                       );
//                       print("RESPUESTA = $respuesta");
//                       days.clear();

//                       //////////
//                       ///[add options]
//                       ///
//                       listaOptions.forEach((OptionEdit element) { 
//                         print(" # ${element.values} -- ${element.controller.text}");
//                         api.postOptions(
//                           idProduct: respuesta, 
//                           titulo: element.controller.text, 
//                           limit: element.limit,// multiopcion, 
//                           opciones: element.values
//                         );
//                       });
//                       RestartWidget.restartApp(context);
//                       // setState(() {
                        
//                       // });
//                     });
//                   }
//                 }
//               )

//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   List<OptionEdit> listaOptions = [];
//   _options(){
//     return Container(
//       child: Column(
//         children: [
//           // ...listaOptions.map((element) {
//           //   return element;
//           // }).toList(),

//           ListView.builder(
//             primary: false,
//             shrinkWrap: true,
//             itemCount: listaOptions.length,
//             itemBuilder: (context, index) {
//               return Row(
//                 children: [
//                   Expanded(child: listaOptions[index]),
//                   IconButton(icon: Icon(Icons.close), onPressed: () {
//                     print(index);
//                     listaOptions.removeAt(index);
//                     setState(() {
                      
//                     });
//                   },)
//                 ],
//               );
//             },
//           ),

          
//           CupertinoButton(
//             child: Text("AGREGAR OPCIONES"), 
//             onPressed: (){
//               listaOptions.add(OptionEdit());
//               setState(() {
                
//               });
//             }
//           )
//         ],
//       ),
//     );
//   }

// ///[Upload Image]
//   File _imageFile;
//   String photo ;
//   ///NOTE: Only supported on Android & iOS
//   ///Needs image_picker plugin {https://pub.dev/packages/image_picker}
//   final picker = ImagePicker();

//   Future pickImage() async {
//     final pickedFile = await picker.getImage(source: ImageSource.camera,imageQuality: 15);

//     setState(() {
//       _imageFile = File(pickedFile.path);
//     });
//   }

//   Future pickGallery() async {
//     final pickedFile = await picker.getImage(source: ImageSource.gallery,imageQuality: 15);

//     setState(() {
//       _imageFile = File(pickedFile.path);
//     });
//   }

//   Future<String> uploadImageToFirebase(BuildContext context,String carpeta) async {
//     String fileName = basename(_imageFile.path);
//     Reference firebaseStorageRef =
//         FirebaseStorage.instance.ref().child('$carpeta/$fileName');
//     UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
//     TaskSnapshot taskSnapshot = await uploadTask;// onComplete;xxxxxxx 
//     return taskSnapshot.ref.getDownloadURL().then(
//           (value) {
//             print("Done: $value");
//             photo = value;
//             // setState(() { });
//             return value.toString();
//           } ,
//         );
//     // return photo;
//   }


//   // Widget cargando = Text("Presione aqui para subir");
//   _uploadImage(BuildContext context){
//     return Container(
//       width: double.infinity,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           Text("Suba una imagen de su producto"),
//           SizedBox(height: 5,),
//           Stack(
//             // mainAxisAlignment: MainAxisAlignment.center,
//             alignment: _imageFile != null?Alignment.bottomRight:Alignment.center,
//             children: [
//               Container(
//                 width: double.infinity,
//                 height: 200,
//                 child: _imageFile != null
//                     ? Image.file(_imageFile,fit: BoxFit.cover,)
//                     : Container(color: Colors.grey,),
//               ),
//               Row(
//                 mainAxisAlignment: _imageFile != null?MainAxisAlignment.end:MainAxisAlignment.center,
//                 children: [
//                   // ignore: deprecated_member_use
//                   RaisedButton(
//                     color: Colors.white54,
//                     padding: EdgeInsets.all(5),
//                     child: Icon(
//                       Icons.add_a_photo,
//                       size: 40,
//                     ),
//                     onPressed: pickImage,
//                   ),
//                   SizedBox(width: 15,),
//                   // ignore: deprecated_member_use
//                   RaisedButton(
//                     color: Colors.white54,
//                     padding: EdgeInsets.all(5),
//                     child: Icon(
//                       Icons.image_search,
//                       size: 40,
//                     ),
//                     onPressed: pickGallery,
//                   ),
//                 ],
//               ),
//             ],
//           ),

//           // Padding(
//           //   padding: const EdgeInsets.all(8.0),
//           //   child: CupertinoButton(
//           //     onPressed: () {
//           //       // print("  -------- ");
//           //       // print(_scrollController.position.maxScrollExtent);
//           //       if(_imageFile!=null){ 
//           //         print("entro para cargar foto");
//           //         cargando = CircularProgressIndicator();
//           //         uploadImageToFirebase(context,this.widget.name);
//           //       }
//           //       setState(() {
                  
//           //       });
                
//           //     },
//           //     color: photo!=null? Colors.green[300]:Colors.grey,
//           //     child:  photo!=null? Text("Imagen subida exitosamente"):cargando ,
//           //   ),
//           // )
//         ],
//       ),
//     );
//   }


//   /////////////////////////
//   ///[widget edit]
//   _editText(TextEditingController tituloController,TextEditingController priceController){
//     return Container(
//       // height: 70,
//       padding: EdgeInsets.all(15),
//       child: Row(
//         children: [
//           Expanded(
//             flex: 2,
//             child: EditTextField(color: Colors.orange ,label: "Nombre del producto",tituloController: tituloController,)
//           ),
//           Expanded(
//             flex: 1,
//             child: PriceEdit(priceController:priceController)
//           )
//         ],
//       ),
//     );
//   }

//   ////////////////////////////////////
  
//   bool lunes = true;
//   bool martes = true;
//   bool miercoles = true;
//   bool jueves = true;
//   bool viernes = true;
//   bool sabado = true;
//   bool domingo = true;
//   int preparationTime = 0;

//   // String error = null;
//   // final numericRegex = 
//   //   RegExp(r'^-?(([0-9]*)|(([0-9]*)\.([0-9]*)))$');
//   _labelNumberPreparation({ String text = "Tiempo de preparacion",  String ejemplo = "Ej. 25 minutos"}){
//     return Container(
//       padding: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
//       width: double.infinity,
//       child: Column(
//         children: [
//           Text(text),
//           Container(
//             width: double.infinity,
//             child: Row(
//               children: [
//                 Expanded(
//                   flex: 3,
//                   child: TextFormField(
//                     // validator: (value) {
//                     //   if (value.contains(",")) {
//                     //     print(" # entro = $value");
//                     //     return ('Por favor remplace la coma por un punto.');
//                     //   }
//                     //   return null;
//                     // },
//                     onChanged: (value) {

//                       preparationTime = int.parse(value);
                      
//                       // print("#### ${numericRegex.hasMatch(value)}");

//                       // if (numericRegex.hasMatch(value)) {
//                       //   print(" # entro = $value");
//                       //   error = 'Use punto.';
//                       //   // return ('Por favor remplace la coma por un punto.');
//                       // }else{
//                       //   print(" # no entro = $value");
//                       //   error = null;
//                       // }
//                         setState(() {  });
//                     },
//                     decoration: InputDecoration(
//                       border: OutlineInputBorder(),
//                       helperText: "Ej: $ejemplo",
//                       // errorText: error,
//                       focusedBorder: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(20),
//                         borderSide: BorderSide(
//                             color: Colors.orange,
//                             style: BorderStyle.solid,
//                             width: 2),
//                       ),
//                       enabledBorder: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(20),
//                         borderSide: BorderSide(
//                             color: Colors.orange,
//                             style: BorderStyle.solid,
//                             width: 2),
//                       ),

//                     ),

//                     keyboardType: TextInputType.number,
                    
//                     // onChanged: (value) {
//                     //   preparationTime = int.parse(value);
//                     //   setState(() {
                        
//                     //   });
//                     // },
//                   ),
//                 ),
//                 Expanded(
//                   flex: 2,
//                   child: Container(
//                     alignment: Alignment.center,
//                     child: Container(
//                       child: Text("Minutos.",style: TextStyle(fontSize: 25,color: Colors.grey),)
//                     ),
//                   )
//                 )
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   _labelDias(){
//     return Container(
//       padding: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
//       width: double.infinity,
//       child: Column(
//         children: [
//           Text("Dias disponible del producto:"),
//           SingleChildScrollView(
//             scrollDirection: Axis.horizontal,
//             child: Row(
//               children: [
//                  Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Lun"),
//                     Checkbox(
//                       value: lunes,
//                       activeColor: Colors.orange,
//                       onChanged: (bool value) {
//                         setState(() {
//                           lunes = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Tuesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Mar"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: martes,
//                       onChanged: (bool value) {
//                         setState(() {
//                           martes = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Wednesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Mie"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: miercoles,
//                       onChanged: (bool value) {
//                         setState(() {
//                           miercoles = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Jue"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: jueves,
//                       onChanged: (bool value) {
//                         setState(() {
//                           jueves = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Tuesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Vie"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: viernes,
//                       onChanged: (bool value) {
//                         setState(() {
//                           viernes = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 // [Wednesday] checkbox
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Sab"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: sabado,
//                       onChanged: (bool value) {
//                         setState(() {
//                           sabado = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text("Dom"),
//                     Checkbox(
//                       activeColor: Colors.orange,
//                       value: domingo,
//                       onChanged: (bool value) {
//                         setState(() {
//                           domingo = value;
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }

// }

