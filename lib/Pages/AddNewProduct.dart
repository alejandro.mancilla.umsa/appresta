import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:restorant_app/Api/FIrestoreApi.dart';
import 'package:path/path.dart';
import 'package:restorant_app/Providers/ProviderListPlatos.dart';
import 'package:restorant_app/Providers/ProviderRestaurant.dart';
import 'package:restorant_app/Widgets/EditDescription.dart';
import 'package:restorant_app/Widgets/EditTextField.dart';
import 'package:restorant_app/Widgets/OptionEdit.dart';
import 'package:restorant_app/Widgets/PriceEdit.dart';

class AddNewProduct extends StatefulWidget {
  final String idRest;
  final String name;

  const AddNewProduct({this.idRest, this.name});

  @override
  _AddNewProductState createState() => _AddNewProductState();
}

class _AddNewProductState extends State<AddNewProduct> {
  int numberOption = 0;
  String respuesta = "false";
  ProviderRestaurant providerRestaurant;
  List<String> days = [];
  List<String> daysFinal = [];
  TextEditingController tituloController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  ApiFirebase apiFirebase = ApiFirebase();

  ProviderListPlatos providerListPlatos;
  bool lunes;
  bool martes;
  bool miercoles;
  bool jueves;
  bool viernes;
  bool sabado;
  bool domingo;
  // int preparationTime = 0;
  TextEditingController preparationTime = new TextEditingController();

  // ignore: avoid_init_to_null
  String error = null;
  final numericRegex = RegExp(r'^-?(([0-9]*)|(([0-9]*)\.([0-9]*)))$');
  @override
  void initState() {
    providerListPlatos =
        Provider.of<ProviderListPlatos>(this.context, listen: false);
    providerRestaurant =
        Provider.of<ProviderRestaurant>(this.context, listen: false);
    lunes = false;
    martes = false;
    miercoles = false;
    jueves = false;
    viernes = false;
    sabado = false;
    domingo = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text(
            "Creando nuevo producto",
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.normal),
          ),
          backgroundColor: Colors.white,
        ),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  _uploadImage(context),
                  _editText(tituloController, priceController),
                  EditDescription(descriptionController: descriptionController),
                  _options(),
                  // _labelNumberPreparation(),
                  _labelDias(),
                  // Text("data $idResta"),
                  CupertinoButton(
                      child: Text("Crear"),
                      color: Colors.blue,
                      onPressed: () {
                        updateProduct(context);
                      })
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<OptionEdit> listaOptions = [];
  _options() {
    return Container(
      child: Column(
        children: [
          // ...listaOptions.map((element) {
          //   return element;
          // }).toList(),

          ListView.builder(
            primary: false,
            shrinkWrap: true,
            itemCount: listaOptions.length,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  Expanded(child: listaOptions[index]),
                  IconButton(
                    icon: Icon(
                      Icons.remove_circle_outline,
                      color: Colors.redAccent,
                    ),
                    onPressed: () {
                      print(index);
                      listaOptions.removeAt(index);
                      setState(() {});
                    },
                  )
                ],
              );
            },
          ),

          CupertinoButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("AGREGAR OPCIONES"),
                  SizedBox(
                    width: 5,
                  ),
                  Icon(Icons.add)
                ],
              ),
              onPressed: () {
                // numberOption = numberOption + 1;
                listaOptions.add(OptionEdit());
                setState(() {});
              })
        ],
      ),
    );
  }

  updateProduct(BuildContext context) {
    EasyLoading.show(
      status: 'Cargando...',
      dismissOnTap: false,
    );
    daysFinal = [];

    if (lunes) daysFinal.add("Lunes");
    if (martes) daysFinal.add("Martes");
    if (miercoles) daysFinal.add("Miercoles");
    if (jueves) daysFinal.add("Jueves");
    if (viernes) daysFinal.add("Viernes");
    if (sabado) daysFinal.add("Sabado");
    if (domingo) daysFinal.add("Domingo");
    CollectionReference plato =
        FirebaseFirestore.instance.collection('Products');
    List<Map> listaOptionMap = [];
    listaOptions.forEach((element) {
      List<Map> listaOptionMapItem = [];

      element.values.forEach((element) {
        OptionMapItem optionMapItem = OptionMapItem(name: element, price: 0);

        listaOptionMapItem.add(optionMapItem.toJson());
      });

      OptionMap optionMap = OptionMap(
          quantity: element.limit ? 0 : 1,
          title: element.controller.text,
          options: listaOptionMapItem);

      print("""
        ${element.controller.text} = ${element.limit} = ${element.values} =
      """);
      listaOptionMap.add(optionMap.toJson());
    });

    print("""
    $photo
    ${tituloController.text}
    ${priceController.text}
    ${descriptionController.text}
    ${preparationTime.text}
    $daysFinal
    $listaOptionMap
    """);

    plato.add({
      'city': 'El Alto', //TODO cambiar manualmente la ciudad
      'description': descriptionController.text,
      'idBusiness': this.widget.idRest,
      'name': tituloController.text,
      'nameBusiness': this.widget.name,
      'photo': photo,
      'price': priceController.text,
      'stock': true,
      'coordenates': null,
      'options': listaOptionMap,
      'days': daysFinal,
      'emergency': false
    }).then((value) {
      plato.doc(value.id).update({'idProduct': value.id}).then((value2) {
        // Flushbar(
        //   title: "Aceptado",
        //   message: "Plato agregado correctamente",
        //   backgroundColor: Colors.orange,
        //   margin: EdgeInsets.all(8),
        //   borderRadius: 8,
        //   duration: Duration(seconds: 2),
        //   boxShadows: [
        //     BoxShadow(
        //       color: Colors.red[800],
        //       offset: Offset(0.0, 2.0),
        //       blurRadius: 3.0,
        //     )
        //   ],
        // ).show(context);

        // Future.delayed(Duration(milliseconds: 2100), () async {
        //   // List<BusinessModel> listaBusinnes = [];
        //   // listaBusinnes = await apiFirebase.readRestaurants();
        //   // providerRestaurant.listaBusiness = listaBusinnes;
        //   apiFirebase.readProducts(this.widget.idRest, providerListPlatos);
        //   Navigator.of(context).pop();
        //   Navigator.of(context).pop();
        // });
      });

      EasyLoading.showSuccess('Producto creado con exito!');
      Navigator.of(context).pop();
    }).catchError((error) {
      EasyLoading.showError(
          'Fallo la creacion del producto, revisa tu conexión a internet');
    }).timeout(Duration(seconds: 8), onTimeout: () {
      EasyLoading.showError(
          'Fallo la creacion del producto, revisa tu conexión a internet');
    });
  }

  ///[Upload Image]
  File _imageFile;
  String photo;

  ///NOTE: Only supported on Android & iOS
  ///Needs image_picker plugin {https://pub.dev/packages/image_picker}
  final picker = ImagePicker();

  Future pickImage(BuildContext context) async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 15);
    _actionsPickImage(pickedFile, context);
  }

  Future pickGallery(BuildContext context) async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 15);
    _actionsPickImage(pickedFile, context);
  }

  _actionsPickImage(PickedFile pickedFile, BuildContext context) {
    setState(() {
      try {
        _imageFile = File(pickedFile.path);
        uploadImageToFirebase(context, this.widget.name);
      } catch (e) {}
    });
  }

  Future<String> uploadImageToFirebase(
      BuildContext context, String carpeta) async {
    String fileName = basename(_imageFile.path);
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('$carpeta/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
    TaskSnapshot taskSnapshot = await uploadTask;
    print("################# Done: xx");

    return taskSnapshot.ref.getDownloadURL().then(
      (value) {
        print("################# Done: $value");
        photo = value;
        // setState(() { });
        return value.toString();
      },
    );
    // return photo;
  }

  // Widget cargando = Text("Presione aqui para subir");
  _uploadImage(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Text("Suba una imagen de su producto"),
          // SizedBox(
          //   height: 5,
          // ),
          Stack(
            // mainAxisAlignment: MainAxisAlignment.center,
            alignment: photo != null ? Alignment.bottomRight : Alignment.center,
            children: [
              Container(
                width: double.infinity,
                height: 200,
                child: _imageFile != null
                    ? Image.file(
                        _imageFile,
                        fit: BoxFit.cover,
                      )
                    : Container(
                        color: Colors.grey.shade400,
                        // child: Image.network(photo,fit: BoxFit.cover,),
                        child: Container(),
                      ),
              ),
              Row(
                mainAxisAlignment: photo != null
                    ? MainAxisAlignment.end
                    : MainAxisAlignment.center,
                children: [
                  // ignore: deprecated_member_use
                  RaisedButton(
                    color: Colors.white54,
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.add_a_photo,
                      size: 40,
                    ),
                    onPressed: () => pickImage(context),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  // ignore: deprecated_member_use
                  RaisedButton(
                    color: Colors.white54,
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.image_search,
                      size: 40,
                    ),
                    onPressed: () => pickGallery(context),
                  ),
                ],
              ),
            ],
          ),

          // Padding(
          //   padding: const EdgeInsets.all(8.0),
          //   child: CupertinoButton(
          //     onPressed: () {
          //       // print("  -------- ");
          //       // print(_scrollController.position.maxScrollExtent);
          //       if(_imageFile!=null){
          //         print("entro para cargar foto");
          //         cargando = CircularProgressIndicator();
          //         uploadImageToFirebase(context,this.widget.name);
          //       }
          //       setState(() {

          //       });

          //     },
          //     color: photo!=null? Colors.green[300]:Colors.grey,
          //     child:  photo!=null? Text("Imagen subida exitosamente"):cargando ,
          //   ),
          // )
        ],
      ),
    );
  }

  /////////////////////////
  ///[widget edit]
  _editText(TextEditingController tituloController,
      TextEditingController priceController) {
    return Container(
      // height: 70,
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: EditTextField(
                showIcon: false,
                color: Colors.grey.shade200,
                label: "Nombre del producto",
                tituloController: tituloController,
              )),
          Expanded(flex: 1, child: PriceEdit(priceController: priceController))
        ],
      ),
    );
  }

  ////////////////////////////////////

  _labelNumberPreparation(
      {String text = "Tiempo de preparacion",
      String ejemplo = "Ej. 25 minutos"}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      width: double.infinity,
      child: Column(
        children: [
          Text(text),
          Container(
            width: double.infinity,
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: TextFormField(
                    // validator: (value) {
                    //   if (value.contains(",")) {
                    //     print(" # entro = $value");
                    //     return ('Por favor remplace la coma por un punto.');
                    //   }
                    //   return null;
                    // },
                    onChanged: (value) {
                      // preparationTime = int.parse(value);

                      // print("#### ${numericRegex.hasMatch(value)}");

                      if (numericRegex.hasMatch(preparationTime.text)) {
                        print(" # entro = $value");
                        error = null;
                        // return ('Por favor remplace la coma por un punto.');
                      } else {
                        print(" # no entro = $value");
                        error = 'Solo ingrese números.';
                      }
                      setState(() {});
                    },
                    controller: preparationTime,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      helperText: "Ej: $ejemplo",
                      errorText: error,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide(
                            color: Colors.orange,
                            style: BorderStyle.solid,
                            width: 2),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide(
                            color: Colors.orange,
                            style: BorderStyle.solid,
                            width: 2),
                      ),
                    ),

                    keyboardType: TextInputType.number,

                    // onChanged: (value) {
                    //   preparationTime = int.parse(value);
                    //   setState(() {

                    //   });
                    // },
                  ),
                ),
                Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.center,
                      child: Container(
                          child: Text(
                        "Minutos.",
                        style: TextStyle(fontSize: 25, color: Colors.grey),
                      )),
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }

  _labelDias() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
      width: double.infinity,
      child: Column(
        children: [
          // Text("Dias disponible del producto:"),
          Padding(
            padding: const EdgeInsets.only(bottom: 25, top: 15),
            child: Text(
              "Dias disponible del producto",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Lun"),
                    Checkbox(
                      value: lunes,
                      activeColor: Colors.orange,
                      onChanged: (bool value) {
                        setState(() {
                          lunes = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Tuesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Mar"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: martes,
                      onChanged: (bool value) {
                        setState(() {
                          martes = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Wednesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Mie"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: miercoles,
                      onChanged: (bool value) {
                        setState(() {
                          miercoles = value;
                        });
                      },
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Jue"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: jueves,
                      onChanged: (bool value) {
                        setState(() {
                          jueves = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Tuesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Vie"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: viernes,
                      onChanged: (bool value) {
                        setState(() {
                          viernes = value;
                        });
                      },
                    ),
                  ],
                ),
                // [Wednesday] checkbox
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Sab"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: sabado,
                      onChanged: (bool value) {
                        setState(() {
                          sabado = value;
                        });
                      },
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Dom"),
                    Checkbox(
                      activeColor: Colors.orange,
                      value: domingo,
                      onChanged: (bool value) {
                        setState(() {
                          domingo = value;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class OptionMapItem {
  String name;
  double price;

  OptionMapItem({this.name, this.price});
  Map toJson() {
    return {"name": this.name, "price": this.price};
  }
}

class OptionMap {
  List<Map> options;
  int quantity;
  String title;

  OptionMap({this.options, this.quantity, this.title});

  Map toJson() {
    return {
      "options": this.options,
      "quantity": this.quantity,
      "title": this.title
    };
  }
}
