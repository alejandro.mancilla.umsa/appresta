// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:restorant_app/Api/ApiConection.dart';
// import 'package:restorant_app/Api/Models/ProductModel.dart';
// import 'package:restorant_app/main.dart';



// class AddOptions extends StatefulWidget {
//   final String idPlato;

//   const AddOptions({ this.idPlato}) ;

//   @override
//   _AddOptionsState createState() => _AddOptionsState();
// }

// class _AddOptionsState extends State<AddOptions> {
//   final ApiConection api = new ApiConection();
  
//   ProductModel modelo ;
//   @override
//   void initState() { 
//     super.initState();
//   }

//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     _cargadoDatos();
    
//   }
//   _cargadoDatos() async {
//     modelo = await api.getProduct(this.widget.idPlato);
//     setState(() {
      
//     optionsActualizado.add(_grupoEjemplo("Ejemplo", ["item 1","item 2","mayonesa","llahua"]));
//     });
//   }
//   @override
//   Widget build(BuildContext context) {
//     return modelo != null ? _parteSuperior(context): CircularProgressIndicator();
//   }

//   TextEditingController _controllerOptionsTitulo = new TextEditingController();
//   TextEditingController _controllerOptions = new TextEditingController();
//   bool multiopcion = false;
//   _parteSuperior(BuildContext context){
//     TextStyle estiloTitulo = TextStyle(fontWeight: FontWeight.bold,fontSize: 20);
//     TextStyle estiloSubtitulo = TextStyle(fontWeight: FontWeight.normal,fontSize: 20);
//     return SingleChildScrollView(
//       child: Container(
//         width: double.infinity,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             FadeInImage(
//               placeholder: AssetImage("assets/images/tenor.gif"), 
//               image: NetworkImage(modelo.photo),
//               width: double.infinity,
//               height: 200,
//               fit: BoxFit.cover,
//             ),
//             SizedBox(height: 10,),
//             Row(
//               children: [
//                 Expanded(
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Padding(
//                         padding: const EdgeInsets.symmetric(horizontal: 15),
//                         child: Text(modelo.name,style: estiloTitulo,),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.symmetric(horizontal: 15),
//                         child: Text(modelo.description,style: estiloSubtitulo),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Container(
//                   padding: EdgeInsets.all(8),
//                   margin: EdgeInsets.only(right: 30),
//                   decoration: BoxDecoration(
//                     color: Colors.orange,
//                     borderRadius: BorderRadius.circular(15)
//                   ),
//                   child: Padding(
//                     padding:EdgeInsets.all(8),
//                     child: Text("${modelo.price.toString()} Bs.", style: TextStyle(color: Colors.white),), 
//                   ),
//                 )
//               ],
//             ),

//             SizedBox(height: 20,),
//             ...optionsActualizado,
            
//             _labelText(
//               text: "Titulo", 
//               controller: _controllerOptionsTitulo, 
//               ejemplo: "Aderesos"
//             ),
//             _labelText(
//               text: "Opciones", 
//               controller: _controllerOptions, 
//               ejemplo: "Mayonesa, llahua, (separado por coma)"
//             ),
//             Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     SizedBox(width: 20,),
//                     Expanded(child: Text("Marque la siguiente casilla si los clientes podran seleccionar mas de una opcion")),
//                     Container(
//                       margin: EdgeInsets.symmetric(horizontal: 20),
//                       child: Checkbox(
//                         value: multiopcion,
//                         onChanged: (bool value) {
//                           setState(() {
//                             multiopcion = value;
//                           });
//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//             Container(
//               width: double.infinity,
//               padding: EdgeInsets.symmetric(horizontal: 25,vertical: 10),
//               child: CupertinoButton(
//                 color: Colors.orange,
//                 child: Text("Agregar Opciones"), 
//                 onPressed: (){
//                   print("""
                  
//                   ${_controllerOptionsTitulo.text}
//                   ${_controllerOptions.text}
//                   $multiopcion
                  
//                   """);
//                   optionsActualizado.add(_grupoEjemplo(_controllerOptionsTitulo.text,_controllerOptions.text.split(",")));
//                   api.postOptions(
//                     idProduct: this.widget.idPlato, 
//                     titulo: _controllerOptionsTitulo.text, 
//                     limit: multiopcion, 
//                     opciones: _controllerOptions.text.split(",")
//                   );
//                   _controllerOptionsTitulo.clear();
//                   _controllerOptions.clear();
//                   setState(() {
                    
//                   });
//                 }
//               ),
//             ),


//             Container(
//               width: double.infinity,
//               padding: EdgeInsets.symmetric(horizontal: 25,vertical: 10),
//               child: CupertinoButton(
//                 color: Colors.green,
//                 child: Text("Finalizar Nuevo Producto"), 
//                 onPressed: (){
//                   // ProviderListPlatos _listPlatos = Provider.of<ProviderListPlatos>(context,listen: false); 
//                   // _listPlatos.indexPage = 1;

//                   Flushbar(
//                     title: "Aceptado",
//                     message: "Plato agregado correctamente",
//                     backgroundColor: Colors.orange,
//                     margin: EdgeInsets.all(8),
//                     borderRadius: 8,
//                     duration: Duration(seconds: 2),
//                     boxShadows: [BoxShadow(color: Colors.red[800], offset: Offset(0.0, 2.0), blurRadius: 3.0,)],
//                   )..show(context);

//                   Future.delayed(Duration(milliseconds: 2001),(){
//                     RestartWidget.restartApp(context);
//                   });
//                 }
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
//   List<Widget> optionsActualizado = [];
  

//   _grupoEjemplo(String titulo, List<String> items){
//     TextStyle estiloTitulo = TextStyle(fontWeight: FontWeight.bold,fontSize: 20);
//     List<Widget> list = [];
//     items.forEach((e) {
//       print(e);
//       list.add(_item(e)) ;
//     });
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 15),
//             child: Text(titulo,style: estiloTitulo,),
//           ),
//           SizedBox(height: 7,),
//           Wrap(
//             direction: Axis.horizontal,
//             children: list
//             // [
//             //   _item("item 1"),
//             //   _item("item 2"),
//             //   _item("mayonesa"),
//             //   _item("llahua"),
//             // ],
//           )
//       ],
//     );
//   }

//   _item(String texto){
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: 8),
//       padding: EdgeInsets.all(8),
//       child: Text(texto),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(20),
//         color: Colors.grey[100],
//         boxShadow: [
//           BoxShadow(offset: Offset(1,0),color: Colors.grey),
//           BoxShadow(offset: Offset(0,1),color: Colors.grey),
//           BoxShadow(offset: Offset(-1,0),color: Colors.grey),
//           BoxShadow(offset: Offset(0,-1),color: Colors.grey),
//         ]
//       ),
//     );
//   }


//   _labelText({@required String text, @required TextEditingController controller, @required String ejemplo}){
//     return Container(
//       padding: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
//       width: double.infinity,
//       child: Column(
//         children: [
//           Text(text),
//           Container(
//             width: double.infinity,
//             child: TextFormField(
//               decoration: InputDecoration(
//                 border: OutlineInputBorder(),
//                 helperText: "Ej: $ejemplo",
//                 // errorText: (
//                 //   controller.text.length >0 
//                 // )?"Error":null,
//               ),
//               controller: controller,
//               keyboardType: TextInputType.text,
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }