import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:restorant_app/Core/styleMapa.dart';

// ignore: must_be_immutable
class ChangeMapPosition extends StatefulWidget {
  TextEditingController controllerTextLatLong;
  TextEditingController controllerTextDirection;
  TextEditingController controllerTextCiudad;
  CameraPosition positionMap;
  // final Completer<GoogleMapController> controller;

  ChangeMapPosition(
      {Key key,
      // this.controller,
      this.controllerTextLatLong,
      this.controllerTextDirection,
      this.controllerTextCiudad,
      this.positionMap})
      : super(key: key);

  @override
  _ChangeMapPositionState createState() => _ChangeMapPositionState();
}

class _ChangeMapPositionState extends State<ChangeMapPosition> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black26,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 25, vertical: 45),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                color: Colors.red,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    GoogleMap(
                      mapType: MapType.normal,
                      initialCameraPosition: this.widget.positionMap,
                      rotateGesturesEnabled: false,
                      myLocationButtonEnabled: true,
                      myLocationEnabled: true,
                      zoomControlsEnabled: true,
                      scrollGesturesEnabled: true,
                      zoomGesturesEnabled: true,

                      onMapCreated: (GoogleMapController controller) {
                        // this.widget.controller.complete(controller);
                        // _controllerGoogleMaps = controller;
                        controller.setMapStyle(dataMapStyle);
                        //await controller.animateCamera(CameraUpdate.newCameraPosition(position));
                      },

                      onCameraIdle: () {
                        print('hhhhhhhhh ${this.widget.positionMap.target}');
                        _obtenerDireccion(
                          this.widget.positionMap.target.latitude,
                          this.widget.positionMap.target.longitude,
                        );
                        setState(() {});
                        // print('========= $banderaEdit ');

                        // _obtenerDireccion(
                        //     positionMap.target.latitude, positionMap.target.longitude);
                        // if (!banderaEdit) {
                        //   banderaEdit = true;
                        // }
                      },
                      onCameraMove: (value) {
                        // print("###### $value");
                        this.widget.positionMap = value;
                      },
                      // onCameraMoveStarted: () {
                      //   // print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                      // },
                      // onMapCreated: (GoogleMapController controller) {
                      //   _controller.complete(controller);
                      //   // _controllerGoogleMaps = controller;
                      //   controller.setMapStyle(dataMapStyle);
                      // },

                      // scrollGesturesEnabled: statusMap,
                      // indoorViewEnabled: true,
                      // markers: _markers,
                      // initialCameraPosition: kGooglePlex,
                      // onMapCreated: (GoogleMapController controller) {
                      //   _controller.complete(controller);
                      //   mapController = controller;

                      // },
                      gestureRecognizers:
                          <Factory<OneSequenceGestureRecognizer>>{}
                            ..add(Factory<PanGestureRecognizer>(
                                () => PanGestureRecognizer()))
                            ..add(Factory<VerticalDragGestureRecognizer>(
                                () => VerticalDragGestureRecognizer()))
                            ..add(Factory<HorizontalDragGestureRecognizer>(
                                () => HorizontalDragGestureRecognizer()))
                            ..add(
                              Factory<ScaleGestureRecognizer>(
                                  () => ScaleGestureRecognizer()),
                            ),
                    ),
                    Image.asset(
                      'assets/icon/marker.png',
                      width: 40,
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Direccion:',
                    style: TextStyle(color: Colors.grey, fontSize: 10),
                  ),
                  Text('${this.widget.controllerTextDirection.text}'),
                  Text(
                    'Ciudad',
                    style: TextStyle(color: Colors.grey, fontSize: 10),
                  ),
                  Text('${this.widget.controllerTextCiudad.text}'),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CupertinoButton(
                  child: Text(
                    'Cancelar',
                    style: TextStyle(color: Colors.red),
                  ),
                  onPressed: () {
                    this.widget.controllerTextLatLong.text = '';
                    this.widget.controllerTextDirection.text = '';
                    this.widget.controllerTextCiudad.text = '';
                    Navigator.pop(context);
                  },
                ),
                CupertinoButton(
                    child: Text('Confirmar'),
                    onPressed: () {
                      // this.widget.controller = ;
                      this.widget.controllerTextLatLong.text =
                          '${this.widget.positionMap.target.latitude},${this.widget.positionMap.target.longitude}';
                      // this.widget.controllerTextDirection.text = 'RESPUESTA';
                      // this.widget.controllerTextCiudad.text = 'RESPUESTA';
                      Navigator.pop(context);
                    }),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _obtenerDireccion(double lat, double long) async {
    List<Placemark> placemarks = await placemarkFromCoordinates(lat, long);

    this.widget.controllerTextCiudad.text = placemarks[0].locality;
    this.widget.controllerTextDirection.text = placemarks[0].street;
    if (this.widget.controllerTextDirection.text.contains('Unnamed')) {
      this.widget.controllerTextDirection.text = '';
    }
  }
}
