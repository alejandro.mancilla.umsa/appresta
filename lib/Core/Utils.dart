import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instancia = UserPreferences._internal();

  factory UserPreferences() {
    return _instancia;
  }

  UserPreferences._internal();

  SharedPreferences _prefs;

  initPreferences() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  String get userTokenNotification =>
      _prefs.getString('pref_TokenNotification');
  set userTokenNotification(String data) {
    _prefs.setString('pref_TokenNotification', data);
  }

  String get userGoogleId => _prefs.getString('pref_userGoogleId');
  set userGoogleId(String data) {
    _prefs.setString('pref_userGoogleId', data);
  }

  String get userName => _prefs.getString('pref_userName');
  set userName(String data) {
    _prefs.setString('pref_userName', data);
  }

  String get userPhone => _prefs.getString('pref_userPhone');
  set userPhone(String data) {
    _prefs.setString('pref_userPhone', data);
  }

  void setAcceptLocation() {
    _prefs.setBool('accept_location', true);
  }

  bool get acceptLocation {
    return _prefs.getBool('accept_location') ?? false;
  }
}
