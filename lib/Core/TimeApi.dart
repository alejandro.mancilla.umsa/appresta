import 'package:ntp/ntp.dart';

class TimeApi {
  Future<DateTime> getTime()async{
    return _checkTime('pool.ntp.org');
  }

  // getTimeGoogle(){
  //   Future<int> getNtpOffset({
  //     String lookUpAddress: 'time.google.com',
  //     int port: 123,
  //     DateTime localTime,
  //     Duration timeout,
  //     DnsApiProvider dnsProvider = DnsApiProvider.google,
  //   });
  // }

  Future<DateTime> _checkTime(String lookupAddress) async {
    DateTime _myTime;
    DateTime _ntpTime;

    /// Or you could get NTP current (It will call DateTime.now() and add NTP offset to it)
    _myTime = DateTime.now();

    /// Or get NTP offset (in milliseconds) and add it yourself
    int offset = 0 ;
    
    try {
      offset = await NTP.getNtpOffset(localTime: _myTime, lookUpAddress: lookupAddress);
      _ntpTime = _myTime.add(Duration(milliseconds: offset));

    } catch (e) {
      print("###########error $e");
    }

    // print('\n==== $lookupAddress ====');
    // print('My time: $_myTime');
    // print('NTP time: $_ntpTime');
    // print('Difference: ${_myTime.difference(_ntpTime).inMilliseconds}ms');
    return _ntpTime ?? _myTime;
  } 

  getDias(String day){
    switch (day) {
      case "Monday":
        return "Lunes";
        break;
      case "Tuesday":
        return "Martes";
        break;
      case "Wednesday":
        return "Miercoles";
        break;
      case "Thursday":
        return "Jueves";
        break;
      case "Friday":
        return "Viernes";
        break;
      case "Saturday":
        return "Sabado";
        break;
      case "Sunday":
        return "Domingo";
        break;
      default:
    }
  }

}
