class BusinessModel {
  double businessPercentage;
  String category;
  String city;
  List days = [];
  String description;
  String direction;
  String email;

  bool emergency;

  List googleId = [];
  String idBusiness;
  String name;
  String nameOwner;
  String numberOwner;
  String photo;
  int preparationTime;
  List schedule = [];
  bool showingInApp;
  String slogan;
  List tokenNotificationBusiness = [];
  List type = [];
  String ubication;
  String zone;

  String scheduleNow = '';

  bool statusOpenLocal = false;

  String statusBusinessOpen = 'Automatic';
  // String token;
  // // double deliveryTime;
  // bool status = false;

  // BusinessModel({
  //   businessPercentage = '';
  //   category = '';
  //   city = '';
  //   days = '';
  //   description = '';
  //   direction = '';
  //   email = '';
  //   emergency = '';
  //   googleId = '';
  //   idBusiness = '';
  //   name = '';
  //   nameOwner = '';
  //   numberOwner = '';
  //   photo = '';
  //   preparationTime = '';
  //   schedule = '';
  //   showingInApp = '';
  //   slogan = '';
  //   tokenNotificationBusiness = '';
  //   type = '';
  //   ubication = '';
  //   zone = '';
  // });

  BusinessModel.fromJson(Map json) {
    businessPercentage = json['businessPercentage'] != null
        ? json['businessPercentage'] * 1.0
        : 0.0;
    category = json['category'];
    city = json['city'];
    days = json['days'];
    description = json['description'];
    direction = json['direction'];
    email = json['email'];
    emergency = json['emergency'] ?? false;
    googleId = json['googleId'];
    idBusiness = json['idBusiness'];
    name = json['name'];
    nameOwner = json['nameOwner'];
    numberOwner = json['numberOwner'];
    photo = json['photo'];
    preparationTime =
        json['preparationTime'] != null ? json['preparationTime'] : 0;
    schedule = json['schedule'];
    showingInApp = json['showingInApp'] ?? false;
    slogan = json['slogan'];
    tokenNotificationBusiness = json['tokenNotificationBusiness'];
    type = json['type'];
    ubication = json['ubication'];
    zone = json['zone'] != null ? json['zone'] : 'Sin Zona';
    statusBusinessOpen = json['statusBusinessOpen'] != null
        ? json['statusBusinessOpen']
        : 'Automatic';

    if (statusBusinessOpen == 'AlwaysOpen') {
      statusOpenLocal = true;
    }
    if (statusBusinessOpen == 'AlwaysCloses') {
      statusOpenLocal = false;
    }
  }
}
