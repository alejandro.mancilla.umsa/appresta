class ProductModelUnit {
  String city;
  String coordenates;
  List days = [];
  String description;
  bool emergency = false;
  String idBusiness;
  String idProduct;
  String name;
  String nameBusiness;
  double rangePriority;

  List<Options> options = [];

  String photo;
  double price;
  bool stock;

  ProductModelUnit.createByJson(Map json) {
    List<Options> listaOrder = [];
    List lista = json["options"];

    lista?.forEach((element) {
      try {
        listaOrder.add(Options.createByJson(element));
      } catch (e) {}
    });

    this.city = json["city"];
    this.description = json["description"];
    this.idBusiness = json["idBusiness"];
    this.idProduct = json["idProduct"];
    this.name = json["name"];
    this.nameBusiness = json["nameBusiness"];
    this.photo = json["photo"];
    this.price = double.parse("${json["price"]}");
    this.stock = json["stock"];
    this.coordenates = json["coordenates"] ?? '';
    this.options = listaOrder;
    this.days = json["days"];
    this.emergency = json["emergency"] ?? false;
    this.rangePriority =
        json["rangePriority"] != null ? json["rangePriority"] * 1.0 : 1.0;
  }
  // ProductModelUnit(
  //     {this.city = '',
  //     this.days,
  //     this.description = '',
  //     this.idBusiness = '',
  //     this.idProduct = '',
  //     this.name = '',
  //     this.nameBusiness = '',
  //     this.options,
  //     this.photo = '',
  //     this.price = 0,
  //     this.stock = false,
  //     this.coordenates = "",
  //     this.emergency = false});

  // toJson() {
  //   List<Map> listaMapa = [];
  //   this.options.forEach((element) {
  //     listaMapa.add(element.toJson());
  //   });
  //   return {
  //     'city': this.city,
  //     'days': this.days,
  //     'description': this.description,
  //     'idBusiness': this.idBusiness,
  //     'idProduct': this.idProduct,
  //     'name': this.name,
  //     'nameBusiness': this.nameBusiness,
  //     'options': listaMapa,
  //     'photo': this.photo,
  //     'price': this.price,
  //     'stock': this.stock,
  //     'coordenates': this.coordenates,
  //     'emergency': this.emergency
  //   };
  // }

  // @override
  // String toString() {
  //   return "{'city':${this.city},'days':${this.days},'description':${this.description},'idBusiness':${this.idBusiness},'idProduct':${this.idProduct},'name':${this.name},'nameBusiness':${this.nameBusiness},'options':${this.options},'photo':${this.photo},'price':${this.price},'stock':${this.stock}, 'coordenates' : ${this.coordenates}, 'emergency' : ${this.emergency}}";
  // }
}

class Options {
  int quantity;
  String title;
  List<OptionsItems> options;

  Options({this.options, this.quantity, this.title});

  Options.createByJson(Map json) {
    List<OptionsItems> listaOrder = [];
    List lista = json["options"];

    lista.forEach((element) {
      listaOrder.add(OptionsItems.createByJson(element));
    });

    this.quantity = json["quantity"];
    this.title = json["title"];
    this.options = listaOrder;
  }

  // toJson() {
  //   List<Map> listaMapa = [];

  //   this.options.forEach((element) {
  //     listaMapa.add(element.toJson());
  //   });
  //   return {
  //     'optionsItems': listaMapa,
  //     'quantity': this.quantity,
  //     'title': this.title
  //   };
  // }

  // @override
  // String toString() {
  //   return "{'options':${this.options},'quantity':${this.quantity},'title':${this.title}}";
  // }
}

class OptionsItems {
  String name;
  double price;

  OptionsItems({this.name, this.price});

  OptionsItems.createByJson(Map json) {
    this.name = json["name"];
    this.price = json["price"];
  }

  toJson() {
    return {'name': this.name, 'price': this.price};
  }

  @override
  String toString() {
    return "{'name':${this.name},'price':${this.price}}";
  }
}
