// import 'package:restorant_app/Core/Models/ProductModel.dart';

// class OrderModel {
//   String nameBusiness;
//   // String tokenNegocio;
//   String idBusiness;
//   String percentageBusiness;
//   List<PedidoDetails> listOrderDetails;

//   OrderModel({
//     this.idBusiness,
//     this.listOrderDetails,
//     this.nameBusiness,
//     this.percentageBusiness
//   });

//   Map<String, Object> toJson(){
//     return {
//     'idBusiness' : idBusiness,
//     'listOrderDetails' : listOrderDetails,
//     'nameBusiness' : nameBusiness,
//     'percentageBusiness' : percentageBusiness
//     };
//   }

//   @override
//   String toString() {
//     return "{'idBusiness':$idBusiness,'nameBusiness':$nameBusiness,'listOrderDetails':$listOrderDetails,'percentageBusiness':$percentageBusiness}";
//   }
// }

// class PedidoDetails{
//   int amount;
//   double price;
//   ProductModel product;

//   PedidoDetails({
//     this.amount,
//     this.price,
//     this.product
//   });

//   Map<String, dynamic> toJson(){
//     return {
//       'amount' : amount,
//       'price' : price,
//       'product' : product.toJson()
//     };
//   }

//   PedidoDetails.createByJson(Map json){
//     amount = json['amount'];
//     price = json['price'];
//     product = ProductModel.createByJson(json['product']);
//   }

//   @override
//   String toString() {

//     return "{'amount':$amount,'price':$price,'product':$product}";
//   }
// }
