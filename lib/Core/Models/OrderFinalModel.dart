import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:restorant_app/Core/Models/ProductModel.dart';
// import 'package:restorant_app/Core/Models/ProductModel.dart';

class OrderFinalModel {
  int codOrder;
  Timestamp date;
  String direction;
  String idOrder;
  String nameRider;
  String nameUser;
  String nitUser;

  List<Orden> orden;

  String phoneClient;
  String riderTokenNotification;
  double shippingCost;
  String state;
  List tokenBusiness;
  List tokenNotificationBusiness;
  String tokenRider;
  String tokenUser;
  double totalCost;
  String ubication;
  String userTokenNotification;

  // OrderFinalModel(
  //     {date,
  //     direction,
  //     idOrder,
  //     nameUser,
  //     shippingCost,
  //     totalCost,
  //     tokenUser,
  //     tokenRider,
  //     ubication,
  //     orden,
  //     codOrder,
  //     state});

  // Map<String, Object> toJson() {
  //   var listaMap = <Orden>[];
  //   orden.forEach((element) {
  //     listaMap.add(element.toJson());
  //   });

  //   return {
  //     'date': date,
  //     'direction': direction,
  //     'idOrder': idOrder,
  //     'nameUser': nameUser,
  //     'shippingCost': shippingCost,
  //     'totalCost': totalCost,
  //     'tokenUser': tokenUser,
  //     'tokenRider': tokenRider,
  //     'ubication': ubication,
  //     'state': state,
  //     'orden': listaMap,
  //     'codOrder': codOrder
  //   };
  // }

  OrderFinalModel.createByJson(Map json) {
    List lista = json['orden'] != null ? json['orden'] : [];
    var listaOrden = <Orden>[];
    lista.forEach((element) {
      listaOrden.add(Orden.createByJson(element));
    });

    // date = json['date'];
    // direction = json['direction'];
    // idOrder = json['idOrder'];
    // nameUser = json['nameUser'];
    // shippingCost = json['shippingCost'];
    // totalCost = json['totalCost'];
    // tokenUser = json['tokenUser'];
    // tokenRider = json['tokenRider'];
    // ubication = json['ubication'];
    // state = json['state'];
    // codOrder = json['codOrder'].toString();
    // orden = listaOrden;

    orden = json['orden'] != null ? listaOrden : [];
    tokenBusiness = json['tokenBusiness'] != null
        ? json['tokenBusiness'].map((e) => '$e').toList()
        : [];
    tokenNotificationBusiness = json['tokenNotificationBusiness'] != null
        ? json['tokenNotificationBusiness'].map((e) => '$e').toList()
        : [];

    codOrder = json['codOrder'] != null ? json['codOrder'] : 0;
    date = json['date'] != null ? json['date'] : DateTime.now();
    direction = json['direction'] != null ? json['direction'] : 'Sin Datos';
    idOrder = json['idOrder'] != null ? json['idOrder'] : 'Sin Datos';
    nameRider = json['nameRider'] != null ? json['nameRider'] : 'Sin Datos';
    nameUser = json['nameUser'] != null ? json['nameUser'] : 'Sin Datos';
    nitUser = json['nitUser'] != null ? json['nitUser'] : 'Sin Datos';
    phoneClient =
        json['phoneClient'] != null ? json['phoneClient'] : 'Sin Datos';
    riderTokenNotification = json['riderTokenNotification'] != null
        ? json['riderTokenNotification']
        : 'Sin Datos';
    shippingCost =
        json['shippingCost'] != null ? json['shippingCost'] * 1.0 : 0.0;
    state = json['state'] != null ? json['state'] : 'Sin Datos';
    tokenRider = json['tokenRider'] != null ? json['tokenRider'] : 'Sin Datos';
    tokenUser = json['tokenUser'] != null ? json['tokenUser'] : 'Sin Datos';
    totalCost = json['totalCost'] != null ? json['totalCost'] * 1.0 : 0.0;
    ubication = json['ubication'] != null ? json['ubication'] : 'Sin Datos';
    userTokenNotification = json['userTokenNotification'] != null
        ? json['userTokenNotification']
        : 'Sin Datos';
  }

  // @override
  // String toString() {
  //   return '''
  //   {
  //       'date':$date,
  //       'direction':$direction,
  //       'idOrder':$idOrder,
  //       'nameUser':$nameUser,
  //       'shippingCost':$shippingCost,
  //       'totalCost':$totalCost,
  //       'tokenUser':$tokenUser,
  //       'tokenRider':$tokenRider,
  //       'ubication':$ubication,
  //       'state':$state,
  //       'orden':$orden,
  //     }
  //   ''';
  // }
}

class Orden {
  String idBusiness;
  String nameBusiness;
  List<PedidoDetails> order;
  // String coordenates='';
  Orden({
    idBusiness,
    nameBusiness,
    order,
  });

  // ignore: always_declare_return_types
  // toJson() async {
  //   var listaMap = <Map>[];
  //   order.forEach((element) {
  //     listaMap.add(element.toJson());
  //   });

  //   return {
  //     'nameBusiness': nameBusiness,
  //     'idBusiness': idBusiness,
  //     'order': listaMap,
  //   };
  // }

  Orden.createByJson(Map json) {
    var listaOrder = <PedidoDetails>[];
    List lista = json['order'];

    lista.forEach((element) {
      listaOrder.add(PedidoDetails.createByJson(element));
    });

    idBusiness = json['idBusiness'];
    nameBusiness = json['nameBusiness'];
    // coordenates = json['coordenates'];
    order = listaOrder;
  }

  // @override
  // String toString() {
  //   return '''
  //   'nameBusiness' : $nameBusiness,
  //   'idBusiness' : $idBusiness,
  //   'order' = $order
  //   ''';
  // }
}

class PedidoDetails {
  int amount;
  double price;
  ProductModel product;

  PedidoDetails({this.amount, this.price, this.product});

  // Map<String, dynamic> toJson() {
  //   return {'amount': amount, 'price': price, 'product': product.toJson()};
  // }

  PedidoDetails.createByJson(Map json) {
    amount = json['amount'];
    price = json['price'];
    product = ProductModel.createByJson(json['product']);
  }

  // @override
  // String toString() {
  //   return "{'amount':$amount,'price':$price,'product':$product}";
  // }
}
